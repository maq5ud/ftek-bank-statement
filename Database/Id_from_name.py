import pymysql.cursors
import sys


def bankname_id(bankname):
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='alpha',
                                 charset='utf8mb4',
                                 port=3306,
                                 cursorclass=pymysql.cursors.DictCursor)

    with connection.cursor() as cursor:
        bankIdQuery = "SELECT id FROM `ftek_bank` WHERE 'name' = '%s';" % (bankname)
        cursor.execute(bankIdQuery)
        bankId = cursor.fetchone()

        connection.commit()

    return bankId
