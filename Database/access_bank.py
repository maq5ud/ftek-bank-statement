import pymysql
import pymysql.cursors
import pandas as pd
import numpy as np
import warnings
import sys
import json
from Database.data_from_ifsc import data

sys.path.append('./../')


def access_bank_details(bankname, bankcode, ifsc_code, mobilenumber, statement_dict):
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='alpha',
                                 charset='utf8mb4',
                                 port=3306,
                                 cursorclass=pymysql.cursors.DictCursor)

    with connection.cursor() as cursor:
        bankIdQuery = "SELECT `id` FROM `ftek_bank` WHERE bank_name = '%s';" % (bankname)
        Id = cursor.execute(bankIdQuery)
        if Id is None:
            sql = "INSERT INTO `ftek_bank` (`bank_name`, `bank_code`) VALUES ('%s', '%s');" % (bankname, bankcode)
            bankIdQuery = "SELECT `id` FROM `ftek_bank` WHERE bank_name = '%s';" % (bankname)
            cursor.execute(sql)
            bankId =cursor.execute(bankIdQuery)

        connection.commit()

    with connection.cursor() as cursor:

        """sql = "IF(`ftek_branch`.id = `%s`, INSERT INTO `ftek_branch` (`ifsc`, `contact_no`) VALUES ('%s', '%s'), " \
              "INSERT INTO `ftek_branch` (`id`, `ifsc`, `contact_no`) VALUES (`%s`, '%s', '%s'));" % (Id, ifsc_code, mobilenumber,
                                                                                                      Id, ifsc_code, mobilenumber)
        """

        ifsc_data = data(ifsc_code)

        sql = "SELECT id" \
          "CASE" \
          "WHEN id = '%s' THEN 'INSERT INTO `ftek_branch` (`branch`, `ifsc`, `rtgs`, `neft, `imps`,`contact_no`" \
              "`address`, `city`, `district`, `state`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')'" \
          "ELSE 'INSERT INTO `ftek_branch` (`id`, `branch`, `ifsc`, `rtgs`, `neft, `imps`,`contact_no`" \
              "`address`, `city`, `district`, `state`) VALUES (`%s`, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')'" \
          "FROM ftek_branch;" % (Id, ifsc_data['branch'], ifsc_code, ifsc_data['rtgs'], ifsc_data['neft'],
                                 ifsc_data['imps'], mobilenumber, ifsc_data['address'], ifsc_data['city'],
                                 ifsc_data['district'], ifsc_data['state'], Id, ifsc_data['branch'],
                                 ifsc_code, ifsc_data['rtgs'], ifsc_data['neft'], ifsc_data['imps'], mobilenumber,
                                 ifsc_data['address'], ifsc_data['city'], ifsc_data['district'], ifsc_data['state'])



        cursor.execute(sql)
        connection.commit()

    with connection.cursor() as cursor:
        sql = "INSERT INTO `ftek_statement-detail` (`filename`, `password`, `filesize_in_kb`, `no_of_rows`, `no_of_pages`," \
              "`no_of_columns`) VALUES ('%s', '%s', '%s', '%s', '%s', '%s');" % (statement_dict['filename'], statement_dict['password'],
                                                                                 statement_dict['filesize_in_kb'],statement_dict['no_of_rows'],
                                                                                 statement_dict['no_of_pages'], statement_dict['no_of_columns'])

        cursor.execute(sql)
        connection.commit()
