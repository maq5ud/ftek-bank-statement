import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')


def update_db(new_filename, job_id):
    filename = new_filename
    ana_id = job_id

    connection = pymysql.connect(host = 'localhost',
                                user = 'maqsud',
                                password = 'password',
                                db = 'alpha',
                                charset = 'utf8mb4',
                                port = 3306,
                                cursorclass = pymysql.cursors.DictCursor)

    with connection.cursor() as cursor:
        sql = "UPDATE `analytics_report` SET `analysis_filename` = '%s' WHERE `analytics_report`.`job_id` = '%s';"%(filename,ana_id)
        cursor.execute(sql)
        result =cursor.fetchall()
        tabledf = pd.DataFrame(result)
        connection.commit()