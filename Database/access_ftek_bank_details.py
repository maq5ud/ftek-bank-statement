import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')


def update_ftek_bank_details(header_df,job_id):

	connection = pymysql.connect(host = 'localhost',
								user = 'maqsud',
								password = 'password',
								db = 'alpha',
								charset = 'utf8mb4',
								port = 3306,
								cursorclass = pymysql.cursors.DictCursor)

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `analytics_job` WHERE descr = '%s'" % job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["id"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_meta_data` WHERE user_id = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["upID"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_customer` WHERE upID = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id2 = tabledf["custID"][0]
		connection.commit()

	with connection.cursor() as cursor:
		bname = header_df["Data_Extracted"][3]
		ifsc_code = header_df["Data_Extracted"][2]
		cif_code = header_df["Data_Extracted"][4]
		micr_code = header_df["Data_Extracted"][5]
		custID = str(new_job_id2)
		atype = header_df["Data_Extracted"][8]
		acnumber = header_df["Data_Extracted"][1]

		sql = "INSERT INTO `ftek_bank_details` (`bname`, `ifsc_code`, `cif_code`, `micr_code`, `custID`, `atype`, `acnumber`) VALUES ('%s','%s','%s','%s','%s','%s','%s');"%(bname, ifsc_code, cif_code, micr_code, custID,atype,acnumber)
		# print(sql)
		cursor.execute(sql)
		connection.commit()
		print("ftek_bank_details updated.")