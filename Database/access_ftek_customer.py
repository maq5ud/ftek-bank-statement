import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')


def update_ftek_customer(header_df,job_id):

	connection = pymysql.connect(host = 'localhost',
								user = 'maqsud',
								password = 'password',
								db = 'alpha',
								charset = 'utf8mb4',
								port = 3306,
								cursorclass = pymysql.cursors.DictCursor)

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `analytics_job` WHERE descr = '%s'" % job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["id"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_meta_data` WHERE user_id = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		print(tabledf)
		new_job_id2 = tabledf["upID"][0]
		connection.commit()

	with connection.cursor() as cursor:
		mobile_no = header_df["Data_Extracted"][7]
		email = header_df["Data_Extracted"][9]
		pan_number = header_df["Data_Extracted"][6]
		upID = str(new_job_id2)
		fullname = header_df["Data_Extracted"][0]

		sql = "INSERT INTO `ftek_customer` (`mobile_no`, `email`, `pan_number`, `upID`, `fullname`) VALUES ('%s','%s','%s','%s','%s');"%(mobile_no, email, pan_number, upID, fullname)
		# print(sql)
		cursor.execute(sql)
		connection.commit()
		print("ftek_customer updated.")