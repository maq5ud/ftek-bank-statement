import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')

def update_ftek_feature(basic_feature,job_id):

	connection = pymysql.connect(host = 'localhost',
								user = 'maqsud',
								password = 'password',
								db = 'alpha',
								charset = 'utf8mb4',
								port = 3306,
								cursorclass = pymysql.cursors.DictCursor)

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `analytics_job` WHERE descr = '%s'" % job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["id"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_meta_data` WHERE user_id = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["upID"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_customer` WHERE upID = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id2 = tabledf["custID"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_xns` WHERE upID = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id3 = tabledf["tID"][0]
		connection.commit()

	for ind in basic_feature.index:
		parameter = basic_feature["Parameter"][ind]
		value = basic_feature["Value"][ind]
		tID = str(new_job_id3)
		custID = str(new_job_id2)

		with connection.cursor() as cursor:
			sql = "INSERT INTO `ftek_features` (`parameter`, `value`, `tID`, `custID`) VALUES ('%s','%s','%s','%s');"%(parameter, value, tID, custID)
			# print(sql)
			cursor.execute(sql)
			connection.commit()
	print("ftek_features updated.")