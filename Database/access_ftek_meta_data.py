import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')

def update_ftek_meta_data(meta_df,pth,filelist,job_id):

	connection = pymysql.connect(host = 'localhost',
								user = 'maqsud',
								password = 'password',
								db = 'alpha',
								charset = 'utf8mb4',
								port = 3306,
								cursorclass = pymysql.cursors.DictCursor)

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `analytics_job` WHERE descr = '%s'" % job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["id"][0]
		analytics_user_id = tabledf["analytics_user_id"][0]
		print("analytics_user_id:",analytics_user_id)
		connection.commit()

	with connection.cursor() as cursor:
		# print(meta_df.columns)
		modified_date = meta_df[0][0]
		# print(modified_date)
		created_date = meta_df[0][1]
		# print(created_date)
		user_id = new_job_id
		# print(user_id)
		producer = meta_df[0][2]
		# print(producer)
		description = meta_df[0][3]
		# print(description)
		fonts = meta_df[0][4]
		# print(fonts)
		analytics_id = analytics_user_id
		# file_name = filelist
		# print(file_name)
		# file_path = pth
		# print(file_path)

		sql = "INSERT INTO `ftek_meta_data` (`modified_date`, `created_date`, `user_id`, `producer`, `description`, `fonts`, `analytics_id`) VALUES ('%s','%s','%s','%s','%s','%s','%s');"%(modified_date, created_date, user_id, producer, description, fonts, analytics_id)
		# print(sql)
		cursor.execute(sql)
		connection.commit()
		print("ftek_meta_data updated.")
