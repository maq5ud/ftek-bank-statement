import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')

def update_ftek_xns(table_df_new,job_id):

	connection = pymysql.connect(host = 'localhost',
								user = 'maqsud',
								password = 'password',
								db = 'alpha',
								charset = 'utf8mb4',
								port = 3306,
								cursorclass = pymysql.cursors.DictCursor)

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `analytics_job` WHERE descr = '%s'" % job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id = tabledf["id"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_meta_data` WHERE user_id = '%s'" % new_job_id
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id2 = tabledf["upID"][0]
		connection.commit()

	with connection.cursor() as cursor:
		sql = "SELECT * FROM `ftek_customer` WHERE upID = '%s'" % new_job_id2
		cursor.execute(sql)
		result =cursor.fetchall()
		# print(result)
		tabledf = pd.DataFrame(result)
		# print(tabledf)
		new_job_id3 = tabledf["custID"][0]
		connection.commit()

	for ind in table_df_new.index:
		upID = str(new_job_id2)
		# print(new_job_id1)
		xns_date = table_df_new["Xns Date"][ind]
		xns_date1 = str(xns_date)
		# print(xns_date1)
		cheque_number = table_df_new["Cheque No"][ind]
		narration = table_df_new["Narration"][ind]
		Item = table_df_new["Category"][ind]
		debits = table_df_new["Debits"][ind]
		credits = table_df_new["Credits"][ind]
		balance = table_df_new["Balance"][ind]
		custID = str(new_job_id3)

		with connection.cursor() as cursor:
			sql = "INSERT INTO `ftek_xns` (`upID`, `xns_date`, `cheque_number`, `narration`, `Item`,`debits`, `credits`, `balance`,`custID`) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s');"%(upID, xns_date, cheque_number, narration, Item,debits,credits,balance,custID)
			# print(sql)
			cursor.execute(sql)
			connection.commit()
			
	print("ftek_xns updated.")