import requests


def data(IFSC_Code):
    URL = "https://ifsc.razorpay.com/"

    # Use get() method
    data = requests.get(URL + IFSC_Code).json()

    return data['BRANCH'], data['CITY'], data['RTGS'], data['NEFT'], data['ADDRESS'], data['IMPS'], \
           data['STATE'], data['DISRICT']