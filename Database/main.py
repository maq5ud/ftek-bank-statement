from Database.access_db import update_db
from Database.access_ftek_meta_data import update_ftek_meta_data
from Database.access_new_ftek_meta_data_detail import update_new_ftek_meta_detail
from Database.access_ftek_customer import update_ftek_customer
from Database.access_ftek_bank_details import update_ftek_bank_details
from Database.access_ftek_xns import update_ftek_xns
from Database.access_ftek_feature import update_ftek_feature
from Database.monthly_details import update_monthly_details
from Database.update_db import update_statement_record, update_parsing_exception


def database(meta_df, path, filelist, job_id, header_df, table_df_new, basic_feature, monthly_details):

    try:
        update_ftek_meta_data(meta_df, path, filelist, job_id)
    except Exception as e:
        print("ftek_meta_data error:", e)
        pass
    try:
        update_new_ftek_meta_detail(meta_df, path, filelist, job_id)
    except Exception as e:
        print("ftek_meta_data error:", e)
        pass
    try:
        update_ftek_customer(header_df, job_id)
    except Exception as e:
        print("ftek_customer error:", e)
        pass
    try:
        update_ftek_bank_details(header_df, job_id)
    except Exception as e:
        print("ftek_bank_details error:", e)
        pass
    try:
        update_ftek_xns(table_df_new, job_id)
    except Exception as e:
        print("ftek_xns erro:", e)
        pass
    try:
        update_ftek_feature(basic_feature, job_id)
    except Exception as e:
        print("basic_feature error:", e)
        pass
    try:
        update_monthly_details(monthly_details, table_df_new, job_id)
    except Exception as e:
        print("update_monthly_details error:", e)
        pass