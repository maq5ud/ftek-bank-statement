import pymysql
import pymysql.cursors
import pandas as pd 
import numpy as np 
import warnings
import sys
import json
sys.path.append('./../')

def update_monthly_details(monthly_details_df,xns,job_id):

    connection = pymysql.connect(host = 'localhost',
                                user = 'maqsud',
                                password = 'password',
                                db = 'alpha',
                                charset = 'utf8mb4',
                                port = 3306,
                                cursorclass = pymysql.cursors.DictCursor)

    with connection.cursor() as cursor:
        sql = "SELECT * FROM `analytics_job` WHERE descr = '%s'" % job_id
        cursor.execute(sql)
        result =cursor.fetchall()
        # print(result)
        tabledf = pd.DataFrame(result)
        # print(tabledf)
        new_job_id = tabledf["id"][0]
        connection.commit()

    year = xns["Xns Date"][2][:4]
    df = monthly_details_df
    total_months = len(df.index)-1
    if total_months == 6:
        credit_list = df["Total credits"].to_list()
        debit_list = df["Total debits"].to_list()
        credit_list = credit_list[:-1]; debit_list = debit_list[:-1]
    elif total_months < 6:
        add_value = 6-total_months
        credit_list = df["Total credits"].to_list()
        debit_list = df["Total debits"].to_list()
        for i in range(0,add_value):
            credit_list.insert(0, 0)
        for i in range(0,add_value):
            debit_list.insert(0, 0)
        credit_list = credit_list[:-1]; debit_list = debit_list[:-1]
    else:
        credit_list = df["Total credits"].to_list()
        debit_list = df["Total debits"].to_list()
        credit_list = credit_list[-7:]; debit_list = debit_list[-7:]
        credit_list = credit_list[:-1]; debit_list = debit_list[:-1]

    for i in range(0,6):
        with connection.cursor() as cursor:
            year = year
            month = i+1
            total_credit = credit_list[i]
            total_debit = debit_list[i]
            analytics_job_id = str(new_job_id)

            sql = "INSERT INTO `ftek_monthly_details` (`year`, `month`, `total_credit`, `total_debit`, `analytics_job_id`) VALUES ('%s','%s','%s','%s','%s');"%(year, month, total_credit, total_debit, analytics_job_id)
            # print(sql)
            cursor.execute(sql)
            connection.commit()
    print("ftek_monthly_details updated.")
