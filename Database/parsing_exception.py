import pymysql
import pymysql.cursors
import pandas as pd
import numpy as np
import warnings
import sys
import json
from Database.data_from_ifsc import data

sys.path.append('./../')


def parsing(parsing_statement):
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='',
                                 db='alpha',
                                 charset='utf8mb4',
                                 port=3306,
                                 cursorclass=pymysql.cursors.DictCursor)

    with connection.cursor() as cursor:
        sql = "INSERT INTO `ftek_parsing_exception` (`error_message`, `path`, `filename`, `line_no`) VALUES ('%s', '%s', '%s', '%s');" \
              "" % (parsing_statement['error_message'], parsing_statement['path'], parsing_statement['filename'],
                    parsing_statement['line_no'])

        cursor.execute(sql)
        connection.commit()