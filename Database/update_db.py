import pymysql
import pymysql.cursors
import warnings
import sys

import os
from os.path import *

import yaml
from Helper.ifsc_detail import ifsc_from_razorpay

from inspect import currentframe, getframeinfo

sys.path.append('./../')


def get_connection():
    
    config = yaml.load(open(join(dirname(abspath(__file__)),'config.yaml')))

    connection = pymysql.connect(
                                    host = config['mysql_host'],
                                    user = config['mysql_username'],
                                    password = config['mysql_password'],
                                    db = config['mysql_db'],
                                    charset = config['mysql_charset'],
                                    port = config['mysql_port'],
                                    cursorclass = pymysql.cursors.DictCursor
                                )
    return connection


def get_id(table, column, value):

    connection = get_connection()

    tables = yaml.load(open(join(dirname(abspath(__file__)),'table.yaml')))

    query = "SELECT `id` FROM " + tables[table] + "  WHERE `"+column+"`='%s' LIMIT 1;" %(value)

    cursor = connection.cursor()
    cursor.execute(query)
    id = cursor.fetchone()
    connection.commit()

    if(id is not None):

        return id['id']

    return id


def update_bank(bank_name):

    bank_id = None if (get_id('bank', "name", bank_name) is None) else get_id('bank', "name", bank_name)

    if (bank_id is None):

        connection = get_connection()
        tables = yaml.load(open(join(dirname(abspath(__file__)),'table.yaml')))
        bankQuery = "INSERT INTO "+ tables['bank'] + "VALUES ('%s');" %(bank_name)
        cursor = connection.cursor()
        cursor.execute(bankQuery)
        bank_id = cursor.lastrowid
        cursor.close()
        connection.close()

        return bank_id

    return bank_id


def update_branch(ifsc_data, bank_id):

    branch_id = None if (get_id('branch', "ifsc", ifsc_data['IFSC']) is None) else get_id('branch', "ifsc", ifsc_data['IFSC'])


    if (branch_id is None):

        table = yaml.load(open(join(dirname(abspath(__file__)),'table.yaml')))

        branch_code = ifsc_data['IFSC'][5:]
        rtgs = 1 if (ifsc_data['IFSC']) else 0
        neft = 0
        imps = 0

        pincode = ifsc_data['ADDRESS'][-6:] if (ifsc_data['ADDRESS'][-6:].isnumeric()) else None

        query = """
                    INSERT INTO """ + table['branch'] + """(branch, branch_code, ifsc, rtgs,
                    neft, imps, contact_no, address, pincode, city, district, state, bank_id)
                    VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');
                """ %(ifsc_data['BRANCH'], branch_code, ifsc_data['IFSC'], rtgs, neft, imps,
                    ifsc_data['CONTACT'], ifsc_data['ADDRESS'], pincode, ifsc_data['CITY'],
                    ifsc_data['DISTRICT'], ifsc_data['STATE'], bank_id)

        connection = get_connection()
        cursor = connection.cursor()
        cursor.execute(query)
        branch_id = cursor.lastrowid
        connection.commit()
        connection.close()

        return branch_id

    return branch_id


def update_statement_detail(data):

    connection = get_connection()
    table = yaml.load(open(join(dirname(abspath(__file__)),'table.yaml')))

    query = """
                INSERT INTO """ + table['statement_detail'] + """(filename, filesize_in_kb,
                startDate, endDate, password, no_of_rows, no_of_pages, no_of_columns, has_bank_logo)
                VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');
            """ %(data['filename'], data['filesize_in_kb'], data['startDate'], data['endDate'],
                data['password'], data['no_of_rows'], data['no_of_pages'], data['no_of_columns'],
                data['has_bank_logo'])
    

    cursor = connection.cursor()
    cursor.execute(query)
    statement_detail_id = cursor.lastrowid
    connection.commit()
    connection.close()

    return statement_detail_id


def update_statement_info(file_code, user_id, bank_id, branch_id, statement_details_id):
    
    connection = get_connection()
    table = yaml.load(open(join(dirname(abspath(__file__)),'table.yaml')))
    
    query = """
                INSERT INTO """ + table['statement_info'] + """(filename, user_id,
                bank_id, branch_id, statement_details_id)
                VALUES ('%s', '%s', '%s', '%s', '%s');
            """ %(file_code, user_id, bank_id, branch_id, statement_details_id)

    if(branch_id is None):

        query = """
            INSERT INTO """ + table['statement_info'] + """(filename, user_id,
            bank_id, statement_details_id)
            VALUES ('%s', '%s', '%s', '%s');
        """ %(file_code, user_id, bank_id, statement_details_id) 

    print(query)
    cursor = connection.cursor()
    cursor.execute(query)    
    statement_info_id = cursor.lastrowid
    connection.commit()
    connection.close()

    return statement_info_id


def update_parsing_exception(error_message, module, path, filename, lineno):
    connection = get_connection()
    table = yaml.load(open(join(dirname(abspath(__file__)),'table.yaml')))


def update_statement_record(data):

    bank_id = update_bank(data['bank_name'])

    branch_id = update_branch(ifsc_from_razorpay(data['ifsc'][0]), bank_id) if (data['ifsc'] is not (None or "")) else None

    statement_detail_id = update_statement_detail(data)

    statement_info_id = update_statement_info(data['filename'][:20], data['user_id'], bank_id, branch_id, statement_detail_id)

    return statement_info_id
    