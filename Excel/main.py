import os
from os.path import join, abspath
import pandas as pd


def save_to_excel(user_id, path, sunday_fraud_df, breakup_of_expenses, breakup_of_income, top_expenses,
                  reccuring_expenses, loan_emi, bounced_penal, monthly_details, eod_balance, high_val_trans,
                  irr_xns, table_df_new, job_id, meta_df, header_df, basic_feature, main_feature):
    try:
        directory = str(user_id)
        path = join(path, directory)
        os.mkdir(path)
    except:
        pass
    try:
        sunday_fraud_df["Xns Date"] = sunday_fraud_df["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        breakup_of_expenses["Xns Date"] = breakup_of_expenses["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        breakup_of_income["Xns Date"] = breakup_of_income["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        top_expenses["Xns Date"] = top_expenses["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        reccuring_expenses["Xns Date"] = reccuring_expenses["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        loan_emi["Xns Date"] = loan_emi["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        bounced_penal["Xns Date"] = bounced_penal["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        monthly_details["Xns Date"] = monthly_details["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        eod_balance["Xns Date"] = eod_balance["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        high_val_trans["Xns Date"] = high_val_trans["Xns Date"].dt.date.astype(
            str)
    except:
        pass
    try:
        irr_xns["Xns Date"] = irr_xns["Xns Date"].dt.date.astype(str)
    except:
        pass

    try:
        table_df_new["Xns Date"] = table_df_new["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        table_df_new.drop("Day", axis=1, inplace=True)
    except:
        pass
    try:
        table_df_new.drop("Date", axis=1, inplace=True)
    except:
        pass
    try:
        table_df_new["Narration"] = table_df_new["Narration"].str.replace(
            '\r', '')
    except:
        pass

    print(path)

    print(abspath(join(path, os.pardir)))

    os.chdir(abspath(join(path, os.pardir)) + os.sep)

    new_filename = str(job_id) + '.xlsx'
    with pd.ExcelWriter(new_filename) as writer:
        meta_df.to_excel(writer, sheet_name='Meta Data')
        header_df.to_excel(
            writer, sheet_name='Customer Information', index=False)
        sunday_fraud_df.to_excel(
            writer, sheet_name='Fraud Transactions', index=False)
        breakup_of_expenses.to_excel(writer, sheet_name='Breakup of Expenses')
        breakup_of_income.to_excel(writer, sheet_name='BreakUp of Incomes')
        top_expenses.to_excel(writer, sheet_name='Top Expenses')
        reccuring_expenses.to_excel(writer, sheet_name='Recurring Expenses')
        loan_emi.to_excel(writer, sheet_name='Loans and EMI Transactions')
        bounced_penal.to_excel(writer, sheet_name='Bounced-Penal Xns')
        monthly_details.to_excel(writer, sheet_name='Monthly Details')
        eod_balance.to_excel(writer, sheet_name='EOD Balances')
        high_val_trans.to_excel(writer, sheet_name='High Value Transactions')
        irr_xns.to_excel(writer, sheet_name='FCU Irregular Xns')
        table_df_new.to_excel(writer, sheet_name='Xns', index=False)
        basic_feature.to_excel(writer, sheet_name='Features', index=False)
        main_feature.to_excel(
            writer, sheet_name='Category_Features', index=False)
    # meta_df.to_excel(writer, sheet_name='Meta Data')
    # header_df.to_excel(writer, sheet_name='Customer Information', index = False)
    # table_df_new.to_excel(writer, sheet_name='Xns', index = False)
    # sunday_fraud_df.to_excel(writer, sheet_name='Sunday Fraud Transactions', index = False)

    return new_filename