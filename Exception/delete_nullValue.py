def delete_nullValue(d):
    for key, value in list(d.items()):
        if value is None:
            del d[key]
        elif isinstance(value, dict):
            delete_nullValue(value)
    return d