def throwException(e, module):
    trace = []
    tb = e.__traceback__
    while tb is not None:
        trace.append({
            "filename": tb.tb_frame.f_code.co_filename,
            "name": tb.tb_frame.f_code.co_name,
            "lineno": tb.tb_lineno
        })
        tb = tb.tb_next

    print(str({
        'type': type(e).__name__,
        'message': str(e),
        'trace': trace
    }))

    return {
        "Status": "Unsuccessful",
        "Code": 201,
        "Description": module + " Error",
        "Exception": {
            "type": type(e).__name__,
            "message": str(e),
            "trace": trace
        }
    }