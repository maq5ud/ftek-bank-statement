def jsonLowercase(d):
    d.columns = [x.lower().replace(' ', '_') for x in d.columns]
    return d