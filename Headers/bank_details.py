import requests


def getBankName(IFSC_Code):
    IFSC_Code = ' '.join([str(elem) for elem in IFSC_Code])
    URL = "https://ifsc.razorpay.com/"
    result = requests.get(URL + IFSC_Code).json()

    bankName = result['BANK']
    print('Bank Name : ', bankName)
    return bankName
