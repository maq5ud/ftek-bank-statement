import pandas as pd


def convert_to_df(customer_name, accountnumber, account_number, ifsc, ifsc_code, bankname,
                  result, cif, cif_code, micr, micr_code, pannumber, pancardnumber, mobi, mobilenumber,
                  accounttypecurrent, acc_type, Email, emailid):
    dataset = pd.DataFrame([["Account Name", customer_name]], columns=["Customer Information", "Data_Extracted"])
    if accountnumber != "": dataset.loc[len(dataset)] = ['Account Number', account_number]
    if ifsc != "": dataset.loc[len(dataset)] = ["IFSC Code", ifsc_code]
    if bankname != "": dataset.loc[len(dataset)] = ["Bank name", result]
    if cif != "": dataset.loc[len(dataset)] = ["Cif code", cif_code]
    if micr != "": dataset.loc[len(dataset)] = ["MICR Code", micr_code]
    if pannumber != "": dataset.loc[len(dataset)] = ["Pan card number", pancardnumber]
    if mobi != "": dataset.loc[len(dataset)] = ["Mobile number", mobilenumber]
    if accounttypecurrent != "": dataset.loc[len(dataset)] = ["Account Type", acc_type]
    if Email != "": dataset.loc[len(dataset)] = ["Email id", emailid]
    print(dataset)

    return dataset
