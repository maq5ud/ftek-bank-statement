from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.converter import TextConverter
import io
from Headers.clean_string import clean_string
from Headers.segregate_header import segregate_header
from Headers.account_no import fetch_account_no
from Headers.get_ifsc import fetch_ifsc
from Headers.get_cif import fetch_cif
from Headers.get_micr import fetch_micr
from Headers.get_pan import fetch_pan
from Headers.get_mobile_no import fetch_mobile_no
from Headers.get_type import fetch_type
from Headers.get_email import fetch_email
from Headers.get_bankname import fetch_bankname
from Headers.get_customer_name import fetch_customer_name
from Headers.convert_df import convert_to_df
from Headers.convert_json import convert_to_json
from Headers.bank_details import getBankName
from Headers.customer import cust
from inspect import currentframe, getframeinfo
import os
import sys
import json


def header(path, password, bankname):
    global customer_name
    rsrcmgr = PDFResourceManager()
    retstr = io.StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    device = TextConverter(rsrcmgr, retstr, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    maxpages = 2
    caching = True
    pagenos = set()

    password = b"" if ((password == None) or (password == "")) else password

    for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                                  check_extractable=False):
        interpreter.process_page(page)
    text = retstr.getvalue()

    uppercase = clean_string(text)

    empty, stringofuppercase = segregate_header(uppercase)

    customer_name, stringempty, stringofuppercase = cust(empty, stringofuppercase)

    stringempty, stringofuppercase, ifsc, ifsc_code = fetch_ifsc(stringempty, stringofuppercase)

    bank_name = getBankName(ifsc) if (ifsc != []) else fetch_bankname(bankname)

    stringempty, stringofuppercase, accountnumber, account_number = fetch_account_no(stringempty,
                                                                                     stringofuppercase, bankname)

    stringempty, stringofuppercase, cif_code, cif = fetch_cif(stringempty, stringofuppercase)

    stringempty, stringofuppercase, cif_code, micr, micr_code = fetch_micr(stringempty, stringofuppercase, cif_code)

    stringempty, pannumber, pancardnumber = fetch_pan(stringempty)

    stringempty, stringofuppercase, mobi, mobilenumber = fetch_mobile_no(stringempty, stringofuppercase)

    stringempty, stringofuppercase, accounttypecurrent, acc_type = fetch_type(stringempty, stringofuppercase)

    stringempty, Email, emailid = fetch_email(stringempty)

    customer_name = fetch_customer_name(stringempty, stringofuppercase, bank_name, bankname, accountnumber,
                                        account_number
                                        , customer_name)
    # print("customer_name: ", customer_name)
    dataFrame = convert_to_df(customer_name, accountnumber, account_number, ifsc, ifsc_code, bankname,
                              bank_name, cif, cif_code, micr, micr_code, pannumber, pancardnumber, mobi, mobilenumber,
                              accounttypecurrent, acc_type, Email, emailid)

    headerJson = json.dumps({
        "account_name": customer_name,
        "account_number": (accountnumber if accountnumber else account_number),
        "ifsc_code": (ifsc if ifsc else ifsc_code),
        "bank_name": (bankname if bankname else bank_name),
        "cif_code": cif_code,
        "micr_code": micr_code,
        "pan_number": (pannumber if pannumber else pancardnumber),
        "mobile_number": (mobi if mobi else mobilenumber),
        "account_type": (accounttypecurrent if accounttypecurrent else acc_type),
        "email": (Email if Email else emailid)
    })

    return dataFrame, headerJson
