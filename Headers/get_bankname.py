def fetch_bankname(bankname):
    if bankname == "Axis Bank":
        result = "Axis Bank"
    elif bankname == "State Bank of India":
        result = "SBI"
    elif bankname == "HDFC Bank":
        result = "HDFC Bank"
    elif bankname == "Kotak Mahindra Bank":
        result = "Kotak Mahindra bank"
    elif bankname == "Bank of Maharashtra":
        result = "Bank of Maharashtra"
    elif bankname == "Union Bank of India":
        result = "Union Bank Of India"
    elif bankname == "Punjab National Bank":
        result = "PNB"
    elif bankname == "Bank of Baroda":
        result = "Bank of Baroda"
    elif bankname == "ICICI Bank":
        result = "ICICI"
    elif bankname == "Punjab & Sind Bank":
        result = "Punjab and Sind Bank"
    elif bankname == "IDBI":
        result = "IDBI Bank"
    elif bankname == "Yes Bank":
        result = "Yes Bank"
    elif bankname == "Allahabad Bank":
        result = "Allahabad Bank"
    elif bankname == "Andhra Bank":
        result = "Andhra bank"
    elif bankname == "Bank of India":
        result = "Bank of India"
    elif bankname == "Canara Bank":
        result = "Canara Bank"
    elif bankname == "Central Bank Of India":
        result = "Central Bank Of India"
    elif bankname == "Corporation Bank":
        result = "Corporation Bank"
    elif bankname == "Federal Bank":
        result = "Federal Bank"
    elif bankname == "Indian Bank":
        result = "Indian Bank"
    elif bankname == "Indusind Bank":
        result = "Indusind Bank"
    elif bankname == "Syndicate Bank":
        result = "Syndicate Bank"
    elif bankname == "Vijaya Bank":
        result = "Vijaya Bank"
    elif bankname == "Bandhan Bank":
        result = "Bandhan Bank"
    elif bankname == "Dena Bank":
        result = "Dena Bank"
    elif bankname == "IDFC FIRST Bank":
        result = "IDFC First Bank"
    elif bankname == "Indian Overseas Bank":
        result = "Indian Overseas Bank"
    elif bankname == "Karnataka Bank":
        result = "Karnataka Bank"
    elif bankname == "Karur Vysya Bank":
        result = "KARUR VYSYA Bank"
    elif bankname == "Oriental Bank of Commerce":
        result = "OBC"
    elif bankname == "RBL Bank":
        result = "RBL Bank"
    elif bankname == "Saraswat Co-operative Bank":
        result = "Saraswat Bank"
    elif bankname == "Bharath Co-operative Bank":
        result = "Bharat Bank"
    elif bankname == "UCO Bank":
        result = "UCO Bank"
    elif bankname == "AU Small Finance Bank":
        result = "AU Small Finance Bank"
    elif bankname == "Ujjivan Small Finance Bank":
        result = "Ujjivan Small Finance bank"
    elif bankname == "United Bank of India":
        result = "United Bank of India"
    elif bankname == "Fingrowth Co-operative Bank":
        result = "Fingrowth Co-opp Bank"
    elif bankname == "Greater Bombay Co-operative Bank":
        result = "Greater Bombay Co-opp Bank"
    elif bankname == "NKGSB Co-operative Bank":
        result = "NKGSB bank"
    elif bankname == "Bassein Catholic Co-operative Bank":
        result = "Bassein Catholic Bank"
    elif bankname == "Abhyudaya Co-operative Bank":
        result = "Abhyudaya Co-opp Bank"
    elif bankname == "Standard Chartered Bank":
        result = "Standard Chattered Bank"
    elif bankname == "DCB Bank":
        result = "DCB Bank"
    elif bankname == "South Indian Bank":
        result = "South Indian Bank"
    elif bankname == "Utkarsh Small Finance Bank":
        result = "Utkarsh Small Finance Bank"
    elif bankname == "Equitas Small Finance Bank":
        result = "Equitas Small Finance Bank"
    elif bankname == "Paytm Payments Bank":
        result = "Paytm Payments Bank"
    elif bankname == "Laxmi Vilas Bank":
        result = "Laxmi Villas Bank"
    elif bankname == "Gopinath Patil Parsik Janata Sahakari Bank":
        result = "GP Parasik Sahakari Bank limited"
    else:
        result = ""


    return result
