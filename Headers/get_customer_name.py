import re


def fetch_customer_name(stringempty, stringofuppercase, result, bankname, accountnumber, account_number, customer_name):
    customername = re.findall(r'CUSTOMER NAME: [A-Z]{2,20} [A-Z]{0,20} [A-Z]{0,20}', stringempty)
    lencustomername = len(customername)
    # print("lencustomername",lencustomername)
    if lencustomername >= 1:
        custom_name = customername[0]
        customer_name = str.replace(custom_name, "CUSTOMER NAME:", "")
    #    print("Customer Name",customer_name)
    else:
        customeraname = re.findall(r'CUSTOMER NAME:[A-Z]{2,20} [A-Z]{0,20} [A-Z]{0,20}', stringempty)
        customeraname = re.findall(r'CUSTOMER NAME :[A-Z]{2,20} [A-Z]{0,20} [A-Z]{0,20}', stringempty)
        customeraname = re.findall(r'CUSTOMER NAME :[A-Z._\d]{2,20} [A-Z]{2,20}', stringempty)
        lencustomeraname = len(customeraname)
        if lencustomeraname >= 1:
            customer_nae = customeraname[0]
            customer_nme = str.replace(customer_nae, "CUSTOMER NAME:", "")
            customer_name = str.replace(customer_nme, "CUSTOMER NAME :", "")
        else:
            custname = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{0,20} [A-Z]{0,20}', stringempty)
            lencustname = len(custname)
            if lencustname >= 1:
                custome_name = custname[0]
                customr_name = str.replace(custome_name, "CUSTOMER NAME :", "")
                customer_name = str.replace(customr_name, "ACCOUNT NAME: ", "")
            else:
                accountholdername = re.findall(r'ACCOUNTHOLDER NAME:', stringempty)
                lenaccountholder = len(accountholdername)
                if lenaccountholder >= 1:
                    holder = re.findall(r'\D([A-Z]{2,20} [A-Z]{2,20})\D', stringempty)
                    lenholder = len(holder)
                    #               print("holder",lenholder)
                    if lenholder >= 1:
                        customer_name = holder[1]
                    else:
                        customer_name = ""
                #                  print("Custoer Error 404")
                else:
                    nameofcustomer = re.findall(r"NAME OF CUSTOMER", stringempty)
                    lenofcustomer = len(nameofcustomer)
                    if lenofcustomer == 1:
                        customerextraction = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                        customerextraction = re.findall(r'[A-Z\/]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                        lencustomerextracttion = len(customerextraction)
                        #                 print("Length of customer extraction",lencustomerextracttion)
                        if lencustomerextracttion == 1:
                            customer_name = customerextraction[0]
                        #                    print("Customer List",customerextraction)
                        elif lencustomerextracttion >= 9:  # for Oriental Bank of Commerce pattern 2
                            print(customerextraction)
                            if customerextraction[7] == "JOINT HOLDER NAME":
                                finalname = re.findall(r'[A-Z]{2,20}', stringofuppercase)
                                if len(finalname) >= 1:
                                    print(finalname)
                                    customer_name = finalname[9] + ' ' + finalname[10] + ' ' + finalname[11] + ' ' + \
                                                    finalname[12]

                            else:
                                customer_name = customerextraction[7]
                        else:
                            customer_name = ""
                    #                      print("No customer details here")
                    else:
                        customer_name = ""
                #                print("Customer Name not available")

    if bankname == "Federal Bank":
        customer = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
        customer = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
        customer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
        lencustomerame = len(customer)
        # print(lencustomerame,customer)
        if lencustomerame >= 1:
            name = re.findall(r'NAME                                   :', stringempty)
            lenname = len(name)
            if lenname >= 1:
                if customer[1] == "BRANCH NAME":
                    customer_name = customer[0]
                else:
                    customer_name = customer[1]
            else:
                customer_name = customer[0]
            # print(customer_name)
    elif bankname == "Syndicate Bank":
        customer = re.findall(r'NAME', stringempty)
        lencustomerame = len(customer)
        print(lencustomerame)
        if lencustomerame >= 1:
            customername = re.findall(r'CUSTOMER NAME', stringempty)
            lencustomername = len(customername)
            if lencustomername >= 1:
                customerame = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
                customerame = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
                customerame = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
                lencustomer = len(customerame)
                if lencustomer >= 1:
                    print(customerame)
                    customer_name = customerame[0]
            else:
                customerame = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
                customerame = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
                customerame = re.findall(r'[A-Z]{1,20}  [A-Z]{1,20}', stringempty)
                customerame = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
                lencustomer = len(customerame)
                if lencustomer >= 1:
                    if customerame[2] == "SON OF":
                        holder = re.findall(r'[A-Z]{2,20}  [A-Z]{2,20}', stringofuppercase)
                        if len(holder) >= 1:
                            customer_name = holder[0]
                    else:
                        customer_name = customerame[2]


        else:
            customeraccname = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
            customeraccname = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
            customeraccname = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
            lencustomeraccname = len(customeraccname)
            if lencustomeraccname >= 1:
                customer_name = customeraccname[0]

    elif bankname == "Canara Bank":
        customer = re.findall(r'CUSTOMER NAME', stringempty)
        lencustomer = len(customer)
        # print("customerlength",lencustomer,customer)
        if lencustomer >= 1:
            holder = re.findall(r'[A-Z]{2,20} [A-Z]{1,20}', stringempty)
            lenholder = len(holder)
            singleholder = re.findall(r'\D([A-Z]{2,20} [.])\D', stringempty)
            lensingleholder = len(singleholder)
            print("Length of holder", lenholder)
            print("Length of Single holder", lensingleholder)
            if lenholder >= 1 and lensingleholder >= 1:
                customer_name = singleholder[0]
                print("hello")
            else:
                if holder[0] == "CANARA BANK":
                    customer_name = holder[lenholder - 7]
                else:
                    if holder[0] == "UPI CREDIT":
                        customer_name = holder[1]
                    else:
                        if holder[0] == "PASS SHEET":
                            print(holder)
                            customer_name = holder[7]
                        else:
                            customer_name = holder[0]
        else:
            newname = re.findall(r'NAME     :[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(newname) >= 1:
                customer_names = newname[0]
                customer_name = str.replace(customer_names, "NAME     :", "")
            else:
                customer_name = ""

    elif result == "Karnataka Bank":
        customer = re.findall(r'NAME:-', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customername = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
            lencustomername = len(customername)
            if lencustomername >= 1:
                customer_name = customername[3]
        else:
            customerpart = re.findall(r'NAME', stringempty)
            lenpart = len(customerpart)
            if lenpart >= 1:
                partname = re.findall(r'\D([A-Z]{2,20})\D', stringempty)
                lenpartname = len(partname)
                if lenpartname == 1:
                    print(partname)
                    customer_name = partname[0]
                elif lenpartname >= 8:
                    customer_name = partname[7]
            else:
                newname = re.findall(r'NAME:       [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                if len(newname) >= 1:
                    customer_names = newname[0]
                    customer_name = str.replace(customer_names, "NAME:       ", "")
    elif bankname == "Indian Overseas Bank":
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            if customer[6] != "CUSTOMER ID":
                customer_name = customer[0]
            else:
                customer_name = customer[5]
        else:
            newname = re.findall(r'        M/S. [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} ', stringofuppercase)

            if len(newname) >= 1:
                customer_names = newname[0]
                customer_name = str.replace(customer_names, "        M/S. ", "")
    elif bankname == "Oriental Bank of Commerce":
        customer = re.findall(r'\D(CUSTOMER NAME:[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customer_nae = customer[0]
            customer_nme = str.replace(customer_nae, "CUSTOMER NAME:", "")
            customer_name = str.replace(customer_nme, "CUSTOMER NAME :", "")
    elif bankname == "Karur Vysya Bank":
        customer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customer_name = customer[0]
    elif bankname == "Saraswat Co-operative Bank":
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
        lencustomer = len(customer)
        print(customer)
        if lencustomer >= 1:
            if customer[0] == "STATEMENT OF" or customer[0] == 'PIN CODE':
                customer_name = customer[lencustomer - 1]
            elif customer[lencustomer - 1] == "INSTRUMENT NO":
                customer_name = customer[0]
            else:
                customer_name = customer[8]
    elif bankname == "Bandhan Bank":
        customer = re.findall(r'NAME', stringempty)
        lencustomerame = len(customer)
        print(lencustomerame)
        if lencustomerame >= 1:
            customerame = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
            lencustomer = len(customerame)
            if lencustomer >= 1:
                print(customerame)
                customer_name = customerame[0]
        else:
            newname = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringofuppercase)
            if len(newname) >= 3:
                customer_name = newname[2]
    elif bankname == "Indusind Bank":
        customer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
        lencustomer = len(customer)
        print("GG", customer)
        if lencustomer >= 1:
            if customer[0] == 'INDUSIND BANK':
                if customer[1] == 'UNITED TRADING':
                    customer_name = customer[2]
                else:
                    customer_name = customer[1]
            elif customer[0] == "STATEMENT OF":
                if customer[1] == 'UNITED TRADING':
                    customer_name = customer[2]
                else:
                    holder = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                    lenholder = len(holder)
                    if lenholder >= 1:
                        if holder[0] == "SEWREE INDUSTRIAL PREMISES PLOT NO" or holder[
                            0] == "MASJID WALI GALI SANGAM VIHAR":
                            customer_name = customer[1]
                        else:
                            customer_name = holder[0]
            elif customer[0] == 'ACCOUNT STATEMENT':
                final = customer[lencustomer - 3] + ' ' + customer[lencustomer - 2]
                customer_name = final
            elif customer[3] == "CUSTOMER ID":
                name = re.findall(r'  [A-Z]{2,20}  [A-Z]{2,20}', stringempty)
                print("Your Name", name)
                lenname = len(name)
                if lenname >= 1:
                    customer_name = name[0]
                else:
                    customer_name = customer[0]
            else:
                if customer[0] == "ROOM NO":
                    customer_name = customer[4]
                else:
                    if customer[0] == 'UNITED TRADING':
                        if customer[1] == 'GENERATION DATE':
                            customer_name = customer[2]
                        else:
                            customer_name = customer[1]
                    else:
                        print(customer)
                        customer_name = customer[0]
        else:
            cusstomername = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            print("cusstomername", cusstomername)
            if len(cusstomername) >= 1:

                if cusstomername[1] == "VARDHMAN NAGAR":
                    customer_name = cusstomername[0]
                else:
                    customer_name = cusstomername[1]
            else:
                nextname = re.findall(r'  [A-Z]{2,20} [A-Z]{2,20}([A-Z]{2,20}) [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                print("nextname", nextname)
                if len(nextname) >= 1:
                    customer_name = nextname[0]
    elif bankname == "IDFC FIRST Bank":
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [&] [A-Z]{2,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customer_name = customer[0]
        else:
            name = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
            lename = len(name)
            if lename >= 1:
                print(name)

                if name[4] == "RD FLOOR":
                    person = re.findall(r'[A-Z]{2,20}[.] [A-Z]{2,20}  [A-Z]{2,20}', stringempty)
                    lenperson = len(person)
                    if lenperson >= 1:
                        customer_name = person[0]
                else:
                    customer_name = name[4]
            else:
                holder = re.findall(
                    r'[A-Z]{1,20}&[A-Z]{1,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} ',
                    stringofuppercase)
                if len(holder) >= 1:
                    customer_name = holder[0]
    elif bankname == "RBL Bank":
        customer = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            custmer_name = customer[0]
            customer_name = str.replace(custmer_name, "ACCOUNT NAME: ", "")
        else:
            holder = re.findall(r'ACCOUNTHOLDER NAME:', stringempty)
            lenholder = len(holder)
            if lenholder >= 1:
                name = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                lenname = len(name)
                if lenname >= 1:
                    customer_name = name[1]
            else:
                customerame = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                lencustomerame = len(customerame)
                if lencustomerame >= 1:
                    customer_name = customerame[0]
                else:
                    customerame = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                    lencustomerame = len(customerame)
                    if lencustomerame >= 1:
                        customer_name = customerame[0]
    elif bankname == "Vijaya Bank":
        accountn = re.findall(r'\D(ACCOUNT NAME)\D', stringempty)
        lenaccountn = len(accountn)
        if lenaccountn >= 1:
            holder = re.findall(r'[A-Z]{1,20} [&] [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringempty)
            lenholder = len(holder)
            if lenholder >= 1:
                customer_name = holder[0]
        else:
            name = re.findall(r'NAME', stringempty)
            lename = len(name)
            if lename >= 1:
                customerame = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                lencustomerame = len(customerame)
                if lencustomerame >= 1:
                    print(customerame)
                    customer_name = customerame[2]
            else:
                customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                lencustomer = len(customer)
                if lencustomer >= 1:
                    #  print(customer)
                    customer_name = customer[0]
                else:
                    newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                    if len(newname) >= 1:
                        customer_name = newname[3]

    elif bankname == "HDFC Bank":
        customer = re.findall(r'M/S. [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customer_nme = customer[0]
            customer_name = str.replace(customer_nme, "M/S. ", "")
        else:
            cust_omer = re.findall(r'M/S. [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringempty)
            if len(cust_omer) >= 1:
                customer_names = cust_omer[0]
                customer_name = str.replace(customer_names, "M/S. ", "")
            else:
                customer__name = re.findall(r'M/S. [A-Z]{1,20} [A-Z]{1,20}', stringempty)
                if len(customer__name) >= 1:
                    customer_names = customer__name[0]
                    customer_name = str.replace(customer_names, "M/S. ", "")
                else:
                    nextname = re.findall(r'M/S. [A-Z]{1,20}', stringempty)
                    if len(nextname) >= 1:
                        customer_names = nextname[0]
                        customer_name = str.replace(customer_names, "M/S. ", "")
                    else:
                        customerame = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
                        lencustomerame = len(customerame)
                        if lencustomerame >= 1:
                            if customerame[0] == "PAGE NO":
                                holder = re.findall(r'MR. [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                                if len(holder) >= 1:
                                    customer_names = holder[0]
                                    customer_name = str.replace(customer_names, "MR.", "")
                            else:
                                customer_name = customerame[0]



    elif bankname == "Yes Bank":
        customer = re.findall(r'M/S.[A-Z,]{1,20} [A-Z,]{1,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customer_nae = customer[0]
            customer_name = str.replace(customer_nae, "M/S.", "")
        else:
            customername = re.findall(r'M/S.  [A-Z,]{2,20} [A-Z,]{2,20}', stringofuppercase)
            lencustomer = len(customername)
            if lencustomer >= 1:
                customer_nae = customername[0]
                customer_name = str.replace(customer_nae, "M/S.  ", "")
            else:
                customer = re.findall(r'M/S. [A-Z,]{2,20} [A-Z,]{2,20} [A-Z,]{2,20}', stringempty)
                lencustomer = len(customer)
                if lencustomer >= 1:
                    customer_nae = customer[0]
                    customer_name = str.replace(customer_nae, "M/S.", "")
                else:
                    customer = re.findall(r'PRIMARY HOLDER AGGARWAL STONE', stringofuppercase)
                    if len(customer) >= 1:
                        customer_names = customer[0]
                        customer_name = str.replace(customer_names, "PRIMARY HOLDER", "")
                    else:
                        cust_omer = re.findall(r'MRS. [A-Z]{1,20} [A-Z]{2,20}', stringofuppercase)
                        if len(cust_omer) >= 1:
                            print(cust_omer)
                            customer_name = cust_omer[0]
                        else:
                            customer = re.findall(r'[A-Z,]{2,20} [A-Z,]{2,20} [A-Z,]{1,20} [A-Z,]{2,20}',
                                                  stringofuppercase)
                            if len(customer) >= 1:
                                customer_name = customer[0]

    elif bankname == "IDBI":
        holder = re.findall(r':  [A-Z,]{2,20} [A-Z,]{2,20} [A-Z,]{2,20} [A-Z,]{2,20}', stringempty)
        if len(holder) >= 1:
            customer_nme = holder[0]
            customer_name = str.replace(customer_nme, ":", "")
        else:
            customer = re.findall(r':  [A-Z,]{2,20} [A-Z,]{2,20} [A-Z,]{2,20}', stringempty)
            lencustomer = len(customer)
            if lencustomer >= 1:
                customer_nam = customer[0]
                customer_name = str.replace(customer_nam, ":", "")
            else:
                double = re.findall(r':  [A-Z,]{2,20} [A-Z,]{2,20}', stringempty)
                if len(double) >= 1:
                    customer_nae = double[0]
                    customer_name = str.replace(customer_nae, ":", "")
                else:
                    customerame = re.findall(r':  [A-Z,]{2,20}  [A-Z,]{2,20} [A-Z,]{2,20}', stringempty)
                    if len(customerame) >= 1:
                        customer_nam = customer[0]
                        customer_name = str.replace(customer_nam, ":", "")
                    else:
                        customername = re.findall(r':  [A-Z,]{2,20} [A-Z,]{2,20}', stringofuppercase)
                        if len(customername) >= 1:
                            customer_nam = customername[0]
                            customer_name = str.replace(customer_nam, ":", "")
                        else:
                            newname = re.findall(r'[A-Z,]{2,20} [A-Z,]{2,20}', stringofuppercase)
                            if len(newname) >= 1:

                                if newname[7] == 'SUMMARY OF':
                                    customer_name = newname[0]
                                else:
                                    customer_name = newname[7]
    elif bankname == "Andhra Bank":
        customer = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        lencustomer = len(customer)
        if lencustomer >= 1:
            customer_nam = customer[0]
            customer_name = str.replace(customer_nam, "ACCOUNT NAME:", "")
        else:
            customername = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            lencustomername = len(customername)
            if lencustomername >= 1:
                customer_nam = customername[0]
                customer_name = str.replace(customer_nam, "ACCOUNT NAME:", "")
            else:
                newname = re.findall(r'ACCOUNT NAME      :[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                if len(newname) >= 1:
                    customer_nam = newname[0]
                    customer_name = str.replace(customer_nam, "ACCOUNT NAME      :", "")
                else:
                    nextname = re.findall(r'  NAME :[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                    if len(nextname) >= 1:
                        customer_names = nextname[0]
                        customer_name = str.replace(customer_names, "NAME :", "")
                    else:
                        holder = re.findall(r'MR. [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                        if len(holder) >= 1:
                            customer_names = holder[0]
                            customer_name = str.replace(customer_names, "MR. ", "")

    elif bankname == "Union Bank of India":
        customer = re.findall(r': [A-Z,]{2,20} [A-Z,]{2,20} [A-Z,]{2,20}', stringempty)
        if len(customer) >= 1:
            customer_nam = customer[0]
            customer_name = str.replace(customer_nam, ":", "")
        else:
            customerane = re.findall(r'[A-Z]{1,20} [A-Z]{2,20}', stringempty)
            print(customerane)
            holder = re.findall(r'([A-Z]{5,20})', stringofuppercase)
            print(holder)
            if len(customerane) >= 1:
                if len(holder) >= 1 and len(customerane) >= 1:
                    if len(holder) > 3:
                        if holder[3] == "UNION" or holder[3] == "INDIA":
                            newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                            if len(newname) >= 1:
                                if newname[len(newname) - 3] == "ROAD VILLAGE MANGOLPUR":
                                    nextname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                                    if len(nextname) >= 1:
                                        print("nextname", nextname)
                                        customer_name = nextname[len(nextname) - 10]
                                else:
                                    print("NEW NAME", newname)
                                    if newname[len(newname) - 3] == "AMBEKAR MRG NEAR":
                                        print(holder)
                                        customer_name = holder[22]
                                    else:
                                        customer_name = newname[len(newname) - 3]
                        else:
                            if holder[3] == "AMBEKAR":
                                customer_name = holder[2]
                            elif holder[0] == 'STATEMENT':
                                customer_name = holder[2]
                            else:
                                if customerane[0] == "UNION BANK":
                                    newcustomer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                                    if len(newcustomer) >= 1:
                                        print(newcustomer)
                                        customer_name = newcustomer[len(newcustomer) - 5]
                                elif customerane[0] == "STATEMENT OF":
                                    customer_name = customerane[1]
                                else:
                                    customer_name = customerane[0]
                else:
                    customer_name = customerane[1]


    elif bankname == "Dena Bank":
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} ', stringofuppercase)
        if len(customer) >= 1:
            customer_name = customer[0]
    elif bankname == "State Bank of India":
        customer = re.findall(
            r'ACCOUNT NAME                   :[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}',
            stringofuppercase)
        if len(customer) >= 1:
            customer_n = customer[0]
            customer_name = str.replace(customer_n, "ACCOUNT NAME                   :", "")
        else:
            customerane = re.findall(r'ACCOUNT NAME                   :[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}',
                                     stringofuppercase)
            if len(customerane) >= 1:
                customer_n = customerane[0]
                customer_name = str.replace(customer_n, "ACCOUNT NAME                   :", "")
            else:
                customername = re.findall(r'ACCOUNT NAME                   :[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}',
                                          stringofuppercase)
                if len(customername) >= 1:
                    customer_n = customername[0]
                    customer_name = str.replace(customer_n, "ACCOUNT NAME                   :", "")
                else:
                    holder = re.findall(r'ACCOUNT NAME                   :MR. [A-Z]{1,20} [A-Z]{1,20}',
                                        stringofuppercase)
                    if len(holder) >= 1:
                        customer_n = holder[0]
                        customer_name = str.replace(customer_n, "ACCOUNT NAME                   :", "")
                    else:
                        personal = re.findall(r'ACCOUNT NAME                   :MR. [A-Z]{1,20}  [A-Z]{1,20}',
                                              stringofuppercase)
                        if len(personal) >= 1:
                            customer_n = personal[0]
                            customer_name = str.replace(customer_n, "ACCOUNT NAME                   :", "")
                        else:
                            accountname = re.findall(r'ACCOUNT NAME', stringofuppercase)
                            if len(accountname) >= 1:
                                personname = re.findall(r'([A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20})', stringempty)
                                if len(personname) >= 1:
                                    print("personname", personname)
                                    if personname[0] == "STATE BANK OF":
                                        if personname[1] == 'ACCOUNT STATEMENT FOR':
                                            customer_name = personname[4]
                                        else:
                                            customer_name = personname[1]
                                    elif personname[0] != "STATE BANK OF INDIA":
                                        newname = re.findall(r':[A-Z]{1,20}  [A-Z]{1,20}', stringofuppercase)
                                        print("newnamesd", newname)
                                        if len(newname) >= 1:
                                            customer_names = newname[0]
                                            customer_name = str.replace(customer_names, ":", "")
                                        else:
                                            perronal = re.findall(r': MR. [A-Z]{1,20} [A-Z]{1,20}', stringofuppercase)
                                            if len(perronal) >= 1:
                                                print("perronal", perronal)
                                                customer_names = perronal[0]
                                                customer_name = str.replace(customer_names, ": MR. ", "")
                                            else:
                                                customer_name = personname[0]
                                    else:
                                        customer_name = personname[1]
                                else:
                                    perronal = re.findall(r': MR. [A-Z]{1,20}  [A-Z]{1,20} ', stringofuppercase)
                                    if len(perronal) >= 1:
                                        print("perronal", perronal)
                                        customer_names = perronal[0]
                                        customer_name = str.replace(customer_names, ": MR. ", "")
                            else:
                                names = re.findall(r'NAME : [A-Z]{1,20} [A-Z]{1,20}', stringempty)
                                if len(names) >= 1:
                                    customer_names = names[0]
                                    customer_name = str.replace(customer_names, "NAME : ", "")
                                else:
                                    rame = re.findall(
                                        r'ACCOUNT NAME                   :MR. [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}',
                                        stringofuppercase)
                                    if len(rame) >= 1:
                                        customer_names = rame[0]
                                        customer_name = str.replace(customer_names,
                                                                    "ACCOUNT NAME                   :MR.", "")
                                    else:
                                        spacer_name = re.findall(
                                            r'ACCOUNT NAME                   :[A-Z]{1,20} [A-Z]{1,20},MR. [A-Z]{1,20} [A-Z]{1,20}  .',
                                            stringofuppercase)
                                        if len(spacer_name) >= 1:
                                            print(spacer_name)
                                            customer_names = spacer_name[0]
                                            customer_name = str.replace(customer_names,
                                                                        "ACCOUNT NAME                   :", "")
                                            customer_name = str.replace(customer_name, "  .", "")

                                if customer_name == "ALL TOLL FREE":
                                    finder = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
                                    if len(finder) >= 1:
                                        customer_name = finder[len(finder) - 6]


    elif bankname == "Punjab & Sind Bank":
        customer = re.findall(r'NAME               :M/S [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        if len(customer) >= 1:
            customer_n = customer[0]
            customer_name = str.replace(customer_n, "NAME               :M/S ", "")
        else:
            customerane = re.findall(r'NAME               :M/S [A-Z]{2,20} [A-Z]{2,20}', stringempty)
            if len(customerane) >= 1:
                customer_n = customerane[0]
                customer_name = str.replace(customer_n, "NAME               :M/S ", "")
            else:
                newname = re.findall(r'ACCOUNT NAME:[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}',
                                     stringofuppercase)
                if len(newname) >= 1:
                    customer_name = newname[0]
                    customer_name = str.replace(customer_name, "ACCOUNT NAME:", "")
    elif bankname == "Punjab National Bank":
        customer = re.findall(r'ACCOUNT NAME :[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        # print("customer",customer)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "ACCOUNT NAME :", "")
        else:
            customerane = re.findall(r'ACCOUNT NAME :[A-Z]{2,20} [A-Z]{2,20}', stringempty)
            # print("customerane",customerane)
            if len(customerane) >= 1:
                customer_names = customerane[0]
                customer_name = str.replace(customer_names, "ACCOUNT NAME :", "")
            else:
                holder = re.findall(r'CUSTOMER NAME:', stringempty)
                if len(holder) >= 1:
                    name = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{2,20}', stringempty)
                    print("name", name)
                    if len(name) >= 1:
                        if len(name) == 4:
                            if name[2] == 'MAYUR VIHAT II':
                                customer_name = name[3]
                            else:
                                if name[0] == "ACCOUNT STATEMENT FOR":
                                    newname = re.findall(r'[A-Z]{2,20}  [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                                    if len(newname) >= 1:
                                        customer_name = newname[0]
                                    else:
                                        customer_name = name[2]
                                else:
                                    customer_name = name[1]
                        else:
                            if len(name) > 3:
                                if name[0] == "ACCOUNT STATEMENT FOR":
                                    newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                                    if len(newname) >= 1:
                                        print(newname)
                                        customer_name = newname[len(newname) - 8]
                                else:
                                    customer_name = name[3]
                            elif len(name) == 3:
                                newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                                if len(newname) >= 6:
                                    print("rrf", newname)
                                    if newname[len(newname) - 6] == 'NEW DELHI':
                                        customer_name = newname[len(newname) - 4]
                                    else:
                                        customer_name = newname[len(newname) - 6]
                                else:
                                    customer_name = name[1]
                            else:
                                newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                                if len(newname) >= 6:
                                    print(newname)
                                    if (len(newname) - 6) == 'NEW DELHI':
                                        customer_name = newname[len(newname) - 6]
                                    else:
                                        customer_name = newname[len(newname) - 8]

                                else:
                                    customer_name = name[1]


                    else:
                        newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                        if len(newname) >= 1:
                            if len(newname) >= 8:
                                if newname[0] == "BRANCH NAME" or newname[0] == "BRANCH ADDRESS":
                                    customer_name = newname[len(newname) - 4]
                                else:
                                    customer_name = newname[len(newname) - 7]
                else:
                    nextname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                    print(nextname)
                    if len(nextname) >= 1:
                        if nextname[0] == 'CUST ID':
                            customer_name = nextname[1]
                        elif nextname[4] == "GREEN VIEW":
                            customer_name = nextname[8]
                        elif nextname[1] == 'BRANCH NAME':
                            customer_name = nextname[9]
                        else:
                            newname = re.findall(r'CUSTOMER NAME', stringofuppercase)
                            if len(newname) >= 1:
                                newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                                if len(newname) >= 10:
                                    # print(newname)

                                    customer_name = newname[len(newname) - 8]
                            else:
                                # print("nextname",nextname)
                                customer_name = nextname[4]

    elif bankname == "Kotak Mahindra Bank":
        holder = re.findall(r'[A-Z]{2,20}', stringempty)
        if len(holder) >= 1:
            if holder[0] == "SL":
                newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                if len(newname) >= 1:
                    customer_name = newname[1]
            else:
                customer_name = holder[0]
        else:
            customerane = re.findall(r'[A-Z]{2,20}  [A-Z]{2,20}', stringempty)
            if len(customerane) >= 1:
                customer_name = customerane[0]
            else:
                name = re.findall(r'([A-Z]{2,20} [A-Z]{2,20})', stringempty)
                if len(name) >= 1:
                    customer_name = name[0]
                else:
                    customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                    newname = re.findall(r'([A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20})', stringempty)
                    if len(customer) >= 1:
                        if len(customer) >= 1 and len(newname) >= 1:
                            customer_name = newname[0]
                        else:
                            customer_name = customer[0]
                    else:
                        holder = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                        if len(holder) >= 1:
                            print(holder)
                            customer_name = holder[5]
    elif bankname == "ICICI Bank":
        customer = re.findall(r'M/S.[A-Z]{1,20} [A-Z]{1,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        print(customer)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "M/S.", "")
        else:
            customerane = re.findall(r'M/S.[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
            if len(customerane) >= 1:
                customer_names = customerane[0]
                customer_name = str.replace(customer_names, "M/S.", "")
            else:
                customername = re.findall(r'M/S.[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                if len(customername) >= 1:
                    customer_names = customername[0]
                    customer_name = str.replace(customer_names, "M/S.", "")
                else:
                    holder = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                    if len(holder) >= 1:
                        customer_names = holder[0]
                        customer_name = str.replace(customer_names, "ACCOUNT NAME: ", "")
                    else:
                        name = re.findall(r'MR.[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                        if len(name) >= 1:
                            customer_names = name[0]
                            customer_name = str.replace(customer_names, "MR.", "")
                        else:
                            newn = re.findall(r'MR.[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                            if len(newn) >= 1:
                                customer_names = newn[0]
                                customer_name = str.replace(customer_names, "MR.", "")
                            else:
                                nextn = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringempty)
                                if len(nextn) >= 1:
                                    if nextn[2] == "RTN CHG":
                                        customer_name = ""
                                    elif nextn[2] == "ACCOUNT NAME":
                                        namer = re.findall(r'[A-Z]{2,20}', stringofuppercase)
                                        if len(namer) >= 1:
                                            customer_name = (namer[13] + ' ' + namer[14] + namer[15] + ' ' + namer[
                                                16] + ' ' + namer[17])
                                            if customer_name == "BHAGWATI TRADERSPRATAP ENCLAVE MOHAN":
                                                customer_name = (namer[13] + ' ' + namer[14])
                                    else:
                                        print(nextn)
                                        customer_name = nextn[2]

    elif bankname == "Bank of Baroda":
        customer = re.findall(r'NAME', stringofuppercase)
        if len(customer) >= 1:
            customername = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            print(customername)
            if len(customername) >= 1:
                if customername[3] == "ACCOUNT NAME" or customername[5] == "ACCOUNT NUMBER":
                    newcustomer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringofuppercase)
                    if len(newcustomer) >= 1:
                        print(newcustomer)
                        print("b2")
                        customer_name = newcustomer[0]

                elif customername[3] == "BRANCH NAME":
                    print("A 1")
                    customer_name = customername[4]
                else:
                    if customername[5] == "DILSHAD COLONY":
                        print('c3')
                        customer_name = customername[4]
                    elif customername[5] == "DILSHAD GARDEN" or customername[5] == "IFSC CODE":
                        print('d4')
                        customer_name = customername[6]
                    elif customername[5] == "CUSTOMER DETAILS":
                        print('e5')
                        customer_name = customername[7]
                    elif customername[5] == "INTEREST RATES" or customername[5] == "ACCOUNT NAME":
                        print('f6')
                        customer_name = customername[9]
                    elif customername[5] == "BY CASH":
                        print('g7')
                        customer_name = customername[2]
                    else:

                        if customername[0] == "ACCOUNT NUMBER":
                            customer_name = customername[3]
                        else:
                            customer_name = customername[5]
        else:
            newname = re.findall(r'M/S. [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(newname) >= 1:
                customer_names = newname[0]
                customer_name = str.replace(customer_names, "M/S.", "")
            else:
                name = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{2,20}', stringempty)
                if len(name) >= 1:
                    print("b2", name)
                    customer_name = name[0]
    elif bankname == "Axis Bank":
        customer = re.findall(r'[A-Z]{2,20}  [A-Z]{2,20}', stringempty)
        if len(customer) >= 1:
            if customer[0] == "JOINT HOLDER":
                customer_name = "NO Name"
            else:
                customer_name = customer[0]
        else:
            name = re.findall(r'[A-Z]{2,20} [A-Z]{1,20}', stringempty)
            if len(name) >= 1:
                if name[0] == "STATEMENT OF" or name[0] == "GIRDHARI PURA":
                    holder = re.findall(r'[A-Z]{2,20} [A-Z]{1,20}', stringofuppercase)
                    if len(holder) >= 1:
                        if holder[5] == "JOINT HOLDER":
                            customer_name = holder[4]
                        else:
                            customer_name = holder[5]
                else:
                    if name[0] == "TRAN DATE":
                        customer_name = name[len(name) - 4]
                    else:
                        if name[0] == "JOINT HOLDER":
                            customernamer = re.findall(r'[A-Z]{2,20}', stringofuppercase)
                            if len(customernamer) >= 1:
                                customer_name = customernamer[0]

                        else:
                            customer_name = name[0]
            else:
                holder = re.findall(r'[A-Z]{2,20} [A-Z]{1,20}', stringofuppercase)
                if len(holder) >= 1:
                    # print("holder",holder)
                    customername = holder[2]
                else:
                    customernamer = re.findall(r'[A-Z]{2,20}', stringofuppercase)
                    if len(customernamer) >= 1:
                        customer_name = customernamer[0]


    elif bankname == "Allahabad Bank":
        name = re.findall(r'M/S. [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        if len(name) >= 1:
            customer_names = name[0]
            customer_name = str.replace(customer_names, "M/S.", "")
        else:
            customer = re.findall(r'MR. [A-Z]{2,20}  [A-Z]{2,20}', stringofuppercase)
            if len(customer) >= 1:
                customer_names = customer[0]
                customer_name = str.replace(customer_names, "MR. ", "")
            else:
                newname = re.findall(r'M/S. [A-Z]{2,20}  [A-Z]{2,20}', stringofuppercase)
                if len(newname) >= 1:
                    customer_names = newname[0]
                    customer_name = str.replace(customer_names, "M/S.", "")
                else:
                    nextname = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                    if len(nextname) >= 1:
                        customer_names = nextname[0]
                        customer_name = str.replace(customer_names, "ACCOUNT NAME: ", "")

    elif bankname == "Bank of India":
        nextname = re.findall(r'NAME OF CUSTOMER', stringofuppercase)
        if len(nextname) >= 1:
            nextnamer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            print("33", nextnamer)
            if len(nextnamer) >= 1:
                if len(nextnamer) >= 13:
                    if nextnamer[10] == "CUSTOMER ID":
                        customer_name = nextnamer[13]
                    else:
                        customer_name = nextnamer[10]
        else:
            nname = re.findall(r'NAME', stringempty)
            if len(nname) >= 1:
                person = re.findall(r':  [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                if len(person) >= 1:
                    customer_names = person[0]
                    customer_name = str.replace(customer_names, ":", "")
                else:
                    personal = re.findall(r':  [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                    if len(personal) >= 1:
                        customer_names = personal[0]
                        customer_name = str.replace(customer_names, ":", "")

            else:
                customer = re.findall(r'MR [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                if len(customer) >= 1:
                    customer_names = customer[0]
                    customer_name = str.replace(customer_names, "MR", "")
                else:
                    customer = re.findall(r'([A-Z]{2,20} [A-Z]{2,20})', stringempty)
                    print("customer", customer)
                    if len(customer) >= 1:
                        if customer[0] == "YOUR CONSOLIDATED":
                            customer_name = customer[2]
                        else:
                            customer_name = customer[0]
                    else:
                        name = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
                        print("name", name)
                        if len(name) > 1:
                            customer_name = name[1]
    elif result == "United Bank of India":
        customer = re.findall(r'NAME', stringempty)
        lencustomerame = len(customer)
        print(lencustomerame)
        if lencustomerame >= 1:
            customername = re.findall(r'CUSTOMER NAME', stringempty)
            lencustomername = len(customername)
            if lencustomername >= 1:
                customerame = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
                lencustomer = len(customerame)
                if lencustomer >= 1:
                    customer_name = customerame[2]
            else:
                newname = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
                if len(newname) >= 1:
                    customer_name = newname[0]
    elif result == "UCO Bank":
        customer = re.findall(r'NAME', stringempty)
        lencustomerame = len(customer)
        print(lencustomerame)
        if lencustomerame >= 1:
            customerame = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
            lencustomer = len(customerame)
            if lencustomer >= 1:
                customer_name = customerame[2]
    elif result == "Bharat Bank":
        customerame = re.findall(r'\D([A-Z]{1,20} [A-Z]{1,20})\D', stringempty)
        lencustomer = len(customerame)
        if lencustomer >= 1:
            if customerame[1] == "ACCOUNT NO":
                customer_name = customerame[3]
            else:
                customer_name = customerame[1]
    elif result == "AU Small Finance Bank":
        customername = re.findall(r'ACCOUNT NAME  ', stringempty)
        lencustomername = len(customername)
        if lencustomername >= 1:
            customerame = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringempty)
            lencustomer = len(customerame)
            if lencustomer >= 1:
                if lencustomer >= 7:
                    print(customerame)
                    customer_name = customerame[6]

        else:
            newname = re.findall(r'  CUSTOMER NAME :', stringempty)
            if len(newname) >= 1:
                nextname = re.findall(r'MR.  [A-Z]{1,20} [A-Z]{1,20}', stringempty)
                if len(nextname) >= 1:
                    customer_names = nextname[0]
                    customer_name = str.replace(customer_names, "MR.  ", "")
            else:
                newcustomer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringofuppercase)
                if len(newcustomer) >= 1:
                    if newcustomer[0] == "VALUE DATE":
                        customer_name = newcustomer[3]
                    else:
                        print(newcustomer)
                        customer_name = newcustomer[2]
    elif result == "Ujjivan Small Finance bank":
        customer = re.findall(r'NAME', stringofuppercase)
        lencustomerame = len(customer)
        if lencustomerame >= 1:
            customerame = re.findall(r': [A-Z]{1,20} [A-Z]{1,20}', stringofuppercase)
            lencustomer = len(customerame)
            if lencustomer >= 1:
                customer_names = customerame[0]
                customer_name = str.replace(customer_names, ": ", "")
    elif bankname == "Corporation Bank":

        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) > 1:
            if customer[0] == 'CORPORATION BANK':
                name = re.findall(r'M/S   [A-Z]{1,20} [A-Z]{1,20}', stringempty)
                if len(name) >= 1:
                    customer_names = name[0]
                    customer_name = str.replace(customer_names, "M/S   ", "")
            else:

                if (customer[1] == "CUSTOMER ID" or customer[1] == "CIF CODE") or customer[1] == "CLIENT CODE":
                    customer_name = customer[2]
                elif customer[1] == 'BRANCH CODE':
                    personalname = re.findall(r'NAME', stringofuppercase)
                    if len(personalname) >= 1:
                        personame = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                        if len(personame) >= 1:
                            customer_name = personame[1]
                    else:
                        customer_name = ""
                else:
                    print(customer[1])
                    customer_name = customer[1]
        else:
            nextname = re.findall(r'M/S   [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(nextname) >= 1:
                print(nextname)
                customer_names = nextname[0]
                customer_name = str.replace(customer_names, "M/S   ", "")
            else:
                findername = re.findall(r'ACCOUNTHOLDER NAME : M/S [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                if len(findername) >= 1:
                    customer_names = findername[0]
                    customer_name = str.replace(customer_names, "ACCOUNTHOLDER NAME : M/S", "")
                else:
                    newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                    if len(newname) >= 1:
                        print("newname", newname)
                        customer_name = newname[1]

    elif bankname == "Indian Bank":
        customer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20} [A-Z]{1,20}', stringempty)

        if len(customer) == 1 or len(customer) == 2:
            newname = re.findall(r'[A-Z]{1,20} [A-Z]{1,20}', stringofuppercase)

            if len(newname) >= 13:
                print(newname)
                customer_name = newname[12]
            else:
                customer_name = customer[0]
        elif len(customer) > 1:
            print("customer", customer)
            if customer[1] == "OPP INDIRA PARK COLO":
                holder = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                if len(holder) >= 14:
                    customer_name = holder[14]
            else:
                customer_name = customer[1]
        else:
            newcustomer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(newcustomer) >= 11:

                customer_name = newcustomer[10]
            else:
                customer_name = ""
    elif result == "Abhyudaya Co-opp Bank":
        customer = re.findall(r'CUSTOMER NAME  :  [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "CUSTOMER NAME  :  ", "")
        else:
            customername = re.findall(r'CUSTOMER NAME  :  [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(customername) >= 1:
                customer_names = customername[0]
                customer_name = str.replace(customer_names, "CUSTOMER NAME  :  ", "")
            else:
                name = re.findall(r'CUSTOMER NAME M/S [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} ', stringempty)
                if len(name) >= 1:
                    customer_names = name[0]
                    customer_name = str.replace(customer_names, "CUSTOMER NAME M/S", "")
                else:
                    customer_name = ""
    elif result == "Bassein Catholic Bank":
        customer = re.findall(r'CUSTOMER NAME  : M/S [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, 'CUSTOMER NAME  : M/S', '')
        else:
            name = re.findall(r'\D([A-Z]{2,20} [A-Z]{2,20})\D', stringempty)
            if len(name) >= 4:
                customer_name = name[4]
            else:
                customer_name = ""
    elif result == "Fingrowth Co-opp Bank":
        customer = re.findall(r'MR. [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "MR.", "")
    elif result == "NKGSB bank":
        customer = re.findall(r'M/S. [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "M/S. ", "")
    elif result == "Greater Bombay Co-opp Bank":
        customer = re.findall(r'M/S. [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "M/S. ", "")
        else:
            newname = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(newname) >= 8:
                if newname[7] == "ACCOUNT NO" or newname[7] == "CENTRAL PARK":
                    customer_name = newname[1]
                else:

                    customer_name = newname[7]
    elif result == "Standard Chattered Bank":
        customer = re.findall(r'M/S [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, "M/S", "")
    elif result == "DCB Bank":
        customer = re.findall(r'[A-Z]{1,20} [A-Z]{1,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            print(customer)
            if customer[1] == "MADHUBAN CHSL NEXT":
                name = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                if len(name) >= 1:
                    print(name)
                    customer_name = name[len(name) - 6]
            elif customer[1] == 'DELHI BRANCH PHONE':
                customer_name = customer[2]
            else:
                customer_name = customer[1]
        print("for DCB USing it here", accountnumber)
    elif bankname == "South Indian Bank":
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            customer_name = customer[0]
    elif bankname == "Utkarsh Small Finance Bank":
        customer = re.findall(r'M/S [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        print(len(customer[0]))
        if len(customer) >= 1:
            if len(customer) == 1:
                customer_one = customer[0]
                lencustomerone = len(customer[0])

                if (customer_one[lencustomerone - 3] + customer_one[lencustomerone - 2] + customer_one[
                    lencustomerone - 1]) == "AND":
                    newname = re.findall(r'M/S [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}',
                                         stringofuppercase)
                    if len(newname) >= 1:
                        customer_names = newname[0]
                        customer_name = str.replace(customer_names, "M/S", "")
                else:
                    customer_names = customer[0]
                    customer_name = str.replace(customer_names, "M/S", "")
    elif bankname == "Equitas Small Finance Bank":
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            customer_name = customer[0]
    elif bankname == "Paytm Payments Bank":
        mobilenumber = account_number
        customer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:

            if customer[0] == 'ACCOUNT STATEMENT FOR':
                customername = re.findall(r'[A-Z]{2,20}.[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                if len(customername) >= 1:
                    print(customername)
                    customer_name = customername[2]
            else:
                customer_name = customer[0]


    elif bankname == "Laxmi Vilas Bank":
        customer = re.findall(r'M/S [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customer) >= 1:
            customer_names = customer[0]
            customer_name = str.replace(customer_names, 'M/S', '')
        else:
            customerame = re.findall(r'NAME     :[A-Z]{2,20}.  [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(customerame) >= 1:
                customer_names = customerame[0]
                customer_name = str.replace(customer_names, 'NAME     :', '')
    elif bankname == "Gopinath Patil Parsik Janata Sahakari Bank":
        customername = re.findall(r'M/S [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(customername) >= 1:
            customer_name = customername[0]
            customer_name = str.replace(customer_name, "M/S", "")

    elif bankname == "Bank of Maharashtra":
        name = re.findall(r'MISS. [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringempty)
        if len(name) >= 1:
            customer_names = name[0]
            customer_name = str.replace(customer_names, "MISS.", "")
        else:
            customer = re.findall(r'MR. [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
            if len(customer) >= 1:
                customer_names = customer[0]
                customer_name = str.replace(customer_names, "MR. ", "")
            else:
                newname = re.findall(r'[A-Z]{1,20}[&][A-Z]{1,20} [A-Z]{2,20}', stringofuppercase)
                if len(newname) >= 1:
                    customer_names = newname[0]
                else:
                    nextname = re.findall(r'ACCOUNT NAME: [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                    if len(nextname) >= 1:
                        customer_names = nextname[0]
                        customer_name = str.replace(customer_names, "ACCOUNT NAME: ", "")
                    else:
                        custo_mer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                        if len(custo_mer) >= 1:
                            customer_names = custo_mer[0]
                        else:
                            nname = re.findall(r'[A-Z]{1,20} [A-Z]{2,20} [A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
                            if len(nname) >= 1:
                                customer_names = nname[0]


    elif bankname == "Central Bank Of India":
        custo_mer = re.findall(r'[A-Z]{2,20} [A-Z]{2,20}', stringofuppercase)
        if len(custo_mer) >= 1:
            customer_names = custo_mer[0]

    return customer_name
