import re


def fetch_ifsc(stringempty, stringofuppercase):
    ifsc = re.findall(r"\D([A-Z]{4}\d{7})\D", stringempty)
    lenifsc = len(ifsc)
    if lenifsc == 1:
        ifsc_code = ifsc[0]
    #     print("IFSC Code",ifsc_code)
    else:
        ifsc = re.findall(r"[A-Z]{4}\d{1}[A-Z]{5,6}", stringempty)
        lenifsc = len(ifsc)
        if lenifsc == 1:
            ifsc_code = ifsc[0]
        #        print("IFSC Code",ifsc_code)
        else:
            ifscupper = re.findall(r'IFSC         : [A-Z]{4}\d{7}', stringofuppercase)
            ifscupper = re.findall(r'IFSC CODE:[A-Z]{4}\d{7}', stringofuppercase)
            lenifscupper = len(ifscupper)
            if lenifscupper >= 1:
                ifscu_code = ifscupper[0]
                ifsuc_code = str.replace(ifscu_code, "IFSC         :", "")
                ifsc_code = str.replace(ifsuc_code, "IFSC CODE:", "")

            else:
                ifscode = re.findall(r'IFSC ', stringofuppercase)
                lenifscode = len(ifscode)
                if lenifscode == 1:
                    ifsccode = re.findall(r'\D(: [A-Z]{4}\d{7} )\D', stringofuppercase)
                    lenifsccode = len(ifsccode)
                    #               print("length from text IFSC ",lenifsccode)
                    if lenifsccode == 1:
                        ifsc_codes = ifsccode[0]
                        ifsc_code = str.replace(ifsc_codes, ":", "")
                    #                  print("Stringofuppercase ")
                    else:
                        ifsc_code = ""
                #                 print("Problem in reading here ")

                else:
                    newifcs = re.findall(r'IFCS CODE:', stringofuppercase)
                    if len(newifcs) >= 1:
                        ifcs = re.findall(r'[A-Z]{4}\d{7}', stringofuppercase)
                        if len(ifcs) >= 1:
                            ifsc_code = ifcs[0]
                    else:
                        ifsccorp = re.findall(r'IFSC/RTGS/NE :       [A-Z]{4}\d{7}', stringofuppercase)
                        if len(ifsccorp) >= 1:
                            ifsc_codes = ifsccorp[0]
                            ifsc_code = str.replace(ifsc_codes, "IFSC/RTGS/NE :       ", "")
                        else:
                            ifsckar = re.findall(
                                r'                                                                                      IFSC         : [A-Z]{4}\d{7}',
                                stringofuppercase)
                            if len(ifsckar) >= 1:
                                ifsc_codes = ifsckar[0]
                                ifsc_code = str.replace(ifsc_codes,
                                                        '                                                                                      IFSC         : ',
                                                        "")
                            else:
                                newifsc_code = re.findall(r'IFSC : [A-Z]{4}\d{7}', stringofuppercase)
                                #                                 print("newifsc",newifsc_code)
                                if len(newifsc_code) >= 1:
                                    ifsc_code = newifsc_code[0]
                                #                                     ifsc_code=str.replace(ifsc_codes,"IFSC :")
                                else:
                                    ifsc_codefornkgsb = re.findall(r'IFSC CODE : [A-Z]{4}\d{7}', stringofuppercase)
                                    if len(ifsc_codefornkgsb) >= 1:
                                        ifsc_codes = ifsc_codefornkgsb[0]
                                        ifsc_code = str.replace(ifsc_codes, "IFSC CODE :", "")
                                    else:
                                        ifscforpaytm = re.findall(r'\D([A-Z]{4}\d{7})\D', stringofuppercase)
                                        if len(ifscforpaytm) >= 1:
                                            ifsc_code = ifscforpaytm[0]
                                        else:
                                            ifsc_code = ""
        #            print("ifsc not available in text also ")

    return stringempty, stringofuppercase, ifsc, ifsc_code
