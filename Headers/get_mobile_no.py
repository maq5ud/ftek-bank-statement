import re


def fetch_mobile_no(stringempty, stringofuppercase):
    mobi = re.findall(r"MOBILE NO. :", stringempty)
    mobi = re.findall(r"PHONE NO.", stringempty)
    #    mobi=re.findall(r"MOBILE NO",stringempty)
    # mobi=re.findall(r"PHONE",stringempty)
    mobi = re.findall(r'MOBILE NO.', stringempty)
    mobi = re.findall(r"MOBILE / PHONE NO : ", stringempty)
    mobi = re.findall(r"PHONE", stringempty)
    lenmobi = len(mobi)
    #  print("MOBILE LENGTH",lenmobi)
    if lenmobi == 1:
        mobile = re.findall(r"\d{10,12}", stringempty)
        strmobile = str(mobile)
        lenmob = len(mobile)
        #     print("STR MOBILElength",lenmob)
        if lenmob >= 1:
            mo1 = re.findall(r"\D([91]{2}\d{10})\D", strmobile)
            # mo1=re.findall(r"\D([91]{2}\d{10})\D",strmobile)
            mo2 = re.findall(r"\D([0]{1}\d{10})\D", strmobile)
            mo3 = re.findall(r"\D([7/8/9]?\d{9})\D", strmobile)
            mo4 = re.findall(r"\D([+91]{2}[(0)]{1}\d{10})\D", stringempty)
            lenmo1 = len(mo1)
            lenmo2 = len(mo2)
            lenmo3 = len(mo3)
            lenmo4 = len(mo4)
            #             print("lenmo1",lenmo1)
            #             print("lenmo2",lenmo2)
            #             print("lenmo3",lenmo3)
            #             print("lenmo4",lenmo4)
            if lenmo1 == 1:
                mobilenumber = mo1[0]
            #       print("This is the mobile number",mobilenumber)
            elif lenmo2 == 1:
                mobilenumber = mo2[0]
            #       print("This is the mobile number",mobilenumber)
            elif lenmo3 == 1:
                mobilenumber = mo3[0]
            #       print("This is the mobile number",mobilenumber)
            elif lenmo4 == 1:
                mobilenumber = mo4[0]
            else:
                mobilenumber = ""
        #      print("There is no mobile number12")
        else:
            mobilenumber = ""
        # print("There is no mobile number34")

    else:
        mobi = re.findall(r"REGD. MOBILE NUMBER", stringempty)
        lenmobi = len(mobi)
        if lenmobi == 1:
            mo1 = re.findall(r"\D([91]{2}\d{10})\D", stringempty)
            mo2 = re.findall(r"\D([91]{2}\d{2}[X]{7}\d{1})\D", stringempty)
            mo3 = re.findall(r"\D([0]{1}\d{10})\D", stringempty)
            mo4 = re.findall(r"\D(\d[7/8/9]?\d{9})\D", stringempty)
            lenmo1 = len(mo1)
            lenmo2 = len(mo2)
            lenmo3 = len(mo3)
            lenmo4 = len(mo4)
            #             print("lenmo1",lenmo1)
            #             print("lenmo2",lenmo2)
            #             print("lenmo3",lenmo3)
            #             print("lenmo4",lenmo4)

            if lenmo1 == 1:
                mobilenumber = mo1[0]
            #    print("This is the mobile number",mobilenumber)
            elif lenmo2 == 1:
                mobilenumber = mo2[0]
            #   print("This is the mobile number",mobilenumber)
            elif lenmo3 == 1:
                mobilenumber = mo3[0]
            #     print("This is the mobile number",mobilenumber)
            elif lenmo4 == 1:
                mobilenumber = mo4[0]
            else:
                mobilenumber = ""
            #    print("There is no mobile number56")
        else:
            mono = re.findall(r"PHONE:", stringempty)
            lenmono = len(mono)
            if lenmono == 1:
                mo1 = re.findall(r"\D([91]{2}\d{10})\D", stringempty)
                mo2 = re.findall(r"\D([91]{2}\d{2}[X]{7}\d{1})\D", stringempty)
                mo3 = re.findall(r"\D(\d[7/8/9]?\d{9})\D", stringempty)
                mo4 = re.findall(r"\D([0]{1}\d{10})\D", stringempty)
                #                 mo4=re.findall(r"\D(\d[7/8/9]?\d{9})\D",stringempty)
                lenmo1 = len(mo1)
                lenmo2 = len(mo2)
                #                 print("lenmo2",len(mo2))
                #                 print("lenmo3",len(mo3))
                lenmo3 = len(mo3)
                lenmo4 = len(mo4)
                #                 print("here",lenmo4)
                if lenmo1 == 1:
                    mobilenumber = mo1[0]
                elif lenmo2 == 1:
                    mobilenumber = mo2[0]
                elif lenmo3 == 1:
                    mobilenumber = mo3[0]
                #         print("Correct",mobilenumber)
                elif lenmo4 == 1:
                    mobilenumber = mo4[0]
                #        print("wrong",mobilenumber)
                else:
                    mobilenumber = ""
            else:
                mobilenumber = ""
                lenmobile = len(mobilenumber)
                #   print("Correct",lenmobile)
                if lenmobile == 0:
                    mob = re.findall(r"MOBILE NO.", stringempty)
                    mo = len(mob)
                    if mo == 1:
                        mo1 = re.findall(r"\D([91]{2}\d{10})\D", stringempty)
                        mo2 = re.findall(r"\D([91]{2}\d{2}[X]{7}\d{1})\D", stringempty)
                        mo3 = re.findall(r"\D(\d[7/8/9]?\d{9})\D", stringempty)
                        mo4 = re.findall(r"\D([0]{1}\d{10})\D", stringempty)
                        #                 mo4=re.findall(r"\D(\d[7/8/9]?\d{9})\D",stringempty)
                        lenmo1 = len(mo1)
                        lenmo2 = len(mo2)
                        lenmo3 = len(mo3)
                        lenmo4 = len(mo4)
                        #                 print("here",lenmo4)
                        if lenmo1 == 1:
                            mobilenumber = mo1[0]
                        elif lenmo2 == 1:
                            mobilenumber = mo2[0]
                        elif lenmo3 == 1:
                            mobilenumber = mo3[0]
                        #              print("Correct",mobilenumber)
                        elif lenmo4 == 1:
                            mobilenumber = mo4[0]
                        #             print("wrong",mobilenumber)
                        else:
                            mobilenumber = ""
                    #            print("Mobile no is not present78")

                    else:
                        newmo = re.findall(r'PHONE             : [+91]{2}()\d{10}', stringofuppercase)
                        if len(newmo) >= 1:
                            mobilenumbers = newmo[0]
                            mobilenumber = str.replace(mobilenumbers, "PHONE             : ", "")
                        else:
                            mobile_number = re.findall(r'MOBILE NUMBER \d[7/8/9]?\d{9}', stringempty)
                            if len(mobile_number) >= 1:
                                mobilenumbers = mobile_number[0]
                                mobilenumber = str.replace(mobilenumbers, "MOBILE NUMBER", "")
                            else:
                                mobilenumber = ""
                else:
                    mobilenumber = ""

    return stringempty, stringofuppercase, mobi, mobilenumber
