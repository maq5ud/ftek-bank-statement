import re


def fetch_pan(stringempty):
    pannumber = re.findall(r"^[A-Z]{5}\d{4}[A-Z]{1}", stringempty)
    #     number=pannumber.search(stringempty)
    lenpannumber = len(pannumber)
    if lenpannumber >= 1:
        pancardno = pannumber[0]
        pancardnumber = pancardno
    #        print("Pan",pancardnumber)
    else:
        pannumber = re.findall(r"\D([A-Z]{2}[X]{6}\d{1}[A-Z]{1})\D", stringempty)
        lenpan = len(pannumber)
        if lenpan >= 1:
            pancard = pannumber[0]
            pancardnumber = pancard
        else:
            pancardnumber = ""
    #           print("Pan card number is not present")

    return stringempty, pannumber, pancardnumber
