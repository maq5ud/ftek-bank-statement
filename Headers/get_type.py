import re


def fetch_type(stringempty, stringofuppercase):
    accounttypecurrent = re.findall(r"CURRENT", stringempty)
    accounttypecurrent = re.findall(r"CURRENT ACCOUNT ", stringofuppercase)
    lenaccount = len(accounttypecurrent)
    # print("Len",lenaccount)
    if lenaccount == 1:
        acc_type = accounttypecurrent[0]
    # print("problem")
    else:
        accounttypesavings = re.findall(r"SAVINGS", stringempty)
        accounttypesavings = re.findall(r"SAVINGS", stringofuppercase)
        lenaccounttypesavings = len(accounttypesavings)
        #    print("Problem",lenaccounttypesavings)
        if lenaccounttypesavings >= 1:
            acc_type = accounttypesavings[0]
        #          print("This is the type of account",acc_type)

        else:
            accounttypecurrent = re.findall(r"CURRENT", stringempty)
            accounttypecurrent = re.findall(r": CURRENT", stringempty)
            #     accounttypecurrent=re.findall(r"Current",stringempty)
            lenaccounttypecurrent = len(accounttypecurrent)
            if lenaccounttypecurrent == 1:
                acctype = accounttypecurrent[0]
                acc_type = str.replace(acctype, ":", "")
            #       print("This is the type of account",acc_type)

            else:
                accounttyperegular = re.findall(r"REGULAR", stringempty)
                lenaccounttyperegular = len(accounttyperegular)
                if lenaccounttyperegular == 1:
                    acc_type = accounttyperegular[0]
                #       print("This is the type of account",acc_type)
                else:
                    accounttypeoverdraft = re.findall(r"O/D", stringempty)
                    lenaccounttypeoverdraft = len(accounttypeoverdraft)
                    if lenaccounttypeoverdraft == 1:
                        acc_type = accounttypeoverdraft[0]
                    #            print("This is the type of the account",acc_type)
                    else:
                        accounttypecurrentacc = re.findall(r"CURRENT ", stringempty)
                        lenaccounttypecurrentacc = len(accounttypecurrentacc)
                        #                     print("IDBI acctype",lenaccounttypecurrentacc)
                        if lenaccounttypecurrentacc >= 1:
                            acc_type = accounttypecurrentacc[0]
                        #           print("This is the type of the account",acc_type)
                        else:
                            accounttypemaxima = re.findall(r"SAVING ACCOUNT-INDUS MAXIMA", stringempty)
                            lenaccounttypemaxima = len(accounttypemaxima)
                            if lenaccounttypemaxima >= 1:
                                acc_type = accounttypemaxima[0]
                            #         print("This is the type of maxima",acc_type)
                            else:
                                accounttypecashcreditacc = re.findall(r"\D(CASH CREDIT ACCOUNT)\D", stringempty)
                                lenaccounttypecashcreditacc = len(accounttypecashcreditacc)
                                if lenaccounttypecashcreditacc >= 1:
                                    acc_type = accounttypecashcreditacc[0]
                                #            print("TCash Credit Account")
                                else:
                                    accounttypedcbbusinesssaver = re.findall(r'DCB BUSINESS SAVER A/C', stringempty)
                                    accounttypedcbbusinesssaver = re.findall(r'DCB BUSINESS SAVER A/C',
                                                                             stringofuppercase)
                                    lenaccounttypedcbbusinesssaver = len(accounttypedcbbusinesssaver)
                                    if lenaccounttypedcbbusinesssaver == 1:
                                        acc_type = accounttypedcbbusinesssaver[0]
                                    #               print("This is the type of account",acc_type)
                                    else:
                                        accounttypegenral = re.findall(r"CASH CREDIT GENERAL", stringempty)
                                        lenaccounttypegenral = len(accounttypegenral)
                                        if lenaccounttypegenral >= 1:
                                            acc_type = accounttypegenral[0]
                                        #                  print(acc_type)
                                        else:
                                            accounttypecc = re.findall(r" CC FOR PUBLIC", stringempty)
                                            lenaccounttypecc = len(accounttypecc)
                                            if lenaccounttypecc >= 1:
                                                acc_type = accounttypecc[0]
                                            else:
                                                accountypeccupper = re.findall(r" CC FOR PUBLIC", stringofuppercase)
                                                lenaccountypeccupper = len(accountypeccupper)
                                                if lenaccountypeccupper >= 1:
                                                    acc_type = accountypeccupper[0]
                                                else:
                                                    acc_typeeconomy = re.findall(r"CA-KVB-ECONOMY", stringempty)
                                                    lenacc_typeeconomy = len(acc_typeeconomy)
                                                    if lenacc_typeeconomy >= 1:
                                                        acc_type = acc_typeeconomy[0]
                                                    else:
                                                        acc_typesod = re.findall(r'SOD', stringempty)
                                                        acc_typesod = re.findall(r'SOD', stringofuppercase)
                                                        lenacc_typesod = len(acc_typesod)
                                                        #  print("SOD",lenacc_typesod)
                                                        if lenacc_typesod >= 1:
                                                            acc_type = acc_typesod[0]
                                                        else:
                                                            acc_typeHSS = re.findall(r'HSS-GEN-PUB-IND-URBAN-INR',
                                                                                     stringofuppercase)
                                                            lenacc_typeHSS = len(acc_typeHSS)
                                                            # print("HSS-GEN-PUB-IND-URBAN-INR",lenacc_typeHSS)
                                                            if lenacc_typeHSS >= 1:
                                                                acc_type = acc_typeHSS[0]
                                                            else:
                                                                acc_typeCD = re.findall(r"CD-GEN-PUB-IND-RURAL-INR",
                                                                                        stringofuppercase)
                                                                lenacc_typeCD = len(acc_typeCD)
                                                                # print("CD-GEN-PUB-IND-RURAL-INR",lenacc_typeCD)
                                                                if lenacc_typeCD >= 1:
                                                                    acc_type = acc_typeCD[0]
                                                                else:
                                                                    acc_typehss = re.findall(
                                                                        r"HSS-GEN-PUB-IND-RURAL-INR", stringofuppercase)
                                                                    lenacc_typehss = len(acc_typehss)
                                                                    # print("CD-GEN-PUB-IND-RURAL-INR",lenacc_typehss)
                                                                    if lenacc_typehss >= 1:
                                                                        acc_type = acc_typehss[0]
                                                                    else:
                                                                        acc_typecd = re.findall(
                                                                            r"CD-GEN-PUB-IND-URBAN-INR",
                                                                            stringofuppercase)
                                                                        lenacc_typecd = len(acc_typecd)
                                                                        # print("CD-GEN-PUB-IND-URBAN-INR",lenacc_typecd)
                                                                        if lenacc_typecd >= 1:
                                                                            acc_type = acc_typecd[0]
                                                                        else:
                                                                            acc_typeMetro = re.findall(
                                                                                r"CD-GEN-PUB-OTH-METRO-INR",
                                                                                stringofuppercase)
                                                                            lenacc_typeMetro = len(acc_typeMetro)
                                                                            # print("CD-GEN-PUB-OTH-METRO-INR",lenacc_typeMetro)
                                                                            if lenacc_typeMetro >= 1:
                                                                                acc_type = acc_typeMetro[0]
                                                                            else:
                                                                                acc_typeSEMIURBAN = re.findall(
                                                                                    r"CD-GEN-PUB-OTH-SEMIURBAN-INR",
                                                                                    stringofuppercase)
                                                                                lenacc_typeSEMIURBAN = len(
                                                                                    acc_typeSEMIURBAN)
                                                                                # print("CD-GEN-PUB-OTH-SEMIURBAN-INR",lenacc_typeSEMIURBAN)
                                                                                if lenacc_typeSEMIURBAN >= 1:
                                                                                    acc_type = acc_typeSEMIURBAN[0]
                                                                                else:
                                                                                    acc_typePriorityNF = re.findall(
                                                                                        r"CC-CENT MUDRA UNDER PRIORITYNF",
                                                                                        stringofuppercase)
                                                                                    lenacc_typePriorityNF = len(
                                                                                        acc_typePriorityNF)
                                                                                    # print("CC-CENT MUDRA UNDER PRIORITYNF",lenacc_typePriorityNF)
                                                                                    if lenacc_typePriorityNF >= 1:
                                                                                        acc_type = acc_typePriorityNF[0]
                                                                                    else:
                                                                                        acc_typeOTH = re.findall(
                                                                                            r"CD-GEN-PUB-OTH-URBAN-INR",
                                                                                            stringofuppercase)
                                                                                        lenacc_typeOTH = len(
                                                                                            acc_typeOTH)
                                                                                        # print("CD-GEN-PUB-OTH-URBAN-INR",lenacc_typeOTH)
                                                                                        if lenacc_typeOTH >= 1:
                                                                                            acc_type = acc_typeOTH[0]
                                                                                        else:
                                                                                            acc_typeDynamic = re.findall(
                                                                                                r"DYNAMIC BUSINESS",
                                                                                                stringempty)
                                                                                            lenacc_typeDynamic = len(
                                                                                                acc_typeDynamic)
                                                                                            # print("DYNAMIC BUSINESS",acc_typeDynamic)
                                                                                            if lenacc_typeDynamic >= 1:
                                                                                                acc_type = \
                                                                                                    acc_typeDynamic[0]
                                                                                            else:
                                                                                                acc_Biz = re.findall(
                                                                                                    r"CA BIZ ADVANTAGE25000 MAB",
                                                                                                    stringofuppercase)
                                                                                                acc_Biz = re.findall(
                                                                                                    r"CA BIZ ADVANTAGE25000 MAB",
                                                                                                    stringempty)
                                                                                                lenacc_Biz = len(
                                                                                                    acc_Biz)
                                                                                                # print("CA BIZ ADVANTAGE25000 MAB",acc_Biz)
                                                                                                if lenacc_Biz >= 1:
                                                                                                    acc_type = acc_Biz[
                                                                                                        0]
                                                                                                else:
                                                                                                    acc_Stand = re.findall(
                                                                                                        r"CA BIZ STAND 5000 MAB",
                                                                                                        stringofuppercase)
                                                                                                    acc_Stand = re.findall(
                                                                                                        r"CA BIZ STAND 5000 MAB",
                                                                                                        stringempty)
                                                                                                    lenacc_Stand = len(
                                                                                                        acc_Stand)
                                                                                                    # print("CA BIZ STAND 5000 MAB",acc_Stand)
                                                                                                    if lenacc_Stand >= 1:
                                                                                                        acc_type = \
                                                                                                            acc_Stand[0]
                                                                                                    else:
                                                                                                        acc_SB = re.findall(
                                                                                                            r"SB Standard - 5000 MAB",
                                                                                                            stringofuppercase)
                                                                                                        lenacc_SB = len(
                                                                                                            acc_SB)
                                                                                                        # print("SB Standard - 5000 MAB",acc_SB)
                                                                                                        if lenacc_SB >= 1:
                                                                                                            acc_type = \
                                                                                                                acc_SB[
                                                                                                                    0]
                                                                                                        else:
                                                                                                            acc_REGULAR = re.findall(
                                                                                                                r"SAVINGS BANK - REGULAR",
                                                                                                                stringofuppercase)
                                                                                                            acc_REGULAR = re.findall(
                                                                                                                r"SAVINGS BANK - REGULAR",
                                                                                                                stringempty)
                                                                                                            lenacc_REGULAR = len(
                                                                                                                acc_REGULAR)
                                                                                                            if lenacc_REGULAR >= 1:
                                                                                                                acc_type = \
                                                                                                                    acc_REGULAR[
                                                                                                                        0]
                                                                                                            else:
                                                                                                                SAVING = re.findall(
                                                                                                                    r"SAVING ACCOUNTS",
                                                                                                                    stringofuppercase)
                                                                                                                acc_SAVING = re.findall(
                                                                                                                    r"SAVING ACCOUNTS",
                                                                                                                    stringempty)
                                                                                                                lenacc_SAVING = len(
                                                                                                                    acc_SAVING)
                                                                                                                if lenacc_SAVING >= 1:
                                                                                                                    acc_type = \
                                                                                                                        acc_SAVING[
                                                                                                                            0]
                                                                                                                else:
                                                                                                                    acc_GLOBAL = re.findall(
                                                                                                                        r"CA', 'GLOBALL",
                                                                                                                        stringofuppercase)
                                                                                                                    acc_GLOBAL = re.findall(
                                                                                                                        r"CA', 'GLOBAL",
                                                                                                                        stringempty)
                                                                                                                    lenacc_GLOBAL = len(
                                                                                                                        acc_GLOBAL)
                                                                                                                    if lenacc_GLOBAL >= 1:
                                                                                                                        acc_type = \
                                                                                                                            acc_GLOBAL[
                                                                                                                                0]
                                                                                                                    else:
                                                                                                                        acc_LARGE = re.findall(
                                                                                                                            r"CA- LARGE RETAILERS AND', 'DISTRIBUTORS",
                                                                                                                            stringofuppercase)
                                                                                                                        acc_LARGE = re.findall(
                                                                                                                            r"CA- LARGE RETAILERS AND', 'DISTRIBUTORS",
                                                                                                                            stringempty)
                                                                                                                        lenacc_LARGE = len(
                                                                                                                            acc_LARGE)
                                                                                                                        if lenacc_LARGE >= 1:
                                                                                                                            acc_type = \
                                                                                                                                acc_LARGE[
                                                                                                                                    0]
                                                                                                                        else:
                                                                                                                            acc_EASY = re.findall(
                                                                                                                                r"EASYACCESS-PRIME",
                                                                                                                                stringofuppercase)
                                                                                                                            acc_EASY = re.findall(
                                                                                                                                r"EASYACCESS-PRIME",
                                                                                                                                stringofuppercase)
                                                                                                                            lenacc_EASY = len(
                                                                                                                                acc_EASY)
                                                                                                                            if lenacc_EASY >= 1:
                                                                                                                                acc_type = \
                                                                                                                                    acc_EASY[
                                                                                                                                        0]
                                                                                                                            else:
                                                                                                                                acc_SELECT = re.findall(
                                                                                                                                    r"CA', 'BUSINESS SELECT",
                                                                                                                                    stringofuppercase)
                                                                                                                                acc_SELECT = re.findall(
                                                                                                                                    r"CA', 'BUSINESS SELECT",
                                                                                                                                    stringempty)
                                                                                                                                lenacc_SELECT = len(
                                                                                                                                    acc_SELECT)
                                                                                                                                if lenacc_SELECT >= 1:
                                                                                                                                    acc_type = \
                                                                                                                                        acc_SELECT[
                                                                                                                                            0]
                                                                                                                                else:
                                                                                                                                    acc_VARTHAGA = re.findall(
                                                                                                                                        r"SOD', 'VARTHAGA MITHRA",
                                                                                                                                        stringofuppercase)
                                                                                                                                    acc_VARTHAGA = re.findall(
                                                                                                                                        r"SOD', 'VARTHAGA MITHRA",
                                                                                                                                        stringempty)
                                                                                                                                    lenacc_VARTHAGA = len(
                                                                                                                                        acc_VARTHAGA)
                                                                                                                                    if lenacc_VARTHAGA >= 1:
                                                                                                                                        acc_type = \
                                                                                                                                            acc_VARTHAGA[
                                                                                                                                                0]
                                                                                                                                    else:
                                                                                                                                        accRESIDENT = re.findall(
                                                                                                                                            r"CA-RESIDENT",
                                                                                                                                            stringofuppercase)
                                                                                                                                        accRESIDENT = re.findall(
                                                                                                                                            r"CA-RESIDENT",
                                                                                                                                            stringempty)
                                                                                                                                        lenaccRESIDENT = len(
                                                                                                                                            accRESIDENT)
                                                                                                                                        if lenaccRESIDENT >= 1:
                                                                                                                                            acc_type = \
                                                                                                                                                accRESIDENT[
                                                                                                                                                    0]
                                                                                                                                        else:
                                                                                                                                            acc_CORP = re.findall(
                                                                                                                                                r"SB', 'KVB CORP",
                                                                                                                                                stringofuppercase)
                                                                                                                                            acc_CORP = re.findall(
                                                                                                                                                r"SB', 'KVB CORP",
                                                                                                                                                stringempty)
                                                                                                                                            lenacc_CORP = len(
                                                                                                                                                acc_CORP)
                                                                                                                                            if lenacc_CORP >= 1:
                                                                                                                                                acc_type = \
                                                                                                                                                    acc_CORP[
                                                                                                                                                        0]
                                                                                                                                            else:
                                                                                                                                                acc_FREEDOM = re.findall(
                                                                                                                                                    r"FREEDOM CURRENT",
                                                                                                                                                    stringofuppercase)
                                                                                                                                                acc_FREEDOM = re.findall(
                                                                                                                                                    r"FREEDOM CURRENT",
                                                                                                                                                    stringempty)
                                                                                                                                                lenacc_FREEDOM = len(
                                                                                                                                                    acc_FREEDOM)
                                                                                                                                                if lenacc_FREEDOM >= 1:
                                                                                                                                                    acc_type = \
                                                                                                                                                        acc_FREEDOM[
                                                                                                                                                            0]
                                                                                                                                                else:
                                                                                                                                                    accNONRURAL = re.findall(
                                                                                                                                                        r"CA-GEN-PUB-IND-NONRURAL-INR",
                                                                                                                                                        stringofuppercase)
                                                                                                                                                    accNONRURAL = re.findall(
                                                                                                                                                        r"CA-GEN-PUB-IND-NONRURAL-INR",
                                                                                                                                                        stringempty)
                                                                                                                                                    lenaccNONRURAL = len(
                                                                                                                                                        accNONRURAL)
                                                                                                                                                    if lenaccNONRURAL >= 1:
                                                                                                                                                        acc_type = \
                                                                                                                                                            accNONRURAL[
                                                                                                                                                                0]
                                                                                                                                                    else:
                                                                                                                                                        acc_Stocks = re.findall(
                                                                                                                                                            r"MC-C C STOCKS(SBF)",
                                                                                                                                                            stringofuppercase)
                                                                                                                                                        acc_Stocks = re.findall(
                                                                                                                                                            r"MC-C C STOCKS(SBF)",
                                                                                                                                                            stringempty)
                                                                                                                                                        lenaacc_Stocks = len(
                                                                                                                                                            acc_Stocks)
                                                                                                                                                        if lenaacc_Stocks >= 1:
                                                                                                                                                            acc_type = \
                                                                                                                                                                acc_Stocks[
                                                                                                                                                                    0]
                                                                                                                                                        else:
                                                                                                                                                            greaterconv = re.findall(
                                                                                                                                                                r'GREATER CONVENIENT CA SCH-OTH',
                                                                                                                                                                stringofuppercase)
                                                                                                                                                            if len(
                                                                                                                                                                    greaterconv) >= 1:
                                                                                                                                                                acc_type = \
                                                                                                                                                                    greaterconv[
                                                                                                                                                                        0]
                                                                                                                                                            else:
                                                                                                                                                                acc_type = ""

    return stringempty, stringofuppercase, accounttypecurrent, acc_type
