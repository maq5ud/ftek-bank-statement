import enum
import os

from os.path import *

from Helper.helper import *

# Enum for size units
class SIZE_UNIT(enum.Enum):
    BYTES = 1
    KB = 2
    MB = 3
    GB = 4


def convert_unit(size_in_bytes, unit):
    """ Convert the size from bytes to other units like KB, MB or GB"""
    if unit == SIZE_UNIT.KB:
        return size_in_bytes / 1024
    elif unit == SIZE_UNIT.MB:
        return size_in_bytes / (1024 * 1024)
    elif unit == SIZE_UNIT.GB:
        return size_in_bytes / (1024 * 1024 * 1024)
    else:
        return size_in_bytes




def get_file_size(filename, user_id, size_type=SIZE_UNIT.KB):
    """ Get file in size in given unit like KB, MB or GB"""
    
    path =  getFilePath(filename, user_id) 

    size = getsize(path)

    return convert_unit(size, size_type)


