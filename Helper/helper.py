import os
from os.path import *


def getPath(user_id):
    parentPath = abspath(join(abspath(__file__), os.pardir, os.pardir))

    path = abspath(join(parentPath, 'case_1', str(user_id)))

    return path


def getFilePath(filename, user_id):
    path = abspath(join(getPath(user_id), filename))

    return path
