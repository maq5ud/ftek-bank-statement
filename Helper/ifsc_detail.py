import requests


def ifsc_from_razorpay(IFSC_Code):
    URL = "https://ifsc.razorpay.com/"

    return requests.get(URL + IFSC_Code).json()
