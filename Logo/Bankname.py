def bankname(text):
    if text == "AU Small Finance Bank":
        return "AU Small Finance Bank"
    if text == "axis bank":
        return "Axis Bank"
    if text == "Abhyudaya Co-operative Bank":
        return "Abhyudaya Co-operative Bank"
    if text == "Allahabad bank":
        return "Allahabad Bank"
    if text == "Andhra bank":
        return "Andhra Bank"
    if text == "bandhan":
        return "Bandhan Bank"
    if text == "bank of baroda":
        return "Bank of Baroda"
    if text == "bank of maharashtra":
        return "Bank of Maharashtra"
    if text == "Bassein Catholic Co-operative Bank":
        return "Bassein Catholic Co-operative Bank"
    if text == "Bharat":
        return "Bharath Co-operative Bank"
    if text == "Canara Bank":
        return "Canara Bank"
    if text == "Central bank of india":
        return "Central Bank Of India"
    if text == "coorporation":
        return "Corporation Bank"
    if text == "DCB Bank":
        return "DCB Bank"
    if text == "Dena bank":
        return "Dena Bank"
    if text == "Fingrowth Co-operative Bank":
        return "Fingrowth Co-operative Bank"
    if text == "Greater Bombay Co-operative Bank":
        return "Greater Bombay Co-operative Bank"
    if text == "hdfc":
        return "HDFC Bank"
    if text == "icici":
        return "ICICI Bank"
    if text == "IDBI":
        return "IDBI"
    if text == "IDFC first":
        return "IDFC FIRST Bank"
    if text == "indian bank":
        return "Indian Bank"
    if text == "Indian Overrseas":
        return "Indian Overseas Bank"
    if text == "induslnd":
        return "Indusind Bank"
    if text == "Karnataka":
        return "Karnataka Bank"
    if text == "Karur Vysya Bank":
        return "Karur Vysya Bank"
    if text == "kotak mahindra":
        return "Kotak Mahindra Bank"
    if text == "Lakshmi Vilas Bank":
        return "Laxmi Vilas Bank"
    if text == "NKGSB Co-operative Bank":
        return "NKGSB Co-operative Bank"
    if text == "Oriental Bank of Commerce":
        return "Oriental Bank of Commerce"
    if text == "punjab":
        return "Punjab National Bank"
    if text == "punjab and sind bank":
        return "Punjab & Sind Bank"
    if text == "RBL Bank":
        return "RBL Bank"
    if text == "Saraswat Bank":
        return "Saraswat Co-operative Bank"
    if text == "sbi":
        return "State Bank of India"
    if text == "South Indian bank":
        return "South Indian bank"
    if text == "Standard Chartered Bank":
        return "Standard Chartered Bank"
    if text == "syndicate":
        return "Syndicate Bank"
    if text == "UCO":
        return "UCO Ban"
    if text == "Ujjivan Small Finance Bank":
        return "Ujjivan Small Finance Bank"
    if text == "union":
        return "Union Bank of India"
    if text == "united bank of india":
        return "United Bank of India"
    if text == "vijaya":
        return "Vijaya Bank"
    if text == "Yes Bank":
        return "Yes Bank"
