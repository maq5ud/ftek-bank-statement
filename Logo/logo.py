import cv2
import tensorflow as tf
from tensorflow import keras
import numpy as np
import fitz
from IPython.display import Image, display
from tensorflow.keras.preprocessing.image import img_to_array, load_img
from collections import deque
import pickle
from keras.models import load_model
from Database.Id_from_name import bankname_id
from Bankname import bankname


def logo(filename, password):
    doc = fitz.open(filename)  # put your pdf file here
    for i in range(len(doc)):
        for img in doc.getPageImageList(i):
            xref = img[0]
            pix = fitz.Pixmap(doc, xref)
            if pix.n < 5:  # this is GRAY or RGB
                # pix.writePNG("p0-1.jpg" % (i, xref))  # this store pdf extrated image in local storage
                pix.writePNG("p0-1.jpg")
            else:  # CMYK: convert to RGB first
                pix1 = fitz.Pixmap(fitz.csRGB, pix)  # this store pdf extrated image in local storage
                # pix1.writePNG("p%s-%s.jpg" % (i, xref))
                pix1.writePNG("p0-1.jpg")
                pix1 = None
            pix = None

    # this is your image which extrated from pdf that is shown below
    image = 'p0-1.jpg'
    # display(image)

    print("Load model.........................")
    loaded_model = load_model('modelll.h5')  # put uploaded H5 file name here
    print("Model loaded....")

    img = cv2.imread(image)
    label = "lb.pickle"

    lb = pickle.loads(open("label", "rb").read())

    mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
    Q = deque(maxlen=128)

    vs = cv2.VideoCapture(image)

    (W, H) = (None, None)

    while True:
        (grabbed, frame) = vs.read()

        if not grabbed:
            break

        if W is None or H is None:
            (H, W) = frame.shape[:2]

        output = frame.copy()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.resize(frame, (224, 224)).astype("float32")
        frame -= mean

        preds = loaded_model.predict(np.expand_dims(frame, axis=0))[0]
        Q.append(preds)

        results = np.array(Q).mean(axis=0)
        i = np.argmax(results)
        label = lb.classes_[i]

        # text = label.upper()
        text = label
        name = bankname(text)
        # print(text)
        """cv2.putText(output, text, (4, 4), cv2.FONT_HERSHEY_SIMPLEX,
            0.25, (200,255,155), 2)

        cv2.imshow("Output",output)"""
        key = cv2.waitKey(10) & 0xFF

        if key == ord("q"):
            break

    vs.release()

    Id = bankname_id(name)
    # print(name)
    return Id


# logo('HDFC bank Statement.pdf', "")
