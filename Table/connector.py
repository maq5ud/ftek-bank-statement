from Table.set1.statement_parsing_01 import state_file01
from Table.set1.statement_parsing_02 import state_file02
from Table.set1.statement_parsing_03 import state_file03
from Table.set1.statement_parsing_04 import state_file04
from Table.set1.statement_parsing_05 import state_file05
from Table.set1.statement_parsing_06 import state_file06
from Table.set1.statement_parsing_07 import state_file07
from Table.set1.statement_parsing_08 import state_file08
from Table.set1.statement_parsing_09 import state_file09
from Table.set1.statement_parsing_10 import state_file10
from Table.set1.statement_parsing_11 import state_file11
from Table.set1.statement_parsing_12 import state_file12
from Table.set2.statement_parsing_13 import state_file13
from Table.set2.statement_parsing_14 import state_file14
from Table.set2.statement_parsing_15 import state_file15
from Table.set2.statement_parsing_16 import state_file16
from Table.set2.statement_parsing_17 import state_file17
from Table.set2.statement_parsing_18 import state_file18
from Table.set2.statement_parsing_19 import state_file19
from Table.set2.statement_parsing_20 import state_file20
from Table.set2.statement_parsing_21 import state_file21
from Table.set2.statement_parsing_22 import state_file22
from Table.set2.statement_parsing_23 import state_file23
from Table.set2.statement_parsing_24 import state_file24
from Table.set2.statement_parsing_25 import state_file25
from Table.set2.statement_parsing_26 import state_file26
from Table.set2.statement_parsing_27 import state_file27
from Table.set2.statement_parsing_28 import state_file28
from Table.set2.statement_parsing_29 import state_file29
from Table.set2.statement_parsing_30 import state_file30
from Table.set2.statement_parsing_31 import state_file31
from Table.set2.statement_parsing_32 import state_file32
from Table.set2.statement_parsing_33 import state_file33
from Table.set2.statement_parsing_34 import state_file34
from Table.set2.statement_parsing_35 import state_file35
from Table.set2.statement_parsing_36 import state_file36
from Table.set2.statement_parsing_37 import state_file37
from Table.set2.statement_parsing_38 import state_file38
from Table.set2.statement_parsing_39 import state_file39
from Table.set2.statement_parsing_40 import state_file40
from Table.set2.statement_parsing_41 import state_file41
from Table.set2.statement_parsing_42 import state_file42
from Table.set2.statement_parsing_43 import state_file43
from Table.set2.statement_parsing_44 import state_file44
from Table.set2.statement_parsing_45 import state_file45
from Table.set2.statement_parsing_46 import state_file46
from Table.set2.statement_parsing_47 import state_file47
from Table.set2.statement_parsing_48 import state_file48


def table_connector(job_id, analytics_id, bankname, filename, password):
    print("bankname", bankname)
    if bankname == "State Bank of India":
        result = state_file01(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Axis Bank":
        result = state_file02(job_id, analytics_id, bankname, filename, password)
    elif bankname == "HDFC Bank":
        result = state_file03(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Kotak Mahindra Bank":
        result = state_file04(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bank of Maharashtra":
        result = state_file05(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Union Bank of India":
        result = state_file06(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Punjab National Bank":
        result = state_file07(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bank of Baroda":
        result = state_file08(job_id, analytics_id, bankname, filename, password)
    elif bankname == "ICICI Bank":
        result = state_file09(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Punjab & Sind Bank":
        result = state_file10(job_id, analytics_id, bankname, filename, password)
    elif bankname == "IDBI":
        result = state_file11(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Yes Bank":
        result = state_file12(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Allahabad Bank":
        result = state_file13(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Andhra Bank":
        result = state_file14(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bank of India":
        result = state_file15(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Canara Bank":
        result = state_file16(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Central Bank Of India":
        result = state_file17(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Corporation Bank":
        result = state_file18(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Federal Bank":
        result = state_file19(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Indian Bank":
        result = state_file20(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Indusind Bank":
        result = state_file21(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Syndicate Bank":
        result = state_file22(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Vijaya Bank":
        result = state_file23(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bandhan Bank":
        result = state_file24(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Dena Bank":
        result = state_file25(job_id, analytics_id, bankname, filename, password)
    elif bankname == "IDFC FIRST Bank":
        result = state_file26(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Indian Overseas Bank":
        result = state_file27(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Karnataka Bank":
        result = state_file28(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Karur Vysya Bank":
        result = state_file29(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Oriental Bank of Commerce":
        result = state_file30(job_id, analytics_id, bankname, filename, password)
    elif bankname == "RBL Bank":
        result = state_file31(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Saraswat Co-operative Bank":
        result = state_file32(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bharath Co-operative Bank":
        result = state_file33(job_id, analytics_id, bankname, filename, password)
    elif bankname == "UCO Bank":
        result = state_file34(job_id, analytics_id, bankname, filename, password)
    elif bankname == "AU Small Finance Bank":
        result = state_file35(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Ujjivan Small Finance Bank":
        result = state_file36(job_id, analytics_id, bankname, filename, password)
    elif bankname == "United Co-operative Bank":
        result = state_file37(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Fingrowth Co-operative Bank":
        result = state_file38(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Greater Bombay Co-operative Bank":
        result = state_file39(job_id, analytics_id, bankname, filename, password)
    elif bankname == "NKGSB Co-operative Bank":
        result = state_file40(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bangalore City Co-operative Bank":
        result = state_file41(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Abhyudaya Co-operative Bank":
        result = state_file42(job_id, analytics_id, bankname, filename, password)
    elif bankname == "DCB Bank":
        result = state_file43(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Equitas Small Finance Bank":
        result = state_file44(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Ujjivan Small Finance Bank":
        result = state_file45(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Laxmi Vilas Bank":
        result = state_file46(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Gopinath Patil Parsik Janata Sahakari Bank":
        result = state_file47(job_id, analytics_id, bankname, filename, password)
    elif bankname == "Bangalore City Co-operative Bank":
        result = state_file48(job_id, analytics_id, bankname, filename, password)

    return result
