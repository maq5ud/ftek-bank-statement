import pandas as pd
import numpy as np
import time

from PyPDF2 import PdfFileReader
from tabula.io import read_pdf
import camelot
import re
import math, operator, functools
import datetime
from Table.set1.common_parsing import lattice_parsing


# def state_file01(job_id,analytics_id,bankname,filename,password):
# 	df = lattice_parsing(filename,password)
# 	return df

def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def sbi_p1(f, pas):
    print("STATEMENT PARSING 01")
    tables = read_pdf(f,
                      lattice=True,
                      pages="all",
                      silent=True,
                      multiple_tables=True,
                      password=pas,
                      pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nSBI Column Headers Missing")
                pass

    # print(df.columns)
    try:
        mode = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        print("\nPost Date columns present")
        df = sbi_p2(f, pas)
    except:
        df = sbi_p1_process(df)
        pass

    return df


def sbi_p1_process(df):
    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
        print("DEP1")
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
            print("DEP2")
        except:
            print("DEP NOT FOUND")
            pass
    print("LLLLL", dep)
    df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})
    print("!!!!!!!!!!!")
    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        for i in df.index:
            #     # create new column
            #     if "Dr" in df["new_column"][i].astype(str):
            #         df[bal][i] = df[bal][i]*-1
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)
    return df


# df.to_csv("check.csv", index=0)

def sbi_p2_pro(f, i, pas):
    tables = camelot.read_pdf(f, pages=str(i))
    tmp = pd.DataFrame();
    if len(tables) != 0:
        tmp = tables[1].df
        if tmp.shape[1] == 7:
            tmp.columns = ["Post Date", "Value Date", "Details", "Chq.No", "Debit", "Credit", "Balance"]
            tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
            try:
                idx2 = [c for c in tmp[
                    tmp.apply(lambda row: row.astype(str).str.lower().str.contains('brought forward').any(),
                              axis=1) == True].index][0]
                tmp = tmp.iloc[idx2 + 1:, :];
                tmp.reset_index(drop=True, inplace=True)
            # tmp.drop(tmp.index[:idx2+1], inplace=True, reset_index=True)
            except:
                pass
            try:
                idx2 = [c for c in tmp[
                    tmp.apply(lambda row: row.astype(str).str.lower().str.contains('carried forward').any(),
                              axis=1) == True].index][0]
                tmp = tmp.iloc[:idx2, :];
                tmp.reset_index(drop=True, inplace=True)
            # tmp.drop(tmp.index[idx2:], inplace=True, reset_index=True)
            except:
                pass
        else:
            print("Another pattern found")
    return tmp


def sbi_p2(f, pas):
    pdf = PdfFileReader(open(f, 'rb'))
    if pdf.isEncrypted:
        pdf.decrypt(pas)
    else:
        pass
    total_cnt = pdf.getNumPages()
    df = pd.DataFrame();
    for i in range(1, total_cnt + 1):
        tmp = sbi_p2_pro(f, i, pas)
        df = pd.concat([df, tmp]).reset_index(drop=True)

    print(df)

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        print("\nBalance columns missing 2")

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
            except:
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    df['Wdl1'] = df[wdl].fillna(
        df[dep].astype(str).apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(
            float))
    df['flag'] = df.iloc[:, 0].astype(str) + df['Wdl1'].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))

    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannannan' in row else row).fillna(method='ffill')

    df[narr] = df.groupby('flag')[narr].transform(lambda x: ''.join(x))
    df = df.drop_duplicates(['flag'], keep='first').iloc[0:, :-3].reset_index(drop=True)

    try:
        for i in df.index:
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            if df["bal"][i] == "Cr":
                df[bal][i] = df[bal][i]
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)

    # df.to_csv("check.csv", index=0)
    print("parsed")
    return df


def sbi_p3(f, pas):
    tables = read_pdf(f,
                      lattice=True,
                      pages="all",
                      silent=True,
                      multiple_tables=True,
                      password=pas,
                      pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nSBI Column Headers Missing")
                if len(df.columns) == 5:
                    df.columns = [["Date", "Narration", "Debit", "Credit", "Balance"]]
                else:
                    pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    print(df.columns)

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            df["CHQ"] = np.nan
            chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
            pass

    try:
        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                    except:
                        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        print("\nBalance columns missing 3")

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                        "").replace(
            "DR", "")).astype(float)

    try:
        for i in df.index:
            if df["bal"][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    datelist = []
    try:
        for i in df["Xns Date"]:
            match = re.search('\d{2}-\w{3}-\d{2}', i)
            datelist.append(match[0])
    except:
        for i in df["Xns Date"]:
            match = re.search('\d{2}/\d{2}/\d{2}', i)
            datelist.append(match[0])

    df["Xns Date"] = datelist

    try:
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][1] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    # print(df.head())
    return df


def state_file01(job_id, analytics_id, bankname, filename, password):
    try:
        df = sbi_p1(filename, password)
        print("\n In state_file01->sbi_p1")
    except:
        df = sbi_p3(filename, password)
        print("\n In state_file01->sbi_p3")

    return df
