import pandas as pd
import numpy as np
import time
import tabula
import camelot
import re
import math, operator, functools
import datetime
from PyPDF2 import PdfFileWriter, PdfFileReader
from Table.set1.common_parsing import lattice_parsing

pd.set_option('mode.chained_assignment', None)


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def axis_tab(f, pas):
    global dep
    print("STATEMENT PARSING 02")
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)
    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                try:
                    idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                                  axis=1) == True].index if c in df[
                               df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                        axis=1) == True].index][0]
                    df.columns = df.iloc[idx]
                    df = df.iloc[idx + 1:, :]
                    df.reset_index(drop=True, inplace=True)
                except:
                    print("\nAxis Column Headers Missing")
                    pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction type').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('reason for return').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction total').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    df = axis_process(df)
    return df


def axis_process(df):
    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\nBalance columns missing")

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        drcr = [c for c in df.columns if "DR" in str(c).upper()][0]
        df[amt] = df[amt].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\Amount columns missing")

    try:
        if amt in df.columns:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for i, j in enumerate(df[drcr]):
                if "DR" in j:
                    df["Debits"][i] = df[amt][i]
                elif "CR" in j:
                    df["Credits"][i] = df[amt][i]
                else:
                    pass
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[[bal, wdl, dep]] = df[[bal, wdl, dep]].replace({"NA": np.nan, "-": np.nan})
    except:
        pass

    try:
        df[wdl] = df[wdl].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    try:
        df[dep] = df[dep].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]

    df.drop_duplicates(inplace=True)

    return df


def axis_came(f, pas):
    tables = camelot.read_pdf(f, pages='1,2')
    if len(tables) != 0:
        dt = pd.DataFrame()
        tmp = pd.DataFrame()
        first_check = tables[0].df
        second_check = tables[1].df
        # print(first_check.shape[1])
        # print(second_check.shape[1])
        if first_check.shape[1] > 5:
            try:
                idx = [c for c in first_check[
                    first_check.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in first_check[
                           first_check.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                             axis=1) == True].index][0]
                first_check.columns = first_check.iloc[idx]
            except:
                try:
                    idx = [c for c in first_check[
                        first_check.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                          axis=1) == True].index if c in dt[
                               dt.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                        axis=1) == True].index][0]
                    first_check.columns = first_check.iloc[idx]
                except:
                    print("\nAxis-Column headers missing")
            tables2 = camelot.read_pdf(f, pages='all')
            print("\nlength of first_check:", len(first_check.columns))
            if len(first_check.columns) == 7:
                print("Column length is 7")
                for i in range(len(tables2)):
                    tmp = tables2[i].df
                    if tmp.shape[1] < 5:
                        print("\nDiiferent structure on page:", i)
                        pass
                    elif tmp.shape[1] == 7:
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif tmp.shape[1] == 6:
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        tmp[6] = np.nan
                        tmp = tmp[[0, 6, 1, 2, 3, 4, 5]]
                        tmp.columns = range(tmp.shape[1])
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    else:
                        print("\nAnother Structure on page:", i)
                        pass

            elif len(first_check.columns) == 8:
                print("Column length is 8")
                for i in range(len(tables2)):
                    tmp = tables2[i].df
                    if (tmp.shape[1] < 5):
                        print("\nDiiferent structure on page:", i)
                        pass
                    elif (tmp.shape[1] == 8):
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif (tmp.shape[1] == 7):
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        tmp[7] = np.nan
                        tmp = tmp[[0, 1, 2, 7, 3, 4, 5, 6]]
                        tmp.columns = range(tmp.shape[1])
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    else:
                        print("\nAnother Structure on page:", i)
                        pass
            elif len(first_check.columns) > 8:
                print("Column length greater than 8")
                for i in range(len(tables2)):
                    tmp = tables2[i].df
                    if tmp.shape[1] > 8:
                        try:
                            idx = [c for c in tmp[
                                tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                          axis=1) == True].index if c in tmp[
                                       tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                                 axis=1) == True].index][0]
                            tmp.columns = tmp.iloc[idx];
                            tmp = tmp.iloc[idx:, :];
                            tmp.reset_index(drop=True, inplace=True)
                        except:
                            try:
                                idx = [c for c in tmp[
                                    tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                              axis=1) == True].index if c in tmp[tmp.apply(
                                    lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                    axis=1) == True].index][0]
                                tmp.columns = tmp.iloc[idx];
                                tmp = tmp.iloc[idx:, :];
                                tmp.reset_index(drop=True, inplace=True)
                            except:
                                print("\nAxis-Column headers missing");
                                pass
                        tmp.drop("", axis=1, inplace=True)
                        tmp.columns = [0, 1, 2, 3, 4, 5, 6, 7]
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        # print("\n",tmp)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif (tmp.shape[1] == 8):
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        # print("\n",tmp)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif (tmp.shape[1] == 7):
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        tmp[7] = np.nan;
                        tmp = tmp[[0, 1, 2, 7, 3, 4, 5, 6]];
                        tmp.columns = range(tmp.shape[1]);
                        # print("\n",tmp)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif (tmp.shape[1] == 6):
                        try:
                            idx = [c for c in tmp[
                                tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                          axis=1) == True].index if c in tmp[
                                       tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                                 axis=1) == True].index][0]
                        except:
                            try:
                                idx = [c for c in tmp[
                                    tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                              axis=1) == True].index if c in tmp[tmp.apply(
                                    lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                    axis=1) == True].index][0]
                            except:
                                print("\nAxis-Column headers missing")
                        tmp.columns = tmp.iloc[idx]
                        tmp = tmp.iloc[idx + 1:, :]
                        tmp.reset_index(drop=True, inplace=True)
                        tmp = tmp[~tmp.index.isin(tmp[tmp.apply(lambda row: row.astype(str).str.lower().str.contains(
                            'page:|account status|total|reason for return|inward clg|opening balance|statement of a/c').any(),
                                                                axis=1) == True].index)]
                        if tmp.columns[0] == 'Tran Date\nValue Date\nTransaction Particulars':
                            tmp.columns = ["A", "B", "C", "D", "E", "F"]
                            tmp.reset_index(drop=True, inplace=True)
                            tmp1 = pd.DataFrame(tmp.A.str.split('\n', 2).tolist(), columns=["Z1", "Z2", "Z3"])
                            result = pd.concat([tmp1, tmp], axis=1, sort=False)
                            result.drop('A', axis=1, inplace=True)
                            result["Y"] = result["Z1"]
                            for i, j in enumerate(result["Y"]):
                                if re.search(r'(\d+-\d+-\d+)', j):
                                    result.at[i, 'Y'] = ""
                                    pass
                                elif re.search(r'^\s*$', j):
                                    result.at[i, 'Y'] = ""
                                    pass
                                else:
                                    pass

                            for i, j in enumerate(result["Z1"]):
                                if re.search(r'(\d+-\d+-\d+)', j):
                                    pass
                                elif re.search(r'^\s*$', j):
                                    result.at[i, 'Z1'] = ""
                                    pass
                                else:
                                    result.at[i, 'Z1'] = ""
                                    pass
                            result.fillna(value=pd.np.nan, inplace=True)
                            result['X'] = result['Z3'].fillna('') + result['Y'].fillna('')
                            result.drop(['Z3', 'Y'], axis=1, inplace=True)
                            result = result[['Z1', 'Z2', 'X', 'B', 'C', 'D', 'E', 'F']]
                            result.columns = [0, 1, 2, 3, 4, 5, 6, 7]
                            result = pd.DataFrame(result).replace(r'^\s*$', np.nan, regex=True)
                            # print("\n",result)
                            dt = pd.concat([dt, result]).reset_index(drop=True)
                        else:
                            print("\nIn without box pdf another Structure on page:", i);
                            pass

        elif (second_check.shape[1] > 5):
            try:
                idx = [c for c in second_check[
                    second_check.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                       axis=1) == True].index if c in second_check[
                           second_check.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                              axis=1) == True].index][0]
                second_check.columns = second_check.iloc[idx]
            except:
                try:
                    idx = [c for c in second_check[
                        second_check.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                           axis=1) == True].index if c in dt[
                               dt.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                        axis=1) == True].index][0]
                    second_check.columns = second_check.iloc[idx]
                except:
                    print("\nAxis-Column headers missing")
            tables2 = camelot.read_pdf(f, flavor='stream', pages='all', edge_til=500, password=pas)
            if len(second_check.columns) == 7:
                print("Column length is 7")
                for i in range(len(tables2)):
                    tmp = tables2[i].df
                    if tmp.shape[1] < 5:
                        print("\nDiiferent structure on page:", i);
                        pass
                    elif tmp.shape[1] == 7:
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif tmp.shape[1] == 6:
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        tmp[6] = np.nan;
                        tmp = tmp[[0, 6, 1, 2, 3, 4, 5]];
                        tmp.columns = range(tmp.shape[1]);
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    else:
                        print("\nAnother Structure on page:", i);
                        pass

            elif len(second_check.columns) == 8:
                print("Column length is 8")
                for i in range(len(tables2)):
                    # print("\n",i,":",tmp.shape[1])
                    tmp = tables2[i].df
                    if tmp.shape[1] < 5:
                        print("\nDiiferent structure on page:", i);
                        pass
                    elif tmp.shape[1] == 8:
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    elif tmp.shape[1] == 7:
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        tmp[7] = np.nan;
                        tmp = tmp[[0, 1, 2, 7, 3, 4, 5, 6]];
                        tmp.columns = range(tmp.shape[1]);
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    else:
                        print("\nAnother Structure on page:", i);
                        pass

    else:
        print("\nLength of table is 0");
        pass
    return dt


def axis_proceed(df):
    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
        print("1st attempt")
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            print("\nAxis-Column headers missing")
            pass

    try:
        df = df.drop(["Init."], axis=1)
    except:
        try:
            df = df.drop(["Branch Name"], axis=1)
        except:
            pass

    df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
        'opening balance|transaction total|closing balance|particulars|transaction').any(), axis=1) == True].index)]
    # df.drop(df.nunique(dropna=False)[(df.nunique(dropna=False) == 1)].index, axis=1,inplace=True)

    try:
        narr = [n for n in df.columns if "PARTICULARS" in str(n).upper()][0]
    except:
        try:
            narr = [n for n in df.columns if "NARRATION" in str(n).upper()][0]
        except:
            try:
                narr = [n for n in df.columns if "DESCRIPTION" in str(n).upper()][0]
            except:
                pass

    list1 = ['Tran Date', 'Chq No', 'Particulars', 'Debit', 'Credit', 'Balance']
    list2 = ['Tran Date', 'Value Date', 'Transaction Particulars', 'Chq No', 'Amount(INR)', 'DR/CR', 'Balance(INR)']
    list3 = ['Tran Date', 'Value Date', 'Transaction Particulars', 'Chq No.', 'Amount', 'DR|CR', 'Balance']

    if list(df.columns.values) == list1:
        print("list1")
        df = axis_backfill(df, narr)

    elif list(df.columns.values) == list2:
        print("list2")
        df = axis_backfill(df, narr)

    elif list(df.columns.values) == list3:
        print("list3")

        try:
            bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
            df[bal] = df[bal].apply(lambda x: abc(x))
        except:
            print("\nBalance Column Missing")

        try:
            df.replace(r'^\s*$', np.nan, regex=True, inplace=True)
        # type(df["Transaction Particulars"][15])
        except:
            pass

        try:
            df.dropna(axis=0, how='all', inplace=True)
        except:
            pass

        try:
            df.reset_index(drop=True, inplace=True)
        except:
            pass

        try:
            for j, i in enumerate(df["Transaction Particulars"]):
                if isnan(i) == True:
                    df[narr][j] = df[narr][j - 1] + df[narr][j + 1]
                else:
                    pass
        except:
            pass

        try:
            df.dropna(subset=['Tran Date'], inplace=True)
        except:
            try:
                df.dropna(subset=['Value Date'], inplace=True)
            except:
                pass

        # df.to_csv("parsed_completed.csv",index=0)
        print("parsed_completed")

    else:
        print("In axis_proceed, Another pattern found")
    return df


def axis_backfill(df, narr):
    print("axis_backfill")
    df = df[pd.notnull(df[narr])]
    df.iloc[:, -1] = df.iloc[:, -1].fillna(df.iloc[:, -2])

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance Column Missing")

    df["flag"] = df.iloc[:, -1].shift(1)
    df = df[~df.index.isin(df[df.iloc[:, -1] == df.iloc[:, -2]].index)]

    df = df.T.drop_duplicates().T
    df[bal] = df[bal].apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(float)
    df["flag"] = df.iloc[:, 0].astype(str) + df[bal].astype(str)
    df["flag2"] = np.arange(len(df))

    df.loc[df[["flag"]].duplicated(keep=False), 'flag'] = df["flag"] + df["flag2"].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannan' in row else row).fillna(method='bfill')

    df[narr] = df.groupby('flag')[narr].transform(lambda x: ''.join(x))
    df = df.drop_duplicates(['flag'], keep='last').iloc[0:, :-2].reset_index(drop=True)
    # df.to_csv("parsed_completed.csv",index=0)
    print("parsed_completed")
    return df


def axis_filter(df):
    try:
        drcr = [c for c in df.columns if "DR" in str(c).upper()][0]
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        print(drcr, amt)
        df["Credits"] = ""
        df["Debits"] = ""
    except:
        pass

    try:
        for i in df.index:
            if df[drcr][i] == "DR":
                df["Debits"][i] = df[amt][i]
            elif df[drcr][i] == "CR":
                df["Credits"][i] = df[amt][i]
            else:
                print(i)
                print("Dr & CR Absent")
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    print("withdraw column detected........")
    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "": np.nan})

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    # df.to_csv("check.csv", index=0)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        for i in df.index:
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]

    df.drop_duplicates(inplace=True)

    return df


# df.to_excel("axis05.xlsx", index=False)

def axis_stream(f, pas):
    tables = tabula.read_pdf(f,
                             stream=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                try:
                    idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                                  axis=1) == True].index if c in df[
                               df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                        axis=1) == True].index][0]
                    df.columns = df.iloc[idx];
                    df = df.iloc[idx + 1:, :];
                    df.reset_index(drop=True, inplace=True)
                except:
                    print("\nAxis Column Headers Missing");
                    pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction type').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('reason for return').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction total').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\nBalance columns missing")

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        drcr = [c for c in df.columns if "DR" in str(c).upper()][0]
        df[amt] = df[amt].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\Amount columns missing")

    try:
        if amt in df.columns:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for i, j in enumerate(df[drcr]):
                if "DR" in j:
                    df["Debits"][i] = df[amt][i]
                elif "CR" in j:
                    df["Credits"][i] = df[amt][i]
                else:
                    pass
    except:
        pass

    try:
        wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
        except:
            pass

    try:
        df[wdl] = df[wdl].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    try:
        dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[dep] = df[dep].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                except:
                    pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        df[[bal, wdl, dep]] = df[[bal, wdl, dep]].replace({"NA": np.nan, "-": np.nan})
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass

    try:
        df = df[df['Balance'].notna()]
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]

    df.drop_duplicates(inplace=True)

    return df


"""def axis_json(filename):
    tables = tabula.read_pdf_with_template(filename, "tabula-01-01-19 TO 31-01-19 Axis.json")
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    df = df.replace(r'^\s*$', np.nan, regex=True)
    return df

"""


def axis_json(filename):
    tables = tabula.read_pdf_with_template(filename, "tabula-01-01-19 TO 31-01-19 Axis.json")
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    df = df.replace(r'^\s*$', np.nan, regex=True)

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction type').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('reason for return').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction total').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\nBalance columns missing")

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        drcr = [c for c in df.columns if "DR" in str(c).upper()][0]
        df[amt] = df[amt].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\Amount columns missing")

    try:
        if amt in df.columns:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for i, j in enumerate(df[drcr]):
                if "DR" in j:
                    df["Debits"][i] = df[amt][i]
                elif "CR" in j:
                    df["Credits"][i] = df[amt][i]
                else:
                    pass
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[[bal, wdl, dep]] = df[[bal, wdl, dep]].replace({"NA": np.nan, "-": np.nan})
    except:
        pass

    try:
        df[wdl] = df[wdl].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    try:
        df[dep] = df[dep].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    return df

def state_file02(job_id, analytics_id, bankname, filename, password):
    try:
        print("AXIS STREAM")
        df = axis_stream(filename, password)

    except:
        try:
            print("AXIS TAB")
            df = axis_tab(filename, password)
        except:
            try:
                print("AXIS CAM")
                df = axis_came(filename, password)
                print("axis proceed")
                df = axis_proceed(df)
                print("axis filter")
                df = axis_filter(df)
            except:
                try:
                    df = axis_json(filename)
                except:
                    pass


    return df
