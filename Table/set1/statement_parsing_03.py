import pandas as pd
import numpy as np
import time
import tabula
import camelot
import re
import math, operator, functools
import datetime
from Table.set1.common_parsing import lattice_parsing
pd.set_option('mode.chained_assignment', None)

def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def hdfc_p1(f, pas):
    pars = tabula.read_pdf(f,
                           pages='all',
                           silent=True,
                           stream=True,
                           multiple_tables=True,
                           password=pas,
                           pandas_options={'header': None})

    df = pd.DataFrame()
    tables = []
    for i, c in enumerate(pars):
        if c.shape[1] == 7:
            tables.append(c)
        elif c.shape[1] == 6:
            c[6] = np.nan
            c = c[[0, 1, 2, 3, 4, 6, 5]]
            c.columns = range(c.shape[1])
            tables.append(c)
        elif c.shape[1] == 8:
            c = c[[0, 1, 3, 4, 5, 6, 7]]
            c.columns = range(c.shape[1])
            tables.append(c)
    df = pd.concat(tables, ignore_index=True)
    return df


def hdfc_p2(f, pas):
    pars = tabula.read_pdf(f,
                           pages='all',
                           silent=True,
                           lattice=True,
                           multiple_tables=True,
                           password=pas,
                           pandas_options={'header': None})

    df = pd.DataFrame()
    tables = []
    for i, c in enumerate(pars):
        if c.shape[1] == 7:
            tables.append(c)
        elif c.shape[1] == 6:
            c[6] = np.nan
            c = c[[0, 1, 2, 3, 4, 6, 5]]
            c.columns = range(c.shape[1])
            tables.append(c)
        elif c.shape[1] == 8:
            c = c[[0, 1, 3, 4, 5, 6, 7]]
            c.columns = range(c.shape[1])
            tables.append(c)
    df = pd.concat(tables, ignore_index=True)
    return df


def hdfc_p3(f, pas):
    tables = camelot.read_pdf(f, pages="all", flavor='stream', password=pas, flag_size=True)
    if len(tables) != 0:
        dt = pd.DataFrame()
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            # print(tmp.head())
            if tmp.shape[1] < 5:
                print("\nDiiferent structure on page:", i)
                pass;
            elif tmp.shape[1] == 5:
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx]
                    tmp = tmp.iloc[idx + 1:, :]
                    tmp.reset_index(drop=True, inplace=True)
                    tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                    dt = pd.concat([dt, tmp]).reset_index(drop=True)
                except Exception as e:
                    # print("Error:",e)
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[
                                   tmp.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                             axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx]
                        tmp = tmp.iloc[idx + 1:, :]
                        tmp.reset_index(drop=True, inplace=True)
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    except Exception as e:
                        # print("Error:",e)
                        # print("\nHDFC-Column headers missing")
                        pass

            elif tmp.shape[1] > 5:
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx]
                    tmp = tmp.iloc[idx + 1:, :]
                    tmp.reset_index(drop=True, inplace=True)
                    # tmp = tmp.loc[:,df.columns.notnull()]
                    tmp = tmp.replace(r'^\s*$', np.nan, regex=True)
                    tmp.dropna(axis=1, how='all', inplace=True)
                    print(tmp.head())
                    tmp.columns = ["Txn Date", "Narration", "Withdrawals", "Deposits", "Closing Balance"]
                    tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                    dt = pd.concat([dt, tmp]).reset_index(drop=True)
                except Exception as e:
                    print("Error:", e)
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[
                                   tmp.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                             axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx]
                        tmp = tmp.iloc[idx + 1:, :]
                        tmp.reset_index(drop=True, inplace=True)
                        # tmp = tmp.loc[:,df.columns.notnull()]
                        tmp = tmp.replace(r'^\s*$', np.nan, regex=True)
                        tmp.dropna(axis=1, how='all', inplace=True)
                        print(tmp.head())
                        tmp.columns = ["Txn Date", "Narration", "Withdrawals", "Deposits", "Closing Balance"]
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    except Exception as e:
                        print("Error:", e)
                        print("\nHDFC-Column headers missing")
                        pass
            else:
                pass

        return dt


def hdfc_p1_process(df):
    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nHDFC-Column headers missing")
                pass

    try:
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                    axis=1) == True].index]
        print("IDX:", idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('statement summary').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
            'page:|account status|total|reason for return|inward clg|opening balance|statement of a/c|statement summary|generated on|generated by|computer generated statement|not').any(),
                                           axis=1) == True].index)]
    except:
        pass
    try:
        df.drop(df.nunique(dropna=False)[(df.nunique(dropna=False) == 1)].index, axis=1, inplace=True)
    except:
        pass
    try:
        df.iloc[:, -1] = df.iloc[:, -1].fillna(df.iloc[:, -2])
    except:
        pass

    if len(df.columns) == 7:
        for j, i in enumerate(df.columns):
            if isnan(i) != False:
                df.columns = ["Date", "Narration", "Chq./Ref.No.", "Value Dt", "Withdrawal Amt.", "Deposit Amt.",
                              "Closing Balance"]
            else:
                pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df.loc[:, bal] = df[bal].apply(lambda x: abc(x))
    except:
        try:
            bal = [c for c in df.columns if "CLOSING" in str(c).upper()][0]
            df.loc[:, bal] = df[bal].apply(lambda x: abc(x))
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df.loc[:, bal] = df[bal].apply(lambda x: abc(x))
            except:
                print("\nBalance columns missing")

    try:
        df['flag'] = df.iloc[:, -1].shift(1)
    except:
        pass
    try:
        df = df[~df.index.isin(df[df.iloc[:, -1] == df.iloc[:, -2]].index)]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            df["Cheque No"] = np.nan
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
            except:
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass


    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    except:
        pass
    try:
        df['Wdl1'] = df[wdl].astype(str).apply(
            lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(float) * -1
    except:
        pass
    try:
        df['Wdl1'] = df['Wdl1'].fillna(
            df[dep].astype(str).apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(
                float))
    except:
        pass

    # df=df.T.drop_duplicates().T
    try:
        df[bal] = df[bal].apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass
    df['flag'] = df.iloc[:, 0].astype(str) + df['Wdl1'].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))

    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannannan' in row else row).fillna(method='ffill')

    df[narr] = df.groupby('flag')[narr].transform(lambda x: ' '.join(x))
    df = df.drop_duplicates(['flag'], keep='first').reset_index(drop=True)

    df = df.replace(r'^\s*$', np.nan, regex=True)

    if isnan(df[dep][0]) == False:
        df["Credits"] = np.nan
        try:
            df.loc["Credits", 0] = float(str(df[dep][0]).replace(",", "").replace("(Cr)", "").replace("(Dr)", ""))
        except:
            pass
        df["Debits"] = np.nan
    elif isnan(df[wdl][0]) == False:
        df["Credits"] = np.nan
        df["Debits"] = np.nan
        try:
            df.loc["Debits", 0] = float(str(df[wdl][0]).replace(",", "").replace("(Cr)", "").replace("(Dr)", ""))
        except:
            pass

    try:
        for i, j in enumerate(df[bal]):
            if i < len(df[bal]) - 1:
                if df[bal][i] < df[bal][i + 1]:
                    df["Credits"][i + 1] = df[bal][i + 1] - df[bal][i]
                elif df[bal][i] > df[bal][i + 1]:
                    df["Debits"][i + 1] = df[bal][i + 1] - df[bal][i]
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, "Withdrawal Amt.","Deposit Amt.", bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df = df[df['Balance'].notna()]

    try:
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    return df


def hdfc_htm(f):
    table = pd.read_html(f)
    df = table[1]

    def abc(a):
        if type(a) == str:
            if len(a.split(' ')) == 2:
                z = a.split(' ')[1]
            else:
                z = a.split(' ')[0]
        else:
            z = a
        return z

    def isnan(value):
        try:
            return math.isnan(float(value))
        except:
            return False

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nHDFC-Column headers missing")
                pass

    try:
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                    axis=1) == True].index]
        print("IDX:", idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        for i in df.index:
            #     # create new column
            #     if "Dr" in df["new_column"][i].astype(str):
            #         df[bal][i] = df[bal][i]*-1
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df = df.drop(df.index[-1])

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)

    return df


def state_file03(job_id, analytics_id, bankname, filename, password):
    try:
        df = hdfc_p1(filename, password)
        df = hdfc_p1_process(df)
        df.reset_index(inplace=True, drop=True)
    except:
        try:
            df = hdfc_htm(filename)
            df.reset_index(inplace=True, drop=True)
        except:
            try:
                df = hdfc_p2(filename, password)
                df = hdfc_p1_process(df)
                df.reset_index(inplace=True, drop=True)
            except:
                try:
                    df = hdfc_p3(filename, password)
                    df = hdfc_p1_process(df)
                    df.reset_index(inplace=True, drop=True)
                except:
                    pass

    return df
