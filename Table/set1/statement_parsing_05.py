import pandas as pd
import numpy as np
import time
import tabula
import camelot
import re
import math, operator, functools
import datetime
from Table.set1.common_parsing import lattice_parsing


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def bom_p1(f, pas):
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    return df


def bom_p1_process(df):
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    drop_idx = [c for c in df[
        df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index if
                c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index]
    print(drop_idx)
    print(len(drop_idx))
    if len(drop_idx) > 1:
        for i in drop_idx[1:]:
            print(i)
            df.drop(df.index[i], inplace=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nICICI Column Headers Missing")
                pass
    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('total amount').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    list1 = list(df.columns)
    list2 = []
    for i in list1:
        if isnan(i) == False:
            list2.append(i)
    df = df.iloc[:, :len(list2)]
    df.columns = list2
    print(df.columns)

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHEQ" in str(c).upper()][0]
            except:
                pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    # print(df["bal"])

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
            except:
                pass

    df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan})

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                        "").replace(
            "DR", "")).astype(float)

    try:
        for i in df.index:
            if df["bal"][i] == "Dr":
                df.loc[i, bal] = df.loc[i, bal] * -1
            if df["bal"][i] == "DR":
                df.loc[i, bal] = df.loc[i, bal] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    try:
        df[dat] = df[dat].str.replace("\r", "")
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        print("true")
        df = df.iloc[::-1]

    return df


def bom_htm(f):
    table = pd.read_html(f)
    df = table[0]

    def abc(a):
        if type(a) == str:
            if len(a.split(' ')) == 2:
                z = a.split(' ')[1]
            else:
                z = a.split(' ')[0]
        else:
            z = a
        return z

    def isnan(value):
        try:
            return math.isnan(float(value))
        except:
            return False

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nHDFC-Column headers missing")
                pass

    try:
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                    axis=1) == True].index]
        print("IDX:", idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        for i in df.index:
            #     # create new column
            #     if "Dr" in df["new_column"][i].astype(str):
            #         df[bal][i] = df[bal][i]*-1
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df = df.drop(df.index[-1])

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)

    return df




def state_file05(job_id, analytics_id, bankname, filename, password):
    try:
        df = bom_p1(filename, password)
        df = bom_p1_process(df)
    except:
        try:
            df = bom_htm(filename)
            df.reset_index(inplace=True, drop=True)
        except:
            pass
    return df
