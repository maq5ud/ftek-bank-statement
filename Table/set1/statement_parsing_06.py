import pandas as pd
import numpy as np
import time
import tabula
import camelot
import re
import math, operator, functools
import datetime
from PyPDF2 import PdfFileWriter, PdfFileReader
from Table.set1.common_parsing import lattice_parsing


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def union_p1(f, pas):
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    return df


def union_p1_process(df):
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nUnion Column Headers Missing")
                pass
    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('statement summary').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        try:
            idx2 = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('cumulative totals').any(),
                         axis=1) == True].index][0]
            df.drop(df.index[idx2:], inplace=True)
        except:
            pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        df[bal] = df[bal].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("NaN replacing error")

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        try:
            df[amt] = df[amt].replace(r'^\s*$', np.nan, regex=True)
        except:
            print("NaN replacing error")
            pass
        try:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for j, i in enumerate(df[amt]):
                if "Cr" in str(i):
                    df["Credits"][j] = df[amt][j]
                elif "Dr" in str(i):
                    df["Debits"][j] = df[amt][j]
        except Exception as e:
            print("Error in Creating Debit Credit")
            print("Error", e)
    except:
        # print("Amount Not present")
        pass

    try:
        if amt in df.columns:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for i, j in enumerate(df[amt]):
                if "(Dr)" in j:
                    df["Debits"][i] = df[amt][i]
                elif "(Cr)" in j:
                    df["Credits"][i] = df[amt][i]
                else:
                    pass
    except:
        pass


    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df["CHQ"] = np.nan
                chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
            except:
                pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
                        except:
                            pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        df = df[[dat, chq, narr, wdl, dep, bal]]
        df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    except:
        df = df[[dat, narr, wdl, dep, bal]]
        df.columns = ["Xns Date", "Narration", "Debits", "Credits", "Balance"]

    try:
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
    except:
        pass

    return df


def state_file06(job_id, analytics_id, bankname, filename, password):
    print("06")
    df = union_p1(filename, password)
    df = union_p1_process(df)
    df.reset_index(inplace=True, drop=True)

    return df
