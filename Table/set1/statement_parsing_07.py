import pandas as pd 
import numpy as np 
import time
import tabula
import camelot
import re
import math,operator,functools
import datetime
from PyPDF2 import PdfFileWriter, PdfFileReader 
from Table.set1.common_parsing import lattice_parsing

def abc(a):
	if type(a) == str:
		if "Cr" in a:
			if len(a.split('Cr'))==2:
				z=a.split('Cr')[0]
			else:
				z=a.split('Cr')[0]
		elif "Dr" in a:
			if len(a.split('Dr'))==2:
				z=a.split('Dr')[0]
			else:
				z=a.split('Dr')[0]
		if "CR" in a:
			if len(a.split('CR'))==2:
				z=a.split('CR')[0]
			else:
				z=a.split('CR')[0]
		if "DR" in a:
			if len(a.split('DR'))==2:
				z=a.split('DR')[0]
			else:
				z=a.split('DR')[0]
	else:
		z=a
	return z

def isnan(value):
	try:
		return math.isnan(float(value))
	except:
		return False

def pnb_p1(f,pas):
	tables = tabula.read_pdf(f,
							lattice = True,
							pages = "all",
							silent = True,
							multiple_tables = True,
							password=pas,
							pandas_options = {'header':None})
	df = pd.DataFrame()
	df = pd.concat([c for c in tables]).drop_duplicates() 
	
	df = df.replace(r'^\s*$', np.nan, regex=True)
	df = df[ df.isnull().sum(axis=1) < df.shape[1]-2].reset_index(drop=True)
	
	try:
		idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('cheque').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index][0]
		df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
	except:
		try:
			idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('particular').any(),axis=1)==True].index][0]
			df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
		except:
			try:
				idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('date').any(),axis=1)==True].index][0]
				df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
			except:
				try:
					idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('transaction').any(),axis=1)==True].index][0]
					df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
				except:
					try:
						idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(), axis=1) ==True].index ][0]
						df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
					except:
						print("\nPNB Column Headers Missing"); pass
	
	# [Date ,Particulars, Cheque No. , Withdrawal  ,Deposit .Balance]               
	# ['Transaction\rDate', 'Cheque\rNumber', 'Withdrawal', 'Deposit','Balance', 'Narration'],
	# ['Date', 'Instrument\rID', 'Amount', 'Type', 'Balance', 'Remarks'],
	# ['Transaction\rDate', 'Transaction\rDate', nan,'Cheque\rNumber','Withdrawal','Deposit','Balance',nan]
	# ['Txn No.', 'Txn Date', 'Description', 'Branch Name', 'Cheque No.','Dr Amount', 'Cr Amount', 'Balance']
	
	if "Remarks" in df.columns:
		#print("Remarks")
		df = pnb_p1_process(df)
	elif 'Narration' in df.columns:
		# print("Narration")
		df = pnb_p2_process(df)
	elif 'Particulars' in df.columns:
		#print("Particulars")
		df = pnb_p2_process(df)
	elif "Description" in df.columns:
		#print("Description")
		df = pnb_p2_process(df)
	else:
		df.columns = [0,1,2,3,4,5,6,7]
		df.drop(df.iloc[:,0:1], inplace = True, axis = 1)
		# print(df.columns)
		df.columns = ['Txn Date', 'Description', np.nan, 'Cheque No.','Withdrawal', 'Deposit', 'Balance']
		#print("Fourth format")
		df = pnb_p2_process(df)
			
	return df

def pnb_p1_process(df):
			
	try:
		df = df.loc[:, df.columns.notnull()]
	except:pass
	
	try:
		bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
	except: print("\nBalance columns missing")

	try:
		amt=[c for c in df.columns if "AMOUNT" in str(c).upper() ][0]
	except:pass

	try:
		typ=[c for c in df.columns if "TYPE" in str(c).upper() ][0]
	except:
		try:
			typ=[c for c in df.columns if "DR" in str(c).upper() ][0]
		except:pass

	df[[bal,amt]]=df[[bal,amt]].replace(r'^\s*$', np.nan, regex=True)

	try:
		df["Debits"] = np.nan; df["Credits"] = np.nan;
		for j,i in enumerate(df[typ]):
			if "CR" in str(i):
				df["Credits"][j] = df[amt][j]
			elif "DR" in str(i):
				df["Debits"][j] = df[amt][j]            
	except Exception as e: 
		print("Error",e)

	try:
		dat=[c for c in df.columns if "TRANSACTION DATE" in str(c).upper() ][0]
	except:
		try:
			dat=[c for c in df.columns if "TXN DATE" in str(c).upper() ][0]
		except:
			try:
				dat=[c for c in df.columns if "DATE" in str(c).upper() ][0]
			except:pass

	try:
		chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
	except:
		try:
			chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
		except:
			try:
				df["CHQ"] = np.nan
				chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
			except:pass

	try:
		narr=[c for c in df.columns if "REMARKS" in str(c).upper() ][0]
	except:
		try:
			narr=[c for c in df.columns if "PARTICULARS" in str(c).upper() ][0]
		except:
			try:
				narr=[c for c in df.columns if "DESCRIPTION" in str(c).upper() ][0]
			except:
				try:
					narr=[c for c in df.columns if "DETAILS" in str(c).upper() ][0]
				except:
					try:
						narr=[c for c in df.columns if "NARRATION" in str(c).upper() ][0]
					except:pass

	try:
		wdl=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			wdl=[c for c in df.columns if "DEBIT" in str(c).upper() ][0]
		except: pass

	try:
		dep=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
	except:
		try:
			dep=[c for c in df.columns if "CREDIT" in str(c).upper() ][0]
		except: pass


	try:
		df[[wdl, dep]] = df[[wdl, dep]].replace({"NA":np.nan, "-":np.nan,"0":np.nan, "0.00":np.nan})
	except:pass

	df[dep]=df[dep].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float) *-1
	df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)
	df[bal]=df[bal].astype(str).apply(lambda x:str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)


	df = df[[dat,chq,narr,wdl,dep,bal]]
	df.columns = ["Xns Date","Cheque No","Narration","Debits","Credits","Balance"]

	try:
		df["Xns Date"]=pd.to_datetime(df["Xns Date"], dayfirst=True)
		df["Xns Date"]=df["Xns Date"].dt.date.astype(str)
		
		if df["Xns Date"][2]>df["Xns Date"][len(df)-1]:
			df = df.iloc[::-1]
			df.reset_index(inplace = True, drop =True)
	except:pass

	# df.to_excel("check.xlsx", index=False)
	# print("parsed")
	return df

def pnb_p2_process(df):

	try:
		df = df.loc[:, df.columns.notnull()]
	except:pass

	# print(df.columns)

	try:
		bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
		df[bal]=df[bal].replace(r'^\s*$', np.nan, regex=True)
		df["bal"]=df[bal].apply( lambda x: abc(x) )
	except: print("\nBalance columns missing")

	try:
		dat=[c for c in df.columns if "TRANSACTION DATE" in str(c).upper() ][0]
	except:
		try:
			dat=[c for c in df.columns if "TXN DATE" in str(c).upper() ][0]
		except:
			try:
				dat=[c for c in df.columns if "DATE" in str(c).upper() ][0]
			except:
				try:
					dat=[c for c in df.columns if "TRANSACTION" in str(c).upper() ][0]
				except:pass

	try:
		chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
	except:
		try:
			chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
		except:
			try:
				df["CHQ"] = np.nan
				chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
			except:pass

	try:
		narr=[c for c in df.columns if "REMARKS" in str(c).upper() ][0]
	except:
		try:
			narr=[c for c in df.columns if "PARTICULARS" in str(c).upper() ][0]
		except:
			try:
				narr=[c for c in df.columns if "DESCRIPTION" in str(c).upper() ][0]
			except:
				try:
					narr=[c for c in df.columns if "DETAILS" in str(c).upper() ][0]
				except:
					try:
						narr=[c for c in df.columns if "NARRATION" in str(c).upper() ][0]
					except:pass
					
	try:
		wdl=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			wdl=[c for c in df.columns if "DEBIT" in str(c).upper() ][0]
		except:
			try:
				wdl=[c for c in df.columns if "DR AMOUNT" in str(c).upper() ][0]
			except: pass

	try:
		dep=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
	except:
		try:
			dep=[c for c in df.columns if "CREDIT" in str(c).upper() ][0]
		except:
			try:
				dep=[c for c in df.columns if "CR AMOUNT" in str(c).upper() ][0]
			except: pass

		
	try:
		df[[wdl, dep]] = df[[wdl, dep]].replace({"NA":np.nan, "-":np.nan,"0":np.nan, "0.00":np.nan})
	except:pass

	df[dep]=df[dep].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float) *-1
	df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)
	df["bal"]=df["bal"].astype(str).apply(lambda x:str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)

	for i,val in enumerate(df[bal]):
		if "Dr" in val:
			df["bal"][i] = df["bal"][i]*-1
		if "DR" in val:
			df["bal"][i] = df["bal"][i]*-1
		else:pass

	df = df[[dat,chq,narr,wdl,dep,"bal"]]
	df.columns = ["Xns Date","Cheque No","Narration","Debits","Credits","Balance"]

	try:
		df["Xns Date"]=pd.to_datetime(df["Xns Date"], dayfirst=True)
		df["Xns Date"]=df["Xns Date"].dt.date.astype(str)
		
		if df["Xns Date"][2]>df["Xns Date"][len(df)-1]:
			df = df.iloc[::-1]
			df.reset_index(inplace = True, drop =True)
	except:pass

	# df.to_excel("check.xlsx", index=False)
	# print("parsed")
	return df  

def state_file07(job_id,analytics_id,bankname,filename,password):
	df = pnb_p1(filename,password)
	df.reset_index(inplace=True, drop=True)
		 
	return df