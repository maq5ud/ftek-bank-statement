import pandas as pd
import numpy as np
import time
import tabula
import camelot
import re
import math, operator, functools
import datetime
from PyPDF2 import PdfFileWriter, PdfFileReader
from Table.set1.common_parsing import lattice_parsing


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def bob_p1(f, pas):
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    return df


def bob_p1_process(df):
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                try:
                    idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                                  axis=1) == True].index if c in df[
                               df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                        axis=1) == True].index][0]
                    df.columns = df.iloc[idx];
                    df = df.iloc[idx + 1:, :];
                    df.reset_index(drop=True, inplace=True)
                except:
                    print("\nBOB Column Headers Missing");
                    pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        try:
            idx2 = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                         axis=1) == True].index][0]
            df.drop(df.index[idx2:], inplace=True)
        except:
            try:
                idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(),
                                               axis=1) == True].index][0]
                df.drop(df.index[idx2:], inplace=True)
            except:
                pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df.insert(loc=0, column='CHEQUE', value=np.nan)
                # df["CHEQUE"] = np.nan
                chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            except:
                pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    pass

    try:
        bal = [c for c in df.columns if "BALA" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        print("\nBalance columns missing")
        df["Balance"] = np.nan
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            print("\nDebit columns missing")
            df["Debit"] = np.nan
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            print("\nCredit columns missing")
            df["Credit"] = np.nan
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
            pass

    try:
        df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})
    except:
        pass

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        typ = [c for c in df.columns if "TYPE" in str(c).upper()][0]
        for i, j in enumerate(df[typ]):
            if "Dr" in j:
                df[wdl][i] = df[amt][i]
            elif "Cr" in j:
                df[dep][i] = df[amt][i]
            else:
                pass
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    except:
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    except:
        pass

    try:
        for i in df.index:
            #     # create new column
            #     if "Dr" in df["new_column"][i].astype(str):
            #         df[bal][i] = df[bal][i]*-1
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    try:
        df = df[[dat, chq, narr, wdl, dep, bal]]
        df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    except:
        try:
            df = df[[dat, chq, narr, wdl, dep]]
            df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits"]
        except:
            df = df[[dat, narr, wdl, dep]]
            df.columns = ["Xns Date", "Narration", "Debits", "Credits"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)
    return df


# df.to_csv("check.csv", index=0)

def bob_p2(f, pas):
    tables = tabula.read_pdf(f,
                             stream=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    return df


def bob_p2_process(df):
    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nBOB Column Headers Missing");
                pass

    df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
        'page:|account status|total|reason for return|inward clg|opening balance|statement of a/c|statement summary|generated on|generated by|computer generated statement|not').any(),
                                       axis=1) == True].index)]

    if df.shape[1] > 1:
        if df.shape[1] == 5:
            df.columns = [0, 1, 2, 3, 4]
            df["Mix_data"] = df[0].astype(str) + df[1].astype(str) + df[2].astype(str) + df[3].astype(str) + df[
                4].astype(str)
            df = df[["Mix_data"]]

    df.columns = ["Mix_data"]

    for i, j in enumerate(df["Mix_data"]):
        if j.count("|") == 2:
            if df["Mix_data"][i + 2].count("|") == 4:
                df["Mix_data"][i + 1] = df["Mix_data"][i + 1] + df["Mix_data"][i + 2]
                df = df.drop(i + 2)

    split_data = df['Mix_data'].str.split("|")
    data = split_data.to_list()
    new_df = pd.DataFrame(data)
    df = new_df.replace(r'^\s*$', np.nan, regex=True)

    df = df[[1, 2, 3, 4, 5]]
    df.columns = ["DATE", "DETAILS", "DEBIT", "CREDIT", "BALANCE"]

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index]
        for i in idx:
            df = df.drop(i)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('details').any(),
                                       axis=1) == True].index]
            for i in idx:
                df = df.drop(i)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index]
                for i in idx:
                    df = df.drop(i)
            except:
                print("\nExtra BOB Column Headers Missing");
                pass

    df.dropna(how='all', inplace=True)

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
    except:
        print("\nBalance coprint(lumns missing")

    df['flag'] = df.iloc[:, -1].shift(1)
    df = df[~df.index.isin(df[df.iloc[:, -1] == df.iloc[:, -2]].index)]

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                print("\nDate column missing")
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            df.insert(loc=0, column='CHEQUE', value=np.nan)
            # df["CHEQUE"] = np.nan
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            pass

    # print(df.columns)

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        print("\nNarration column missing")
                        pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
            except:
                print("\nDebit column missing")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            print("\nCredit column missing")
            pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df['Wdl1'] = df[wdl].astype(str).apply(
        lambda x: str(x).replace(",", "").replace("Cr", "").replace("Dr", "").replace("(Cr)", "").replace("(Dr)",
                                                                                                          "")).astype(
        float) * -1
    df['Wdl1'] = df['Wdl1'].fillna(df[dep].astype(str).apply(
        lambda x: str(x).replace(",", "").replace("Cr", "").replace("Dr", "").replace("(Cr)", "").replace("(Dr)",
                                                                                                          "")).astype(
        float))

    # df=df.T.drop_duplicates().T
    df[bal] = df[bal].apply(
        lambda x: str(x).replace(",", "").replace("Cr", "").replace("Dr", "").replace("(Cr)", "").replace("(Dr)",
                                                                                                          "")).astype(
        float)
    df['flag'] = df.iloc[:, 0].astype(str) + df['Wdl1'].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))

    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannannan' in row else row).fillna(method='ffill')

    df[narr] = df.groupby('flag')[narr].transform(lambda x: ' '.join(x))
    df[narr] = df[narr].str.replace("nannannannan", "")
    # print(df.columns)
    df = df.drop_duplicates(['flag'], keep='first').iloc[0:, :-3].reset_index(drop=True)
    # print(df.columns)

    df = df.replace(r'^\s*$', np.nan, regex=True)
    print(dat)
    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)
    return df


def bob_p3(f, pas):
    ex = camelot.read_pdf(f, flavor='stream', pages="all", password=pas, row_tol=12)
    tables = ex
    if len(tables) != 0:
        dt = pd.DataFrame();
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            print(i)
            tmp = tables[i].df
            if (tmp.shape[1] < 5):
                print("\nDiiferent structure on page:", i);
                pass
            elif (tmp.shape[1] > 5):
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx];
                    tmp = tmp.iloc[idx + 1:, :];
                    tmp.reset_index(drop=True, inplace=True)
                except:
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                      axis=1) == True].index if c in tmp[
                                   tmp.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                             axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx];
                        tmp = tmp.iloc[idx + 1:, :];
                        tmp.reset_index(drop=True, inplace=True)
                    except:
                        pass
                dt = pd.concat([dt, tmp]).reset_index(drop=True)
            else:
                print("\nAnother Structure on page:", i);
                pass
    df = dt

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('opening balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2], inplace=True)
        df.reset_index(inplace=True, drop=True)
    except:
        pass
    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
        df.reset_index(inplace=True, drop=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DATE & TIME" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df["CHEQUE"] = np.nan
                chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            except:
                print("Cheque column not found.")
                pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "TRANSACTION DETAILS" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "AVAILABLE BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace(" ", "").replace("Rs.", "").replace("Dr", "").replace("?", "").replace("-",
                                                                                                            "").replace(
                "+", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace(" ", "").replace("Rs.", "").replace("Cr", "").replace("?", "").replace("-",
                                                                                                            "").replace(
                "+", ""))
    except:
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace(" ", "").replace("Rs.", "").replace("?", "").replace("-", "").replace("+", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace(" ", "").replace("\r", "").replace(",", "").replace("?", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "").replace("-", "").replace("+", "")).astype(float) * -1
    except Exception as e:
        print(e)
        print("wdl error")
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace(" ", "").replace("\r", "").replace(",", "").replace("?", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "").replace("-", "").replace("+", "")).astype(float)
    except Exception as e:
        print(e)
        print("dep error")
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace(" ", "").replace("\r", "").replace(",", "").replace("?", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "").replace("-", "").replace("+", "")).astype(float)
    except Exception as e:
        print(e)
        print("bal error")
        pass

    try:
        for i in df.index:
            if df["bal"][i][-2:] == "Dr":
                # print(df["bal"][i][-2:])
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')
    df["Narration"] = df["Narration"].str.replace('\n', '')

    # df=df.drop(df.index[0])
    df.reset_index(inplace=True, drop=True)

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    print("Parsed...")
    df.to_excel("check.xlsx")

    return df


def state_file08(job_id, analytics_id, bankname, filename, password):
    try:
        df = bob_p1(filename, password);
        df = bob_p1_process(df);
    except:
        try:
            df = bob_p2(filename, password);
            df = bob_p2_process(df);
        except:
            df = bob_p3(filename, password)
    return df
