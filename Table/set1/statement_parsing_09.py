import pandas as pd
import numpy as np
import time
import tabula
import camelot
import re
import math, operator, functools
import datetime
from PyPDF2 import PdfFileWriter, PdfFileReader
from Table.set1.common_parsing import lattice_parsing


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def icici_p1(f, pas):
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    return df


def icici_p1_process(df):
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nICICI Column Headers Missing");
                pass
    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total transaction count').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    if 'Cr/Dr' in df.columns:
        print("Cr/Dr present")
        try:
            amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
            crdr = [c for c in df.columns if "DR" in str(c).upper()][0]
            df[[bal, amt]] = df[[bal, amt]].replace(r'^\s*$', np.nan, regex=True)
            try:
                df["Debits"] = np.nan;
                df["Credits"] = np.nan;
                for j, i in enumerate(df[crdr]):
                    # print(i)
                    if "CR" in str(i):
                        df["Credits"][j] = df[amt][j]
                    elif "DR" in str(i):
                        df["Debits"][j] = df[amt][j]
            except Exception as e:
                # print("************")
                print("Error", e)
                pass
        except Exception as e:
            print("Error", e)
            pass
    else:
        try:
            amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
            df[[bal, amt]] = df[[bal, amt]].replace(r'^\s*$', np.nan, regex=True)
            try:
                df["Debits"] = np.nan;
                df["Credits"] = np.nan;
                for j, i in enumerate(df[amt]):
                    if "Cr" in str(i):
                        df["Credits"][j] = df[amt][j]
                    elif "Dr" in str(i):
                        df["Debits"][j] = df[amt][j]
            except Exception as e:
                print("Error", e)
        except:
            pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df["CHQ"] = np.nan
                chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
            except:
                pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "CRIPTION" in str(c).upper()][0]
                        except:
                            pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})
    except:
        pass

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        for i in df.index:
            #     # create new column
            #     if "Dr" in df["new_column"][i].astype(str):
            #         df[bal][i] = df[bal][i]*-1
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df["Xns Date"] = df["Xns Date"].str.replace("\r", "")
    df["Xns Date"] = df["Xns Date"].str.replace(",", "-")

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)

    return df


# df.to_csv("check.csv", index=0)

def icici_p3(f, num, pas):
    tmp = pd.DataFrame()
    tables = camelot.read_pdf(f, pages=str(num), flavor="stream", edge_tol=25, password=pas)
    tmp = tables[0].df
    try:
        idx = [c for c in tmp[
            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index if
               c in tmp[tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                  axis=1) == True].index][0]
        tmp.columns = tmp.iloc[idx]
        tmp = tmp.iloc[idx + 1:, :]
        tmp.reset_index(drop=True, inplace=True)
        print("1st attempt")
    except:
        try:
            idx = [c for c in tmp[
                tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in tmp[tmp.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                         axis=1) == True].index][0]
            tmp.columns = tmp.iloc[idx];
            tmp = tmp.iloc[idx + 1:, :];
            tmp.reset_index(drop=True, inplace=True)
        except:
            print("\nICICI-Column headers missing");
            pass
    return tmp


def icici_p3_process(df):
    # try:
    #     idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index ][0]
    #     df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
    #     print("1st attempt")
    # except:
    #     try:
    #         idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(), axis=1) ==True].index ][0]
    #         df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
    #     except:
    #         print("\nAxis-Column headers missing"); pass

    drop_idx = [c for c in df[
        df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index if
                c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index]
    # print(drop_idx)
    # print(len(drop_idx))
    if len(drop_idx) > 0:
        df.drop(df.index[drop_idx], inplace=True)

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        print("\nBalance columns missing")

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df["CHQ"] = np.nan
                chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
            except:
                pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        for j, i in enumerate(df[narr]):
            if isnan(i) == True:
                df[narr][j] = df[narr][j - 1] + df[narr][j + 1]
            else:
                pass
    except:
        pass

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("(",
                                                                                                        "").replace(")",
                                                                                                                    "").replace(
            "-", "")).astype(float)

    for i, j in enumerate(df["bal"]):
        if "-" in j:
            df[bal][i] = df[bal][i] * -1
        else:
            pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Xns Date"] = df["Xns Date"].str.replace("\r", "")

    list1 = []
    for j, i in enumerate(df["Xns Date"]):
        if isnan(i) == True:
            list1.append(j)
    if len(list1) > 0:
        df.drop(df.index[list1], inplace=True)

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]
        df.reset_index(inplace=True, drop=True)

    # df.to_excel("check.xlsx", index=False)
    return df


def icici_p4(f, pas):
    tables = camelot.read_pdf(f, pages="all", flavor='stream', password=pas, flag_size=True)
    if len(tables) != 0:
        dt = pd.DataFrame();
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            # print(tmp)
            if (tmp.shape[1] < 5):
                print("\nDiiferent structure on page:", i);
                pass;
            elif (tmp.shape[1] >= 8):
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx];
                    tmp = tmp.iloc[idx + 1:, :];
                    tmp.reset_index(drop=True, inplace=True)
                    tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                    dt = pd.concat([dt, tmp]).reset_index(drop=True)
                except Exception as e:
                    print("Error:", e)
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[
                                   tmp.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                             axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx];
                        tmp = tmp.iloc[idx + 1:, :];
                        tmp.reset_index(drop=True, inplace=True)
                        tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    except Exception as e:
                        # print("Error:",e)
                        # print("\nHDFC-Column headers missing")
                        pass
            else:
                pass
        return dt


def icici_p4_process(df):
    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                print("\nICICI-Column headers missing");
                pass

    try:
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                    axis=1) == True].index]
        print("IDX:", idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('statement summary').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
            'page:|account status|total|reason for return|inward clg|opening balance|statement of a/c|statement summary|generated on|generated by|computer generated statement|not').any(),
                                           axis=1) == True].index)]
    except:
        pass
    try:
        df.drop(df.nunique(dropna=False)[(df.nunique(dropna=False) == 1)].index, axis=1, inplace=True)
    except:
        pass

    try:
        df.iloc[:, -1] = df.iloc[:, -1].fillna(df.iloc[:, -2])
    except:
        pass

    if len(df.columns) == 9:
        for j, i in enumerate(df.columns):
            if isnan(i) != False:
                df.columns = ["Tran Date", "Value Date", "Particulars", "Location", "Chq.No.", "Withdrawals",
                              "Deposits", "Balance", "CR/DR"]
            else:
                df.columns = ["Tran Date", "Value Date", "Particulars", "Location", "Chq.No.", "Withdrawals",
                              "Deposits", "Balance", "CR/DR"]
                pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        typ = [c for c in df.columns if "cr" in str(c).upper()][0]
    except:
        print("\type columns missing")

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            df["Cheque No"] = np.nan
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
            except:
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    except:
        pass
    try:
        df['Wdl1'] = df[wdl].astype(str).apply(
            lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(float) * -1
    except:
        pass
    try:
        df['Wdl1'] = df['Wdl1'].fillna(
            df[dep].astype(str).apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(
                float))
    except:
        pass

    df[bal] = df[bal].apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(float)
    df['flag'] = df.iloc[:, 0].astype(str) + df['Wdl1'].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))

    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannannan' in row else row).fillna(method='ffill')

    df[narr] = df.groupby('flag')[narr].transform(lambda x: ' '.join(x))
    df = df.drop_duplicates(['flag'], keep='first').reset_index(drop=True)

    df = df.replace(r'^\s*$', np.nan, regex=True)

    try:
        for i, j in enumerate(df[typ]):
            if "Dr" in j:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    try:
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][0] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    print("parsed")
    return df


def state_file09(job_id, analytics_id, bankname, filename, password):
    try:
        pdf = PdfFileReader(open(filename, 'rb'))
        if pdf.isEncrypted:
            pdf.decrypt(password)
        else:
            pass
        total_cnt = pdf.getNumPages()

        dt = pd.DataFrame()
        for i in range(1, total_cnt + 1):
            tmp = icici_p3(filename, i, password)
            if tmp.shape[1] == 6:
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                dt = pd.concat([dt, tmp]).reset_index(drop=True)
        df = icici_p3_process(dt)
        df.reset_index(inplace=True, drop=True)
    except:
        try:
            df = lattice_parsing(filename, password)
        except:
            try:
                df = icici_p1(filename, password);
                df = icici_p1_process(df)
            except:
                try:
                    df = icici_p4(filename, password);
                    df = icici_p4_process(df);
                except:
                    df = lattice_parsing(filename, password)

    return df
