import pandas as pd 
import numpy as np 
import time
import tabula
import camelot
import re
import math,operator,functools
import datetime
from Table.set1.common_parsing import lattice_parsing

def isnan(value):
	try:
		return math.isnan(float(value))
	except:
		return False

def idbi_p1(f,pas):
	tables = camelot.read_pdf(f, flavor='stream', pages='all', edge_tol=500, password = pas)

	if len(tables) != 0:
		dt = pd.DataFrame(); tmp = pd.DataFrame();
		for i in range (len(tables)):
			tmp = tables[i].df
			if (tmp.shape[1] < 5):
				print("\nDiiferent structure on page:",i); pass
			elif (tmp.shape[1] == 9):
				tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex = True)
				dt = pd.concat([dt,tmp]).reset_index(drop=True).drop_duplicates()
			elif (tmp.shape[1] == 10):
				tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex = True)
				tmp = tmp.replace(r'^\s*$', np.nan, regex=True)
				tmp = tmp[ tmp.isnull().sum(axis=1) < tmp.shape[1]-2].reset_index(drop=True)
				tmp.drop(tmp.columns[0], axis = 1, inplace = True)
				tmp.columns = [0,1,2,3,4,5,6,7,8]   
				dt = pd.concat([dt,tmp]).reset_index(drop=True).drop_duplicates()
			elif (tmp.shape[1] == 8):
				tmp = tables[0].df
				tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex = True)
				tmp = tmp.replace(r'^\s*$', np.nan, regex=True)
				tmp = tmp[ tmp.isnull().sum(axis=1) < tmp.shape[1]-2].reset_index(drop=True)
				list1 = []
				list2 = []
				for i in tmp[2]:
					if "\n" in i:
						split_first = str(i).split('\n')
						list1.append(split_first[0])
						list2.append(split_first[1])
					else:
						split_second = str(i).split(" ")
						list1.append(split_second[0])
						list2.append(split_second[1])

				tmp.insert(2,20,list1)
				tmp.insert(3,21,list2)
				tmp = tmp[[0,1,20,21,3,4,5,6,7]]
				tmp.columns = [0,1,2,3,4,5,6,7,8]
				dt = pd.concat([dt,tmp]).reset_index(drop=True).drop_duplicates()
			else:
				print("\nAnother Structure on page:",i); pass

	return dt

def idbi_p2(f,pas):
	tables = tabula.read_pdf(f,
							lattice = True,
							pages = "all",
							silent = True,
							multiple_tables = True,
							pandas_options = {'header':None},
							password = pas)
	df = pd.DataFrame()
	df = pd.concat([c for c in tables]).drop_duplicates()
	return df

def idbi_p3(f,pas):
	tables = camelot.read_pdf(f, flavor='stream', pages='all', edge_tol=500, password=pas)
	if len(tables) != 0:
		dt = pd.DataFrame(); tmp = pd.DataFrame();
		for i in range (len(tables)):
			tmp = tables[i].df
			if (tmp.shape[1] < 5):
				print("\nDiiferent structure on page:",i); pass
			elif (tmp.shape[1] == 6):
				tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex = True)
				dt = pd.concat([dt,tmp]).reset_index(drop=True).drop_duplicates()
			elif (tmp.shape[1] == 5):
				tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex = True)
				tmp[5] = np.nan; tmp = tmp[[0,1,5,2,3,4,]]; tmp.columns = range(tmp.shape[1]);
				dt = pd.concat([dt,tmp]).reset_index(drop=True)
			else:
				print("\nAnother Structure on page:",i); pass

	df = dt
	# df.head()
	return df

def idbi_p4(f,pas):
	tables = tabula.read_pdf(f,                                            
							   pages='all',
							   silent=True,
							   lattice=True,
							   multiple_tables = True,
							   password=pas,
							   pandas_options={ 'header': None }) 

	df = pd.DataFrame()
	df = pd.concat(tables, ignore_index=True)

	return df

def idbi_p1_process(df):
	df = df.replace(r'^\s*$', np.nan, regex=True)
	df = df[ df.isnull().sum(axis=1) < df.shape[1]-2].reset_index(drop=True)

	try:
		idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index ][0]
		df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
	except:
		try:
			idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index ][0]
			df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
		except:
			try:
				idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(), axis=1) ==True].index ][0]
				df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)           
			except:
				print("\IDBI-Column headers missing"); pass

	try:
		idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('statement summary').any(),axis=1)==True].index][0]    
		df.drop(df.index[idx2:], inplace=True)
	except:
		try:
			idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('cr count').any(),axis=1)==True].index][0]    
			df.drop(df.index[idx2:], inplace=True)
		except:pass
	
	try:  
		df = df[~df.index.isin(df[df.apply(lambda row:row.astype(str).str.lower().str.contains('description').any(),axis=1)==True].index)]
	except:pass   
		
	try:
		narr=[n for n in df.columns if "PARTICULARS" in str(n).upper()][0]
	except:
		try:
			narr=[n for n in df.columns if "NARRATION" in str(n).upper()][0]
		except:
			try:
				narr=[n for n in df.columns if "DESCRIPTION" in str(n).upper()][0] 
			except:
				pass
				
	try:
		bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
	except: 
		print("\nBalance columns missing")
		try:
			df["Balance"] = np.nan
			bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
		except:pass
		
	try:
		amt=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			amt=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
		except:
			try:
				amt=[c for c in df.columns if "AMOUNT" in str(c).upper() ][0]
			except:pass
			
	try:
		typ=[c for c in df.columns if "TYPE" in str(c).upper() ][0]
	except:
		try:
			typ=[c for c in df.columns if "DR" in str(c).upper() ][0]
		except:pass
	 
	try:   
		df[[bal,amt]]=df[[bal,amt]].replace(r'^\s*$', np.nan, regex=True)
	except:pass

	try:
		df["Debits"] = np.nan; df["Credits"] = np.nan;
		for j,i in enumerate(df[typ]):
			if "CR" in str(i):
				df["Credits"][j] = df[amt][j]
			elif "DR" in str(i):
				df["Debits"][j] = df[amt][j] 
			elif "Cr" in str(i):
				df["Credits"][j] = df[amt][j]
			elif "Dr" in str(i):
				df["Debits"][j] = df[amt][j]            
	except Exception as e: 
		print("Error",e)
		
	try:
		dat=[c for c in df.columns if "TRANSACTION DATE" in str(c).upper() ][0]
	except:
		try:
			dat=[c for c in df.columns if "TXN DATE" in str(c).upper() ][0]
		except:
			try:
				dat=[c for c in df.columns if "DATE" in str(c).upper() ][0]
			except:pass
			
	try:
		chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
	except:
		try:
			chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
		except:
			try:
				df["CHQ"] = np.nan
				chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
			except:pass
			
	try:
		narr=[c for c in df.columns if "REMARKS" in str(c).upper() ][0]
	except:
		try:
			narr=[c for c in df.columns if "PARTICULARS" in str(c).upper() ][0]
		except:
			try:
				narr=[c for c in df.columns if "DESCRIPTION" in str(c).upper() ][0]
			except:
				try:
					narr=[c for c in df.columns if "DETAILS" in str(c).upper() ][0]
				except:
					try:
						narr=[c for c in df.columns if "NARRATION" in str(c).upper() ][0]
					except:pass
					
	try:
		wdl=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			wdl=[c for c in df.columns if "DEBIT" in str(c).upper() ][0]
		except: pass
		
	try:
		dep=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
	except:
		try:
			dep=[c for c in df.columns if "CREDIT" in str(c).upper() ][0]
		except: pass

	try:
		df[[wdl, dep]] = df[[wdl, dep]].replace({"NA":np.nan, "-":np.nan,"0":np.nan, "0.00":np.nan})
	except:pass

	df[dep]=df[dep].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float) *-1
	df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)
	df[bal]=df[bal].astype(str).apply(lambda x:str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)

	df = df[[dat,chq,narr,wdl,dep,bal]]
	df.columns = ["Xns Date","Cheque No","Narration","Debits","Credits","Balance"]

	try:
		df["Xns Date"]=pd.to_datetime(df["Xns Date"], dayfirst=True)
		df["Xns Date"]=df["Xns Date"].dt.date.astype(str)

		if df["Xns Date"][0]>df["Xns Date"][len(df)-1]:
			df = df.iloc[::-1]
			df.reset_index(inplace = True, drop =True)
	except:pass

	# df.to_excel("idbi.xlsx", index=False)
	print("Parsed")
	return df

def idbi_p3_process(df):
	try:
		idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index ][0]
		df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
	except:
		try:
			idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index ][0]
			df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)
		except:
			try:
				idx=[ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(), axis=1) ==True].index ][0]
				df.columns=df.iloc[idx] ; df=df.iloc[idx+1:,:] ; df.reset_index(drop=True,inplace=True)           
			except:
				print("\IDBI-Column headers missing"); pass

	try:
		idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('statement summary').any(),axis=1)==True].index][0]    
		df.drop(df.index[idx2:], inplace=True)
	except:
		try:
			idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('cr count').any(),axis=1)==True].index][0]    
			df.drop(df.index[idx2:], inplace=True)
		except:
			try:
				idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('balance as on').any(),axis=1)==True].index][0]    
				df.drop(df.index[idx2:], inplace=True)
			except:pass

	try:
		narr=[n for n in df.columns if "PARTICULARS" in str(n).upper()][0]
	except:
		try:
			narr=[n for n in df.columns if "NARRATION" in str(n).upper()][0]
		except:
			try:
				narr=[n for n in df.columns if "DESCRIPTION" in str(n).upper()][0] 
			except:
				pass
			
	try:
		bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
		df["bal"] = df[bal]
	except: 
		print("\nBalance columns missing")
		
	try:   
		df[bal]=df[bal].replace(r'^\s*$', np.nan, regex=True)
	except:pass

	df['flag']=df.iloc[:,-1].shift(1)
	df=df[~df.index.isin(df[df.iloc[:,-1]==df.iloc[:,-2]].index)]

	try:
		dat=[c for c in df.columns if "TRANSACTION DATE" in str(c).upper() ][0]
	except:
		try:
			dat=[c for c in df.columns if "TXN DATE" in str(c).upper() ][0]
		except:
			try:
				dat=[c for c in df.columns if "DATE" in str(c).upper() ][0]
			except:pass

	try:
		chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
	except:
		try:
			chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
		except:
			try:
				df["CHQ"] = np.nan
				chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
			except:pass
			
	try:
		wdl=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			wdl=[c for c in df.columns if "DEBIT" in str(c).upper() ][0]
		except: pass

	try:
		dep=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
	except:
		try:
			dep=[c for c in df.columns if "CREDIT" in str(c).upper() ][0]
		except: pass

	try:
		df[[wdl, dep]] = df[[wdl, dep]].replace({"NA":np.nan, "-":np.nan,"0":np.nan, "0.00":np.nan})
	except:pass

	df[dep]=df[dep].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float) *-1
	df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)
	df[bal]=df[bal].astype(str).apply(lambda x:str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","")).astype(float)

	df['Wdl1']=df[wdl].astype(str).apply(lambda x: str(x).replace(",","").replace("(Cr)","").replace("(Dr)","")).astype(float) *-1
	df['Wdl1']=df['Wdl1'].fillna(df[dep].astype(str).apply(lambda x: str(x).replace(",","").replace("(Cr)","").replace("(Dr)","")).astype(float) )
	#print(df.head())

	df=df.T.drop_duplicates().T                                  
	df['flag']=df.iloc[:,0].astype(str)+df['Wdl1'].astype(str) +df[bal].astype(str)
	df['flag2']=np.arange(len(df))

	df.loc[ df[['flag']].duplicated(keep=False) , 'flag' ] =df['flag'] + df['flag2'].astype(str)  
	df['flag']=df['flag'].apply( lambda row : np.nan if 'nannan' in row else row ).fillna(method='ffill')  

	df[narr]=df.groupby('flag')[narr].transform( lambda x :''.join(x))           
	df=df.drop_duplicates(['flag'],keep='first').iloc[0:,:-3].reset_index(drop=True)


	df = df[[dat,chq,narr,wdl,dep,bal]]
	df.columns = ["Xns Date","Cheque No","Narration","Debits","Credits","Balance"]

	df["Xns Date"]=pd.to_datetime(df["Xns Date"], dayfirst=True)
	df["Xns Date"]=df["Xns Date"].dt.date.astype(str)

	if df["Xns Date"][0]>df["Xns Date"][len(df)-1]:
		df = df.iloc[::-1]
		df.reset_index(inplace = True, drop =True)

	# df.to_excel("idbi.xlsx", index=False)
	print("Parsed")
	return df

def state_file11(job_id,analytics_id,bankname,filename,password):
	try:
		print("pattern4")
		df = idbi_p4(filename,password);df = idbi_p1_process(df);
		df.reset_index(inplace=True, drop=True)
	except:
		try:
			df = idbi_p1(filename,password);df = idbi_p1_process(df);
			df.reset_index(inplace=True, drop=True)
		except:
			try:
				df = idbi_p2(filename,password);df = idbi_p1_process(df);
				df.reset_index(inplace=True, drop=True)
			except:
				try:
					df = idbi_p3(filename,password);df = idbi_p3_process(df);
					df.reset_index(inplace=True, drop=True)
				except:
					pass
	return df