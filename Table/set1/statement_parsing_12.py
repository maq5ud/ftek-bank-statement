import pandas as pd
import numpy as np
import tabula
import camelot
import math
from PyPDF2 import PdfFileReader
from Table.set1.common_parsing import lattice_parsing
import json
import os


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def yes_p1(f, num, pas):
    tables = tabula.read_pdf(f, stream=True, pages=num, silent=True, multiple_tables=True,
                             password=pas, pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    return df


def yes_p1_process(df, f, num, pas):
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                tables = tabula.read_pdf(f, lattice=True, pages=num, silent=True, multiple_tables=True, password=pas,
                                         pandas_options={'header': None})
                df = pd.DataFrame()
                df = pd.concat([c for c in tables]).drop_duplicates()
                df = df.replace(r'^\s*$', np.nan, regex=True)
                df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

                try:
                    idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                                  axis=1) == True].index if c in df[
                               df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                        axis=1) == True].index][0]
                    df.columns = df.iloc[idx];
                    df = df.iloc[idx + 1:, :];
                    df.reset_index(drop=True, inplace=True)
                except:
                    try:
                        idx = [c for c in df[
                            df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                     axis=1) == True].index if c in df[
                                   df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                            axis=1) == True].index][0]
                        df.columns = df.iloc[idx];
                        df = df.iloc[idx + 1:, :];
                        df.reset_index(drop=True, inplace=True)
                    except:
                        try:
                            idx = [c for c in df[
                                df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index if c in df[
                                       df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                                axis=1) == True].index][0]
                            df.columns = df.iloc[idx];
                            df = df.iloc[idx + 1:, :];
                            df.reset_index(drop=True, inplace=True)
                        except:
                            print("\n Yes Column Headers Missing");
                            pass
    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('page total').any(), axis=1) == True].index][
            0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df.loc[:, df.columns.notnull()]
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            df["Cheque No"] = np.nan
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            pass

    try:
        val_dat = [c for c in df.columns if "VALUE DATE" in str(c).upper()][0]
    except:
        pass

    try:
        narr = [c for c in df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            pass

    df[[wdl, dep]] = df[[wdl, dep]].replace({"NA": np.nan, "-": np.nan, "0": np.nan, "0.00": np.nan})

    try:
        auto = [c for c in df.columns if "AUTOSWEEP" in str(c).upper()][0]
        for j, i in enumerate(df[auto]):
            if isnan(i) == False:
                df[wdl][j] = df[auto][j]
            else:
                pass
    except:
        pass

    try:
        rev = [c for c in df.columns if "REVERSE" in str(c).upper()][0]
        for j, i in enumerate(df[rev]):
            if isnan(i) == False:
                df[dep][j] = df[rev][j]
            else:
                pass
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    except ValueError:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    except ValueError:
        pass

    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)

    try:
        for i in df.index:
            if df["bal"][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        df.drop(['bal'], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([auto], axis=1, inplace=True)
    except:
        pass

    try:
        df.drop([rev], axis=1, inplace=True)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]

    return df


def yes_p3(f, pas):
    ex = camelot.read_pdf(f, flavor='lattice', pages="all", password=pas)
    tables = ex
    print(tables)
    if len(tables) != 0:
        dt = pd.DataFrame();
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            if (tmp.shape[1] < 5):
                print("\nDiiferent structure on page:", i);
                pass
            elif (tmp.shape[1] >= 5):
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx];
                    tmp = tmp.iloc[idx + 1:, :];
                    tmp.reset_index(drop=True, inplace=True)
                except:
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[
                                   tmp.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                             axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx];
                        tmp = tmp.iloc[idx + 1:, :];
                        tmp.reset_index(drop=True, inplace=True)
                    except:
                        pass
                dt = pd.concat([dt, tmp]).reset_index(drop=True)
            else:
                print("\nAnother Structure on page:", i);
                pass

    print(dt)
    df = dt
    print(df)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('balance forward | b/f').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2], inplace=True)
    except:
        pass
    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('end of statement').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    df = df.drop(df.index[0])

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df["CHEQUE"] = np.nan
                chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            except:
                print("Cheque column not found.")
                pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float) * -1
    except Exception as e:
        print(e)
        print("wdl error")
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("dep error")
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", ""))
    except Exception as e:
        print(e)
        print("bal error")
        pass

    df[dep] = df[dep].replace(0.0, np.nan)
    df[wdl] = df[wdl].replace(-0.00, np.nan)
    df[bal] = df[bal].astype(float)

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')
    df = df.drop(df.index[0])

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    print("Parsed...")

    return df


def yes_p4(f, pas):
    tables = tabula.read_pdf(f,
                             pages='all',
                             silent=True,
                             stream=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})

    df = pd.DataFrame()
    df = pd.concat(tables, ignore_index=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            pass

    df.reset_index(drop=True, inplace=True)

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('balance forward | b/f').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    df.reset_index(drop=True, inplace=True)
    # print(df.head())

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('end of statement').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    df.reset_index(drop=True, inplace=True)

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index]
        df.drop(df.index[idx2], inplace=True)
        df.reset_index(drop=True, inplace=True)
    except:
        pass

    # df=df.drop(df.index[0])

    df.columns = ["Transaction Date", "Value Date", "Description", "Withdrawals", "Deposits", "Balance"]

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                df["CHEQUE"] = np.nan
                chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            except:
                print("Cheque column not found.")
                pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float) * -1
    except Exception as e:
        print(e)
        print("wdl error")
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("dep error")
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", ""))
    except Exception as e:
        print(e)
        print("bal error")
        pass

    df[dep] = df[dep].replace(0.0, np.nan)
    df[wdl] = df[wdl].replace(-0.00, np.nan)
    df[bal] = df[bal].astype(float)

    df['Wdl1'] = df[wdl].fillna(
        df[dep].astype(str).apply(lambda x: str(x).replace(",", "").replace("(Cr)", "").replace("(Dr)", "")).astype(
            float))
    df['flag'] = df.iloc[:, 0].astype(str) + df['Wdl1'].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))

    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannan' in row else row).fillna(method='ffill')
    # print(df.head(40))

    df[narr] = df[narr].astype(str)

    df[narr] = df.groupby('flag')[narr].transform(lambda x: ''.join(x))
    df = df.drop_duplicates(['flag'], keep='first').iloc[0:, :-3].reset_index(drop=True)

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')
    df = df.drop(df.index[0])

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    print("Parsed...")
    # df.to_excel("check.xlsx")
    return df


def yes_p5(f, pas):
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages=1,
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})

    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 3].reset_index(drop=True)

    df = df.drop([0], axis=1)
    main_df = pd.DataFrame()
    main_df.append(df)

    pdf_reader = PdfFileReader(f)
    page_no = pdf_reader.numPages

    for i in range(2, page_no):
        # print(i)
        filename = "tabula-yes02.json"
        with open(filename, 'r') as b:
            data = json.load(b)
            data[0]['page'] = i
        os.remove(filename)
        with open(filename, 'w') as b:
            json.dump(data, b, indent=4)

        tables_none = tabula.read_pdf_with_template(f, "tabula-yes02.json")
        df1 = pd.DataFrame()
        df1 = pd.concat([c for c in tables_none]).drop_duplicates()
        df1 = df1.replace(r'^\s*$', np.nan, regex=True)
        df1 = df1[df1.isnull().sum(axis=1) < df1.shape[1] - 2].reset_index(drop=True)
        main_df = main_df.append(df1)

    for i in range(page_no, page_no + 1):
        print(i)
        filename = "tabula-yes02.json"
        with open(filename, 'r') as b:
            data = json.load(b)
            data[0]['page'] = i
        os.remove(filename)
        with open(filename, 'w') as b:
            json.dump(data, b, indent=4)

        tables_two = tabula.read_pdf_with_template(f, "tabula-yes02.json")
        df2 = pd.DataFrame()
        df2 = pd.concat([c for c in tables_two]).drop_duplicates()
        df2 = df2.replace(r'^\s*$', np.nan, regex=True)
        df2 = df2[df2.isnull().sum(axis=1) < df2.shape[1] - 2].reset_index(drop=True)

        df2 = df2.drop(df2.columns[[6, 7, 8]], axis=1)
        main_df = main_df.append(df2)

    df = main_df

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index if
               c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                axis=1) == True].index][0]
        # print(idx)
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            # DAT_VALUE, BOOKBAL
            idx = [c for c in
                   df[df.apply(lambda row: row.astype(str).str.lower().str.contains('dat').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('bal').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                print("Column values not found")
                pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(), axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            try:
                                narr = [c for c in df.columns if "NARATION" in str(c).upper()][0]
                            except:
                                print("Narration column not found.")
                                pass

    # df[narr] = df[narr].replace('\n', ' ').replace('\r', '')

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    # try:
    # 	df[dep]=df[dep].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
    # except:pass
    # try:
    # 	df[wdl]=df[wdl].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
    # except:pass
    # try:
    # 	df[bal]=df[bal].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
    # except:pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr|Dr|CR|DR')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    # try:
    # 	df[wdl]=df[wdl].replace(r'[a-zA-z]+', np.nan, regex=True)
    # except:pass
    # try:
    # 	df[dep]=df[dep].replace(r'[a-zA-z]+', np.nan, regex=True)
    # except:pass

    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("Rs.", "").replace("\r", "").replace(",", "").replace("(", "").replace(")",
                                                                                                        "").replace(
            "Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float) * -1

    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("Rs.", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                       "").replace("Dr",
                                                                                                                   "").replace(
            "CR", "").replace("DR", "")).astype(float)

    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("Rs.", "").replace(",", "").replace("<", "").replace(">", "").replace("(", "").replace(
            ")", "").replace("Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float)

    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')

    # try:
    #   p = r'\b(\d+-\d+-\d+)\b'
    #   df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    # except:pass

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    df.to_excel("check_csv.xlsx", index=0)

    # print("parsed")
    return df


def state_file12(job_id, analytics_id, bankname, filename, password):
    try:
        df1 = yes_p5(filename, password)
    except:
        try:
            pdf = PdfFileReader(open(filename, 'rb'))
            if pdf.isEncrypted:
                pdf.decrypt(password)
            else:
                pass
            total_cnt = pdf.getNumPages()
            chunks = []
            for i in range(1, total_cnt + 1):
                df = yes_p1(filename, i, password)
                df = yes_p1_process(df, filename, i, password)
                chunks.append(df)

            df1 = pd.DataFrame()
            df1 = pd.concat(chunks, ignore_index=True)
            df1.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

            try:
                df1["Xns Date"] = pd.to_datetime(df1["Xns Date"], dayfirst=True)
                df1["Xns Date"] = df1["Xns Date"].dt.date.astype(str)

                if df1["Xns Date"][0] > df1["Xns Date"][len(df) - 1]:
                    df1 = df1.iloc[::-1]
                    df1.reset_index(inplace=True, drop=True)
            except:
                pass

            df1.reset_index(inplace=True, drop=True)
        except:
            try:
                pdf = PdfFileReader(open(filename, 'rb'))
                if pdf.isEncrypted:
                    pdf.decrypt(password)
                else:
                    pass
                total_cnt = pdf.getNumPages()
                chunks = []
                for i in range(1, total_cnt + 1):
                    df = yes_p1(filename, i, password)
                    df = yes_p1_process(df, filename, i, password)
                    chunks.append(df)

                df1 = pd.DataFrame()
                df1 = pd.concat(chunks, ignore_index=True)
                df1.columns = ["Xns Date", "Value Date", "Narration", "Debits", "Credits", "Balance"]

                try:
                    df1["Xns Date"] = pd.to_datetime(df1["Xns Date"], dayfirst=True)
                    df1["Xns Date"] = df1["Xns Date"].dt.date.astype(str)

                    if df1["Xns Date"][0] > df1["Xns Date"][len(df) - 1]:
                        df1 = df1.iloc[::-1]
                        df1.reset_index(inplace=True, drop=True)
                except:
                    pass

                df1.reset_index(inplace=True, drop=True)
            except:
                try:
                    print("3rd pattern")
                    df1 = yes_p3(filename, password)
                    df1.to_excel("check_excel.xlsx")
                except:
                    print("4th pattern")
                    df1 = yes_p4(filename, password)
                    df1.to_excel("check_excel.xlsx")
    return df1
