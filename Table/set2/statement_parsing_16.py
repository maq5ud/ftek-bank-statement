from Table.set2.common_parsing import lattice_parsing
import pandas as pd
import numpy as np
import tabula
import math
import re
import string

pd.set_option('mode.chained_assignment', None)


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def p1(f, pas):
    tables = tabula.read_pdf(f,
                             stream=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})

    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 3].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index if
               c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                axis=1) == True].index][0]
        # print(idx)
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            # DAT_VALUE, BOOKBAL
            idx = [c for c in
                   df[df.apply(lambda row: row.astype(str).str.lower().str.contains('dat').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('bal').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                try:
                    idx = [c for c in df[
                        df.apply(lambda row: row.astype(str).str.lower().str.contains('dt').any(),
                                 axis=1) == True].index
                           if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                               axis=1) == True].index][0]
                    df.columns = df.iloc[idx];
                    df = df.iloc[idx + 1:, :];
                    df.reset_index(drop=True, inplace=True)
                except:
                    print("Column values not found")
                    pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(), axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                try:
                                    dat = [c for c in df.columns if "TRAN DT" in str(c).upper()][0]
                                except:
                                    print("Date column not found")
                                    pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            try:
                                narr = [c for c in df.columns if "NARATION" in str(c).upper()][0]
                            except:
                                print("Narration column not found.")
                                pass

    # df[narr] = df[narr].replace('\n', ' ').replace('\r', '')

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr|Dr|CR|DR')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass
    try:
        df[dep] = df[dep].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float) * -1
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass

    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("<", "").replace(">", "").replace("(",
                                                                                                          "").replace(
                ")", "").replace("Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass
    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')

    # try:
    #   p = r'\b(\d+-\d+-\d+)\b'
    #   df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    # except:pass

    # df["Xns Date"] = df["Xns Date"].str[:10]

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    # df.to_excel("check_csv.xlsx", index=0)

    # print("parsed")
    return df


def p2(f, pas):
    tables = tabula.read_pdf(f,
                             stream=True,
                             pages="all",
                             silent=False,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None}
                             )

    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 3].reset_index(drop=True)

    df.columns = ["TXN Date", "VALUE DATE", "BRANCH", "DETAILS", "CHQ NO", "WITHDRAWL", "DEPOSITS", "BALANCE"]
    lenx = len(df["BALANCE"])
    df = df.iloc[1:]

    count = 0
    for i in range(1, lenx):
        if (str(df["DEPOSITS"][i]) == "nan") and (str(df["BALANCE"][i]) == "nan"):

            df["BALANCE"][i] = df["WITHDRAWL"][i]
            df["DEPOSITS"][i] = df["CHQ NO"][i]
            df["WITHDRAWL"][i] = df["DETAILS"][i]
            df["CHQ NO"][i] = df["BRANCH"][i]
            df["DETAILS"][i] = df["VALUE DATE"][i]

            x = df['TXN Date'][i]
            y = x[0:10]
            z = x[10:]
            df["TXN Date"][i] = y
            df["VALUE DATE"][i] = z
        elif (str(df["BALANCE"][i]) == "nan"):
            df["BALANCE"][i] = df["DEPOSITS"][i]
            df["DEPOSITS"][i] = df["WITHDRAWL"][i]
            df["WITHDRAWL"][i] = df["CHQ NO"][i]
            df["CHQ NO"][i] = df["DETAILS"][i]
            df["DETAILS"][i] = df["VALUE DATE"][i]

            x = df['TXN Date'][i]
            y = x[0:10]
            z = x[10:]
            df["TXN Date"][i] = y
            df["VALUE DATE"][i] = z

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\nBalance columns missing")

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        drcr = [c for c in df.columns if "DR" in str(c).upper()][0]
        df[amt] = df[amt].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\Amount columns missing")

    try:
        if amt in df.columns:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for i, j in enumerate(df[drcr]):
                if "DR" in j:
                    df["Debits"][i] = df[amt][i]
                elif "CR" in j:
                    df["Credits"][i] = df[amt][i]
                else:
                    pass
    except:
        pass

    try:
        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            pass

    try:
        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                    except:
                        pass

    try:
        wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
        except:
            pass

    try:
        df[[bal, wdl, dep]] = df[[bal, wdl, dep]].replace({"NA": np.nan, "-": np.nan})
    except:
        pass

    try:
        df[wdl] = df[wdl].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    try:
        df[dep] = df[dep].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float) * -1
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                           "").replace(
                ")",
                "").replace(
                "(Cr)", "").replace("(Dr)", "")).astype(float)
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    df = df[~df["Xns Date"].str.contains("Posting Da")]

    df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
    df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

    if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
        df = df.iloc[::-1]

    df.drop_duplicates(inplace=True)

    df['Narration'] = df['Narration'].str.lstrip(string.digits)

    return df


def state_file16(job_id,analytics_id,bankname,filename,password):
    try:
        df = lattice_parsing(filename, password)
    except:
        try:
            df = p2(filename, password)
        except:
            df = p1(filename, password)

    return df
