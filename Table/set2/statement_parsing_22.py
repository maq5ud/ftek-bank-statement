import pandas as pd
import numpy as np
import camelot
import tabula
import fitz
import json
import os


def syn_p1(f, pas):
    tables = tabula.read_pdf(f,
                             lattice=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})

    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 3].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index if
               c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                axis=1) == True].index][0]
        # print(idx)
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            # DAT_VALUE, BOOKBAL
            idx = [c for c in
                   df[df.apply(lambda row: row.astype(str).str.lower().str.contains('dat').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('bal').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("Column values not found")
                pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(), axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            try:
                                narr = [c for c in df.columns if "NARATION" in str(c).upper()][0]
                            except:
                                print("Narration column not found.")
                                pass

    # df[narr] = df[narr].replace('\n', ' ').replace('\r', '')

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr|Dr|CR|DR')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass
    try:
        df[dep] = df[dep].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float) * -1
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass

    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("<", "").replace(">", "").replace("(",
                                                                                                          "").replace(
                ")", "").replace("Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass
    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')

    # try:
    #   p = r'\b(\d+-\d+-\d+)\b'
    #   df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    # except:pass

    # df["Xns Date"] = df["Xns Date"].str[:10]

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    # df.to_excel("check_csv.xlsx", index=0)

    # print("parsed")
    return df


def syn_p2(f, pas):
    tables = tabula.read_pdf(f,
                             stream=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})

    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 3].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index if
               c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                axis=1) == True].index][0]
        # print(idx)
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            # DAT_VALUE, BOOKBAL
            idx = [c for c in
                   df[df.apply(lambda row: row.astype(str).str.lower().str.contains('dat').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('bal').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                print("Column values not found")
                pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(), axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            try:
                                narr = [c for c in df.columns if "NARATION" in str(c).upper()][0]
                            except:
                                print("Narration column not found.")
                                pass

    # df[narr] = df[narr].replace('\n', ' ').replace('\r', '')

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr|Dr|CR|DR')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass
    try:
        df[dep] = df[dep].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float) * -1
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass

    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("<", "").replace(">", "").replace("(",
                                                                                                          "").replace(
                ")", "").replace("Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass
    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')

    # try:
    #   p = r'\b(\d+-\d+-\d+)\b'
    #   df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    # except:pass

    # df["Xns Date"] = df["Xns Date"].str[:10]

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    # df.to_excel("check_csv.xlsx", index=0)

    # print("parsed")
    return df


def syn_p3(filename, password):
    tables = camelot.read_pdf(filename, flavor='stream', pages="all", password=password, row_tol=10)
    if len(tables) != 0:
        dt = pd.DataFrame();
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            if (tmp.shape[1] < 5):
                # print("\nDiiferent structure on page:",i); pass
                pass
            elif (tmp.shape[1] >= 5):
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('narration').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx];
                    tmp = tmp.iloc[idx + 1:, :];
                    tmp.reset_index(drop=True, inplace=True)
                    try:
                        tmp.dropna(axis=1, how="all", inplace=True)
                    except:
                        pass
                    try:
                        tmp = tmp.loc[:, tmp.columns.notnull()]
                    except:
                        pass
                    dt = pd.concat([dt, tmp]).reset_index(drop=True)
                except:
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[
                                   tmp.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                             axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx];
                        tmp = tmp.iloc[idx + 1:, :];
                        tmp.reset_index(drop=True, inplace=True)
                        try:
                            tmp.dropna(axis=1, how="all", inplace=True)
                        except:
                            pass
                        try:
                            tmp = tmp.loc[:, tmp.columns.notnull()]
                        except:
                            pass
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    except:
                        pass
            else:
                # print("\nAnother Structure on page:",i); pass
                pass

    df = dt

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            pass

    try:
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(), axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass
    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('carried forward').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
            'b/f|c/f|opening balance|statement of a/c|statement summary|generated on|generated by|brought forward|call').any(),
                                           axis=1) == True].index)]
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    # try:
    #     df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("-",""))
    # except:
    #     pass
    # try:
    #     df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("-",""))
    # except:
    #     pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float) * -1
    except Exception as e:
        print(e)
        print("wdl error")
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("dep error")
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                            "").replace(
                "DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("bal error")
        pass

    df['flag'] = df.iloc[:, 0].astype(str) + df[wdl].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))
    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannan' in row else row).fillna(method='ffill')
    df[narr] = df[narr].astype(str)
    df[narr] = df.groupby('flag')[narr].transform(lambda x: ''.join(x))
    df = df.drop_duplicates(['flag'], keep='first').reset_index(drop=True)

    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    p = r'\b(\d+-\d+-\d+)\b'
    df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    df["Narration"] = df["Narration"].str.replace(p, "")

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    # df.to_excel("check_csv.xlsx", index=0)

    # df.drop_duplicates(inplace=True)

    return df


def syn_p4(f, pas):
    tables = tabula.read_pdf_with_template(f, "sss.json")
    list1 = []
    list2 = []
    x = tables[0]["Date Particulars"]
    y = tables[0]["Chq. No."]
    for i in range(0, (len(x))):
        if i != len(x) - 2:
            if (x[i][2]) == '-':
                if (x[i + 1][2]) != '-':
                    var = x[i] + " " + x[i + 1]
                    if (x[i + 2][2]) != '-':
                        var += x[i + 2]
                        list1.append(var)
                    else:
                        list1.append(var)
                else:
                    list1.append(x[i])

        else:
            if (x[i][2]) == '-':
                if (x[i + 1][2]) != '-':
                    var = x[i] + " " + x[i + 1]
                    list1.append(var)

    for i in range(0, (len(x))):
        if (x[i][2]) == '-':
            list2.append(y[i])

    data = {'Transactions': list1,
            'Cheque No': list2}
    data = pd.DataFrame(data)

    data.insert(0, "Date", " ")

    for i in range(0, len(data["Transactions"])):
        x1 = data["Transactions"].iloc[i]
        y1 = x1[0:10]
        z1 = x1[11:]
        data["Transactions"].iloc[i] = z1
        data["Date"].iloc[i] = y1

    tables2 = tabula.read_pdf_with_template(f, "tabula-bank_statement11.json")

    tables2[0] = tables2[0].shift(-1, axis=0)
    tables2[0] = tables2[0].drop((len(tables2[0]) - 1))

    tables2[0].columns = ["Debit", "Credit", "Balance"]

    result = pd.concat([data, tables2[0]], axis=1)

    doc = fitz.open(f)
    page_no = doc.pageCount

    for i in range(2, page_no + 1):
        print(i)
        filename = "syn_1.json"
        with open(filename, 'r') as b:
            data = json.load(b)
            data[0]['page'] = i
        os.remove(filename)
        with open(filename, 'w') as b:
            json.dump(data, b, indent=4)

        filename_one = "last3.json"
        with open(filename_one, 'r') as b:
            data = json.load(b)
            data[0]['page'] = i
        os.remove(filename_one)
        with open(filename_one, 'w') as b:
            json.dump(data, b, indent=4)

        tables_one = tabula.read_pdf_with_template(f, "syn_1.json")
        list1 = []
        list2 = []
        x = tables_one[0]["Date Particulars"]
        y = tables_one[0]["Chq. No."]
        for i in range(0, (len(x))):
            # print(i)
            if i < (len(x) - 2):
                if len(x[i]) > 2:
                    if (x[i][2]) == '-':
                        if (x[i + 1][2]) != '-':
                            var = x[i] + " " + x[i + 1]

                            if len(x[i + 2]) > 2:
                                if (x[i + 2][2]) != '-':
                                    var += x[i + 2]
                                    list1.append(var)
                                else:
                                    list1.append(var)

                            else:
                                var += x[i + 2]
                                list1.append(var)
                        else:
                            list1.append(x[i])

                    else:
                        pass
                else:
                    pass

            else:
                if len(x[i]) > 2:
                    if (x[i][2]) == '-':
                        try:
                            if (x[i + 1][2]) != '-':
                                var = x[i] + " " + x[i + 1]
                                list1.append(var)
                            else:
                                var = x[i]
                                list1.append(var)
                        except:
                            var = x[i]
                            list1.append(var)
                else:
                    pass

        for i in range(0, (len(x))):
            if len(x[i]) > 2:
                if (x[i][2]) == '-':
                    list2.append(y[i])

        data1 = {'Transactions': list1,
                 'Cheque No': list2}
        data1 = pd.DataFrame(data1)
        data1.insert(0, "Date", " ")

        for i in range(0, len(data1["Transactions"])):
            x1 = data1["Transactions"].iloc[i]
            y1 = x1[0:10]
            z1 = x1[11:]
            data1["Transactions"].iloc[i] = z1
            data1["Date"].iloc[i] = y1

        tables_two = tabula.read_pdf_with_template(f, "last3.json")
        tables_two[0].columns = ["Debit", "Credit", "Balance"]

        xx = pd.concat([data1, tables_two[0]], axis=1)
        result = result.append(xx)

    result = result.iloc[:-1]

    df = result



    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            try:
                                narr = [c for c in df.columns if "NARATION" in str(c).upper()][0]
                            except:
                                print("Narration column not found.")
                                pass

    # df[narr] = df[narr].replace('\n', ' ').replace('\r', '')

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr|Dr|CR|DR')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass
    try:
        df[dep] = df[dep].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float) * -1
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass

    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("<", "").replace(">", "").replace("(",
                                                                                                          "").replace(
                ")", "").replace("Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass
    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    return df


def state_file22(job_id, analytics_id, bankname, filename, password):
    try:
        df = syn_p1(filename, password)
    except:
        try:
            df = syn_p4(filename, password)
        except:
            try:
                df = syn_p2(filename, password)
            except:
                df = syn_p3(filename, password)

    return df
