import re
import tabula
import datetime
import pandas as pd
import numpy as np
import camelot


def p1(filename, password):
    tables = tabula.read_pdf(filename,
                             stream=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=password,
                             pandas_options={'header': None})

    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 3].reset_index(drop=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                           axis=1) == True].index if
               c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                axis=1) == True].index][0]
        # print(idx)
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            # DAT_VALUE, BOOKBAL
            idx = [c for c in
                   df[df.apply(lambda row: row.astype(str).str.lower().str.contains('dat').any(),
                               axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('bal').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                             axis=1) == True].index
                       if
                       c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('description').any(),
                                        axis=1) == True].index][0]
                df.columns = df.iloc[idx];
                df = df.iloc[idx + 1:, :];
                df.reset_index(drop=True, inplace=True)
            except:
                print("Column values not found")
                pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in
                df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                            axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = \
            [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                                    axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df.reset_index(drop=True, inplace=True)
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(), axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            try:
                                narr = [c for c in df.columns if "NARATION" in str(c).upper()][0]
                            except:
                                print("Narration column not found.")
                                pass

    # df[narr] = df[narr].replace('\n', ' ').replace('\r', '')

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("INR", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace(" ", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace("NA", ""))
        df[bal] = df[bal].astype(str).apply(lambda x: str(x).replace(" ", ""))
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split('Cr|Dr|CR|DR')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass
    try:
        df[dep] = df[dep].replace(r'[a-zA-z]+', np.nan, regex=True)
    except:
        pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float) * -1
    except:
        pass

    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace(")", "").replace("Cr",
                                                                                                          "").replace(
                "Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass

    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("<", "").replace(">", "").replace("(",
                                                                                                          "").replace(
                ")", "").replace("Cr", "").replace("Dr", "").replace("CR", "").replace("DR", "")).astype(float)
    except:
        pass
    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]
    df["Narration"] = df["Narration"].str.replace('\r', '')

    # try:
    #   p = r'\b(\d+-\d+-\d+)\b'
    #   df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    # except:pass

    # df["Xns Date"] = df["Xns Date"].str[:10]

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    return df


def p2(filename, password):
    ex = camelot.read_pdf(filename, flavor='stream', pages="all", password=password, row_tol=10)
    tables = ex

    if len(tables) != 0:
        dt = pd.DataFrame();
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            if (tmp.shape[1] < 5):
                # print("\nDiiferent structure on page:",i); pass
                pass
            elif (tmp.shape[1] >= 5):
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx];
                    tmp = tmp.iloc[idx + 1:, :];
                    tmp.reset_index(drop=True, inplace=True)
                except:
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[tmp.apply(
                            lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                            axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx];
                        tmp = tmp.iloc[idx + 1:, :];
                        tmp.reset_index(drop=True, inplace=True)
                    except:
                        pass
                dt = pd.concat([dt, tmp]).reset_index(drop=True)
            else:
                # print("\nAnother Structure on page:",i); pass
                pass

    df = dt

    try:
        idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                            axis=1) == True].index][0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                          axis=1) == True].index if c in df[
                       df.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass
    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('carried forward').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass
    try:
        idx2 = [c for c in df[df.apply(
            lambda row: row.astype(str).str.lower().str.contains('statements are sent to customers').any(),
            axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
            'account number|holding|opening balance|statement of a/c|statement summary|generated on|generated by|brought forward').any(),
                                           axis=1) == True].index)]
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "Txn\nDate" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                            except:
                                try:
                                    dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                                except:
                                    print("Date column not found")
                                    pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    df[wdl] = df[wdl].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                        "").replace(
            "DR", "")).astype(float) * -1

    df[dep] = df[dep].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                        "").replace(
            "DR", "")).astype(float)

    df[bal] = df[bal].astype(str).apply(
        lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace("CR",
                                                                                                        "").replace(
            "DR", "")).astype(float)

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    return df


def p3(filename, password):
    global str
    ex = camelot.read_pdf(filename, password=password, pages='all', flavor='stream', row_tol=25)
    tables = ex
    # tables[1].df

    if len(tables) != 0:
        dt = pd.DataFrame();
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            # if (tmp.shape[1] < 6):
            #     print("\nDiiferent structure on page:",i); pass
            if (i == 1 and tmp.shape[1] < 6):
                # tmp=pd.DataFrame(tmp)
                tmp = tmp.drop(tmp.index[0])
                tmp.insert(0, 0, np.nan, True)
                tmp.insert(1, 1, np.nan, True)
                tmp.insert(1, 1, np.nan, True)
                tmp.columns = [0, 1, 2, 3, 4, 5, 6]
                # print(tmp)
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                dt = pd.concat([dt, tmp]).reset_index(drop=True)
            elif (tmp.shape[1] == 6):
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                try:
                    tmp.insert(1, 1, np.nan, True)
                    # print(tmp)
                    dt = pd.concat([dt, tmp]).reset_index(drop=True)
                except:
                    pass
            elif (tmp.shape[1] > 6):
                if (i == 1):
                    tmp = tmp.drop(tmp.index[0])
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                dt = pd.concat([dt, tmp]).reset_index(drop=True)
            else:
                print("\nAnother Structure on page:", i);
                pass

    df = dt
    df.columns = ["Xns Date", "Cheque No", "Transaction Id", "Particulars", "Cr/Dr", "Credit", "Balance"]
    df["Debit"] = df["Credit"]
    # print(df.columns)
    # df.head(300)

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "XNS DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                            except:
                                print("Date column not found")
                                pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE NO" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    # print(dat,chq,narr,bal,wdl,dep)
    # df.head(150)

    df['flag'] = df.iloc[:, 0].astype(str) + df[wdl].astype(str) + df[bal].astype(str)
    # df.head(150)
    df['flag2'] = np.arange(len(df))
    # df.head(150)
    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    # df.head(15)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannannan' in row else row).fillna(method='ffill')
    # df.head(150)
    df[narr] = df[narr].astype(str)
    df[narr] = df.groupby('flag')[narr].transform(lambda x: ' '.join(x))
    # df.head(150)
    df = df.drop_duplicates(['flag'], keep='first').reset_index(drop=True)
    # df.head(150)

    try:
        for i in df.index:
            if "Dr" in df["Cr/Dr"][i]:
                df[dep][i] = np.nan
            elif "Cr" in df["Cr/Dr"][i]:
                df[wdl][i] = np.nan
            else:
                pass
    except:
        pass

    # try:
    #     df = df.replace(r'^\s*$', np.nan, regex=True)
    # except:pass

    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace(
                "CR", "").replace("DR", "")).astype(float) * -1
    except Exception as e:
        print(e)
        print("wdl error")
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace(
                "CR", "").replace("DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("dep error")
        pass

    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace(
                "CR", "").replace("DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("bal error")
        pass

    # import re
    # import datetime
    # import math
    for j in df.index:
        # print(df["Xns Date"][j])
        try:
            if (np.isnan(df["Xns Date"][j]) == True):
                str = df["Particulars"][j]
                str1 = re.search(r'\d{2}/\d{2}/\d{4}', str)
                if (str1 != None):
                    date = datetime.datetime.strptime(str1.group(), '%d/%m/%Y').date()
                    df["Xns Date"][j] = date.strftime("%d/%m/%Y")
                else:
                    pass
        except:
            # print(j)
            break
    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    return df


def state_file25(job_id, analytics_id, bankname, filename, password):
    try:
        df = p3(filename, password)
    except:
        try:
            df = p2(filename, password)
        except:
            df = p1(filename, password)

    return df
