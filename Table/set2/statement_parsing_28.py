import pandas as pd
import numpy as np
import camelot
from Table.set2.common_parsing import lattice_parsing
import tabula


def df_stream(f, pas):
    tables = tabula.read_pdf(f,
                             stream=True,
                             pages="all",
                             silent=True,
                             multiple_tables=True,
                             password=pas,
                             pandas_options={'header': None})
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    df = df.replace(r'^\s*$', np.nan, regex=True)

    try:
        idx = [c for c in
               df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
               if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index][
            0]
        df.columns = df.iloc[idx]
        df = df.iloc[idx + 1:, :]
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[
                df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) == True].index
                   if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction').any(),
                                       axis=1) == True].index][0]
            df.columns = df.iloc[idx]
            df = df.iloc[idx + 1:, :]
            df.reset_index(drop=True, inplace=True)
        except:
            try:
                idx = [c for c in df[
                    df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                       if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('remarks').any(),
                                           axis=1) == True].index][0]
                df.columns = df.iloc[idx]
                df = df.iloc[idx + 1:, :]
                df.reset_index(drop=True, inplace=True)
            except:
                try:
                    idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                                  axis=1) == True].index if c in df[
                               df.apply(lambda row: row.astype(str).str.lower().str.contains('particular').any(),
                                        axis=1) == True].index][0]
                    df.columns = df.iloc[idx];
                    df = df.iloc[idx + 1:, :];
                    df.reset_index(drop=True, inplace=True)
                except:
                    df.columns = ["TXN Date", "DETAILS", "DEBIT", "CREDIT", "BALANCE", "XXX"]
                    pass

    try:
        idx2 = [c for c in
            df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction type').any(),
                axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in
            df[df.apply(lambda row: row.astype(str).str.lower().str.contains('reason for return').any(),
                axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in
            df[df.apply(lambda row: row.astype(str).str.lower().str.contains('transaction total').any(),
                axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df[bal] = df[bal].replace(r'^\s*$', np.nan, regex=True)
    except:
        print("\nBalance columns missing")

    try:
        amt = [c for c in df.columns if "AMOUNT" in str(c).upper()][0]
        drcr = [c for c in df.columns if "DR" in str(c).upper()][0]
        df[amt] = df[amt].replace(r'^\s*$', np.nan, regex=True)
    except:
        pass

    try:
        if amt in df.columns:
            df["Debits"] = np.nan
            df["Credits"] = np.nan
            for i, j in enumerate(df[drcr]):
                if "DR" in j:
                    df["Debits"][i] = df[amt][i]
                elif "CR" in j:
                    df["Credits"][i] = df[amt][i]
                else:
                    pass
    except:
        pass

        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
            except:
                pass

        try:
            df[wdl] = df[wdl].replace(r'^\s*$', np.nan, regex=True)
        except:
            pass

        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
            except:
                pass

        try:
            df[dep] = df[dep].replace(r'^\s*$', np.nan, regex=True)
        except:
            pass

        try:
            dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                    except:
                        pass

        try:
            chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
            except:
                pass

        try:
            narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULARS" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "DETAILS" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                        except:
                            pass

        try:
            df[[bal, wdl, dep]] = df[[bal, wdl, dep]].replace({"NA": np.nan, "-": np.nan})
        except:
            pass

        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
        try:
            df[wdl] = df[wdl].astype(str).apply(
                lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                               "").replace(
                    ")",
                    "").replace(
                    "(Cr)", "").replace("(Dr)", "")).astype(float) * -1
        except:
            pass
        try:
            df[dep] = df[dep].astype(str).apply(
                lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                               "").replace(
                    ")",
                    "").replace(
                    "(Cr)", "").replace("(Dr)", "")).astype(float)
        except:
            pass
        try:
            df[bal] = df[bal].astype(str).apply(
                lambda x: str(x).replace("\r", "").replace(",", "").replace("(", "").replace("Cr", "").replace("Dr",
                                                                                                               "").replace(
                    ")",
                    "").replace(
                    "(Cr)", "").replace("(Dr)", "")).astype(float)
        except:
            pass

        try:
            df = df[df['Balance'].notna()]
        except:
            pass

        df = df[[dat, narr, wdl, dep, bal]]
        df.columns = ["Xns Date", "Narration", "Debits", "Credits", "Balance"]
        lenX = len(df["Narration"])
        for i in range(0, lenX):
            if str(df["Narration"].iloc[i]) == "nan":
                x = df['Xns Date'].iloc[i]
                y = x[0:10]
                z = x[10:]
                df["Xns Date"].iloc[i] = y
                df["Narration"].iloc[i] = z
            else:
                x = df['Xns Date'].iloc[i]
                y = x[0:10]
                z = x[10:]
                df["Xns Date"].iloc[i] = y

    return df


def df_camelot(filename, password):
    ex = camelot.read_pdf(filename, flavor='stream', pages="all", password=password, row_tol=10, edge_tol=500)
    tables = ex

    if len(tables) != 0:
        dt = pd.DataFrame()
        tmp = pd.DataFrame()
        for i in range(len(tables)):
            tmp = tables[i].df
            if (tmp.shape[1] < 5):
                # print("\nDiiferent structure on page:",i); pass
                pass
            elif (tmp.shape[1] >= 5):
                tmp = pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
                try:
                    idx = [c for c in tmp[
                        tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                  axis=1) == True].index if c in tmp[
                               tmp.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                                         axis=1) == True].index][0]
                    tmp.columns = tmp.iloc[idx];
                    tmp = tmp.iloc[idx + 1:, :];
                    tmp.reset_index(drop=True, inplace=True)
                    dt = pd.concat([dt, tmp]).reset_index(drop=True)
                except:
                    try:
                        idx = [c for c in tmp[
                            tmp.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in tmp[tmp.apply(
                            lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                            axis=1) == True].index][0]
                        tmp.columns = tmp.iloc[idx];
                        tmp = tmp.iloc[idx + 1:, :];
                        tmp.reset_index(drop=True, inplace=True)
                        dt = pd.concat([dt, tmp]).reset_index(drop=True)
                    except:
                        pass
            else:
                # print("\nAnother Structure on page:",i); pass
                pass

    df = dt

    try:
        idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                      axis=1) == True].index if c in df[
                   df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(),
                            axis=1) == True].index][0]
        df.columns = df.iloc[idx];
        df = df.iloc[idx + 1:, :];
        df.reset_index(drop=True, inplace=True)
    except:
        try:
            idx = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                          axis=1) == True].index if c in df[
                       df.apply(lambda row: row.astype(str).str.lower().str.contains('particulars').any(),
                                axis=1) == True].index][0]
            df.columns = df.iloc[idx];
            df = df.iloc[idx + 1:, :];
            df.reset_index(drop=True, inplace=True)
        except:
            pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) == True].index
                if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(),
                                    axis=1) == True].index]
        # print("IDX:",idx2)
        df.drop(df.index[idx2], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('closing balance').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        idx2 = [c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('total').any(),
                                       axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass
    try:
        idx2 = [c for c in df[
            df.apply(lambda row: row.astype(str).str.lower().str.contains('carried forward').any(),
                     axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass
    try:
        idx2 = [c for c in df[df.apply(
            lambda row: row.astype(str).str.lower().str.contains('statements are sent to customers').any(),
            axis=1) == True].index][0]
        df.drop(df.index[idx2:], inplace=True)
    except:
        pass

    try:
        df = df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains(
            'account number|holding|opening balance|statement of a/c|statement summary|generated on|generated by|brought forward').any(),
                                           axis=1) == True].index)]
    except:
        pass

    try:
        dat = [c for c in df.columns if "POSTING DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in df.columns if "POST DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in df.columns if "TRAN DATE" in str(c).upper()][0]
            except:
                try:
                    dat = [c for c in df.columns if "TXN DATE" in str(c).upper()][0]
                except:
                    try:
                        dat = [c for c in df.columns if "Txn\nDate" in str(c).upper()][0]
                    except:
                        try:
                            dat = [c for c in df.columns if "TRANSACTION DATE" in str(c).upper()][0]
                        except:
                            try:
                                dat = [c for c in df.columns if "DATE" in str(c).upper()][0]
                            except:
                                try:
                                    dat = [c for c in df.columns if "DAT" in str(c).upper()][0]
                                except:
                                    print("Date column not found")
                                    pass

    try:
        chq = [c for c in df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in df.columns if "CHE" in str(c).upper()][0]
            except:
                try:
                    chq = [c for c in df.columns if "CH" in str(c).upper()][0]
                except:
                    try:
                        df["CHEQUE"] = np.nan
                        chq = [c for c in df.columns if "CHEQUE" in str(c).upper()][0]
                    except:
                        print("Cheque column not found.")
                        pass

    try:
        narr = [c for c in df.columns if "DESCRIPTION" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in df.columns if "REMARK" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in df.columns if "PARTICULAR" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in df.columns if "NARRATION" in str(c).upper()][0]
                except:
                    try:
                        narr = [c for c in df.columns if "TRANSACTION" in str(c).upper()][0]
                    except:
                        try:
                            narr = [c for c in df.columns if "DETAIL" in str(c).upper()][0]
                        except:
                            print("Narration column not found.")
                            pass

    try:
        bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
        df["bal"] = df[bal]
    except:
        try:
            bal = [c for c in df.columns if "BOOKBAL" in str(c).upper()][0]
            df["bal"] = df[bal]
        except:
            try:
                bal = [c for c in df.columns if "BAL" in str(c).upper()][0]
                df["bal"] = df[bal]
            except:
                try:
                    df["BALANCE"] = np.nan
                    bal = [c for c in df.columns if "BALANCE" in str(c).upper()][0]
                except:
                    print("Balance column not found.")
                    pass

    try:
        wdl = [c for c in df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in df.columns if "DEBIT" in str(c).upper()][0]
        except:
            try:
                wdl = [c for c in df.columns if "DR" in str(c).upper()][0]
            except:
                print("Withdraw column not found.")
                pass

    try:
        dep = [c for c in df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in df.columns if "CREDIT" in str(c).upper()][0]
        except:
            try:
                dep = [c for c in df.columns if "CR" in str(c).upper()][0]
                if str(dep).upper() == "DESCRIPTION":
                    dep = "CR"
                else:
                    pass
            except:
                print("Deposit column not found.")
                pass

    try:
        crdr = [c for c in df.columns if "DR/CR" in str(c).upper()][0]
    except:
        pass

    try:
        df[dep] = df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[bal] = df[bal].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df[dep] = df[dep].astype(str).apply(lambda x: str(x).replace("-", ""))
    except:
        pass
    try:
        df = df.replace(r'^\s*$', np.nan, regex=True)
    except:
        pass
    try:
        df[wdl] = df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace(
                "CR", "").replace("DR", "")).astype(float) * -1
    except Exception as e:
        print(e)
        print("wdl error")
        pass
    try:
        df[dep] = df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace(
                "CR", "").replace("DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("dep error")
        pass
    try:
        df[bal] = df[bal].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "").replace(
                "CR", "").replace("DR", "")).astype(float)
    except Exception as e:
        print(e)
        print("bal error")
        pass

    df['flag'] = df.iloc[:, 0].astype(str) + df[wdl].astype(str) + df[bal].astype(str)
    df['flag2'] = np.arange(len(df))
    df.loc[df[['flag']].duplicated(keep=False), 'flag'] = df['flag'] + df['flag2'].astype(str)
    df['flag'] = df['flag'].apply(lambda row: np.nan if 'nannan' in row else row).fillna(method='ffill')
    df[narr] = df.groupby('flag')[narr].transform(lambda x: ''.join(x))
    df = df.drop_duplicates(['flag'], keep='first').reset_index(drop=True)

    try:
        for i in df.index:
            if "Dr" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            elif "DR" in df["bal"][i]:
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    try:
        for i in df.index:
            if df[crdr][i] == "Dr":
                df[bal][i] = df[bal][i] * -1
            elif df[crdr][i] == "DR":
                df[bal][i] = df[bal][i] * -1
            else:
                pass
    except:
        pass

    df = df[[dat, chq, narr, wdl, dep, bal]]
    df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    p = r'\b(\d+-\d+-\d+)\b'
    df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
    df["Narration"] = df["Narration"].str.replace(p, "")

    try:
        df["Xns Date"] = df["Xns Date"].str.replace('\r', '')
        df["Xns Date"] = pd.to_datetime(df["Xns Date"], dayfirst=True)
        df["Xns Date"] = df["Xns Date"].dt.date.astype(str)

        if df["Xns Date"][2] > df["Xns Date"][len(df) - 1]:
            df = df.iloc[::-1]
            df.reset_index(inplace=True, drop=True)
    except:
        pass

    df.to_excel("check_csv.xlsx", index=0)
    return df


def state_file28(job_id, analytics_id, bankname, filename, password):
    try:
        df = lattice_parsing(filename, password)
    except:
        try:
            df = df_stream(filename, password)
        except:
            df = df_camelot(filename, password)

    return df
