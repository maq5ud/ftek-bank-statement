import pandas as pd
import numpy as np
import tabula
import fitz
import math
pd.set_option('mode.chained_assignment', None)


def abc(a):
    if type(a) == str:
        if len(a.split(' ')) == 2:
            z = a.split(' ')[1]
        else:
            z = a.split(' ')[0]
    else:
        z = a
    return z


def isnan(value):
    try:
        return math.isnan(float(value))
    except:
        return False


def state_file29(job_id,analytics_id,bankname,f,pas):
    doc = fitz.open(f)
    page_no = doc.pageCount

    tables = tabula.read_pdf_with_template(f, "tabula-01.02.18 TO 23.03.18 (2).json")
    df = pd.DataFrame()
    df = pd.concat([c for c in tables]).drop_duplicates()
    df = df.replace(r'^\s*$', np.nan, regex=True)
    df = df[df.isnull().sum(axis=1) < df.shape[1] - 2].reset_index(drop=True)

    tables1 = tabula.read_pdf_with_template(f, "tabula-01.02.18 TO 23.03.18 (1).json")
    df1 = pd.DataFrame()
    df1 = pd.concat([c for c in tables1]).drop_duplicates()
    df1 = df1.replace(r'^\s*$', np.nan, regex=True)
    df1 = df1[df1.isnull().sum(axis=1) < df1.shape[1] - 2].reset_index(drop=True)


    #df.columns = df.iloc[0]
    #df = df[1:]
    #print(df)

    df = df.drop("Particulars", axis=1)
    df.columns = ["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No"]

    df = df.drop(0)

    tables1[0] = tables1[0].append(pd.Series(), ignore_index=True)
    tables1[0] = tables1[0].shift(periods=1)

    result = pd.concat([df, tables1[0]], axis=1)
    result = result.drop(0)

    main_df = pd.DataFrame(
        columns=["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No", "Debit", "Credit", "Balance"])

    main_df = main_df.append(result)
    try:
        len_row = len(main_df["Txn Date"])
        for i in range(1, len_row):
            if str(main_df["Txn Date"].iloc[i]) == "nan":
                if str(main_df["Branch Code"].iloc[i]) == "nan":
                    if str(main_df["Particulars"].iloc[i]) == "TOTAL":
                        main_df = main_df[1:i]
                        break
    except:
        pass

    import json
    import os

    # for i in range((page_no-1),(page_no)):
    # for i in range(2,page_no):
    for i in range(2, page_no):
        # print(i)
        filename = "tabula-01.02.18 TO 23.03.18 (10).json"
        with open(filename, 'r') as b:
            data = json.load(b)
            data[0]['page'] = i
        os.remove(filename)
        with open(filename, 'w') as b:
            json.dump(data, b, indent=4)

        filename1 = "tabula-01.02.18 TO 23.03.18 (11).json"
        with open(filename1, 'r') as b:
            data = json.load(b)
            data[0]['page'] = i
        os.remove(filename1)
        with open(filename1, 'w') as b:
            json.dump(data, b, indent=4)

        tables_none = tabula.read_pdf_with_template(f, "tabula-01.02.18 TO 23.03.18 (10).json")
        tables_ntwo = tabula.read_pdf_with_template(f, "tabula-01.02.18 TO 23.03.18 (11).json")
        tables_ntwo[0] = tables_ntwo[0].append(pd.Series(), ignore_index=True)
        tables_ntwo[0] = tables_ntwo[0].shift(periods=1)

        a = pd.concat([tables_none[0], tables_ntwo[0]], axis=1)

        len_col = len(a.columns)
        # print(len_col)
        # print(a)
        if len_col == 11:
            a.columns = ["Txn", "Value", "Brn", "Narration", "Particulars", "Ref. No", "Debit", "A", "Credit", "B",
                         "Balance"]
            a = a.drop("Particulars", axis=1)
            a = a.drop("A", axis=1)
            a = a.drop("B", axis=1)
            a = a.drop(0)
            a.columns = ["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No", "Debit", "Credit",
                         "Balance"]
            main_df = main_df.append(a)

        elif len_col == 10:
            a.columns = ["Txn", "Value", "Brn", "Narration", "Particulars", "Ref. No", "Debit", "A", "Credit",
                         "Balance"]
            a = a.drop("Particulars", axis=1)
            a = a.drop("A", axis=1)
            a = a.drop(0)
            a.columns = ["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No", "Debit", "Credit",
                         "Balance"]

            main_df = main_df.append(a)

        elif len_col == 6:

            #         if (a["Unnamed: 0"] and a["Unnamed: 1"]) in a.columns:
            #             break;
            # print(a)

            try:
                if str(a["Date Date"].iloc[0]) == "nan":
                    if str(a["Code"].iloc[0]) == "nan":
                        if str(a["Unnamed: 0"].iloc[0]) == "TOTAL":
                            print("@$@$@$@$@$")
                            break
            except:
                try:
                    len_row = len(a["Txn Value"])
                    # print(len_row)
                    for i in range(1, len_row):
                        # if str(df["Narration"].iloc[i]) == "nan":
                        if str(a["Txn Value"].iloc[i]) == "nan":
                            if str(a["Brn"].iloc[i]) == "nan":
                                if str(a["Particulars"].iloc[i]) == "TOTAL":
                                    a = a[1:i]
                                    break
                except:
                    try:
                        len_row = len(a["Txn Value"])
                        for i in range(1, len_row):
                            # if str(df["Narration"].iloc[i]) == "nan":
                            if str(a["Brn"].iloc[i]) == "nan":
                                if str(a["Particulars"].iloc[i]) == "nan":
                                    print(i)
                                    if str(a["Balance"].iloc[i]) == "nan":
                                        print(i)
                                        a = a[1:i]
                                        break
                    except:
                        pass

                # print(a)
                if "Debit Credit" in a:
                    a.insert(4, "Debit", " ")

                    a.insert(5, "Credit", " ")

                    # print(main_df)
                    # print(main_df['Balance'].iloc[-1])
                    print(a)
                    print(a.columns)
                    val = float(a['Balance'].iloc[0].replace(',', '')) - float(
                        main_df['Balance'].iloc[-1].replace(',', ''))
                    if val > 0:
                        a["Credit"].iloc[0] = a["Debit Credit"].iloc[0]
                    elif val < 0:
                        a["Debit"].iloc[0] = a["Debit Credit"].iloc[0]

                    # len(a["Debit Credit"])

                    for i in range(2, len(a["Debit Credit"]) + 1):
                        # print(i)
                        value = float(a['Balance'].iloc[i - 1].replace(',', '')) - float(
                            a['Balance'].iloc[i - 2].replace(',', ''))
                        # print(value)
                        # value = float(a["Balance"].iloc[i]) - float(a["Balance"].iloc[i-1])
                        if value > 0:
                            a["Credit"].iloc[i - 1] = a["Debit Credit"].iloc[i - 1]
                        elif value < 0:
                            a["Debit"].iloc[i - 1] = a["Debit Credit"].iloc[i - 1]
                    #             print("ADADASDA")

                    a = a.drop("Debit Credit", axis=1)

                a.columns = ["Txn", "Brn", "Particulars", "Ref. No", "Debit", "Credit", "Balance"]

                a.insert(1, "Value", " ")
                for i in range(1, len(a["Txn"]) + 1):
                    x = a["Txn"].iloc[i - 1]
                    y = x[1:10]
                    z = x[11:]
                    a["Value"].iloc[i - 1] = z
                    a["Txn"].iloc[i - 1] = y

                a.columns = ["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No", "Debit", "Credit",
                             "Balance"]
                # print(a)
                main_df = main_df.append(a)

        elif len_col == 8:

            a = a.drop(0)
            a.columns = ["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No", "Debit", "Credit",
                         "Balance"]
            main_df = main_df.append(a)

        elif len_col == 9:
            print(a)
            a = a.drop("Particulars", axis=1)
            a.columns = ["Txn Date", "Value Date", "Branch Code", "Particulars", "Ref. No", "Debit", "Credit",
                         "Balance"]
            main_df = main_df.append(a)

        else:
            pass

    try:
        dat = [c for c in main_df.columns if "TRANSACTION DATE" in str(c).upper()][0]
    except:
        try:
            dat = [c for c in main_df.columns if "TXN DATE" in str(c).upper()][0]
        except:
            try:
                dat = [c for c in main_df.columns if "DATE" in str(c).upper()][0]
            except:
                pass

    try:
        chq = [c for c in main_df.columns if "CHQ" in str(c).upper()][0]
    except:
        try:
            chq = [c for c in main_df.columns if "CHEQUE" in str(c).upper()][0]
        except:
            try:
                chq = [c for c in main_df.columns if "REF" in str(c).upper()][0]
            except:
                pass

    try:
        narr = [c for c in main_df.columns if "REMARKS" in str(c).upper()][0]
    except:
        try:
            narr = [c for c in main_df.columns if "PARTICULARS" in str(c).upper()][0]
        except:
            try:
                narr = [c for c in main_df.columns if "DESCRIPTION" in str(c).upper()][0]
            except:
                try:
                    narr = [c for c in main_df.columns if "DETAILS" in str(c).upper()][0]
                except:
                    pass

    try:
        bal = [c for c in main_df.columns if "BALANCE" in str(c).upper()][0]
        main_df["bal"] = main_df[bal].apply(lambda x: abc(x))
    except:
        print("\nBalance columns missing")

    try:
        wdl = [c for c in main_df.columns if "WITHDRAW" in str(c).upper()][0]
    except:
        try:
            wdl = [c for c in main_df.columns if "DEBIT" in str(c).upper()][0]
        except:
            pass

    try:
        dep = [c for c in main_df.columns if "DEPOSIT" in str(c).upper()][0]
    except:
        try:
            dep = [c for c in main_df.columns if "CREDIT" in str(c).upper()][0]
        except:
            print("DEP NOT FOUND")
            pass

    try:
        main_df[dep] = main_df[dep].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        main_df[wdl] = main_df[wdl].apply(lambda x: x.split(' ')[0] if type(x) == str else x)
    except:
        pass
    try:
        main_df[wdl] = main_df[wdl].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float) * -1
    except:
        pass
    try:
        main_df[dep] = main_df[dep].astype(str).apply(
            lambda x: str(x).replace("\r", "").replace(",", "").replace("Cr", "").replace("Dr", "")).astype(float)
    except:
        pass

    main_df = main_df[[dat, chq, narr, wdl, dep, bal]]
    main_df.columns = ["Xns Date", "Cheque No", "Narration", "Debits", "Credits", "Balance"]

    main_df = main_df[main_df['Balance'].notna()]

    try:
        main_df["Xns Date"] = pd.to_datetime(main_df["Xns Date"], dayfirst=True)
        main_df["Xns Date"] = main_df["Xns Date"].dt.date.astype(str)

        if main_df["Xns Date"][0] > main_df["Xns Date"][len(main_df) - 1]:
            main_df = main_df.iloc[::-1]
            main_df.reset_index(inplace=True, drop=True)
    except:
        pass

    return main_df
