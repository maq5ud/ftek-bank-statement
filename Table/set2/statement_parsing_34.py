import pandas as pd
import numpy as np
import tabula

def state_file34(job_id,analytics_id,bankname,filename,password):
	tables=tabula.read_pdf(filename,
							  stream=True,
							  pages="all",
							  silent=True,
							  multiple_tables=True,
							  password = password,
							  pandas_options={'header':None})

	df = pd.DataFrame()
	df = pd.concat([c for c in tables]).drop_duplicates()
	df.reset_index(inplace=True, drop=True)

	df = df.replace(r'^\s*$', np.nan, regex=True)

	try:
		idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('date').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index][0]
		# print(idx)
		df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
	except:
		try:
			# DAT_VALUE, BOOKBAL
			idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('dat').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('bal').any(),axis=1)==True].index][0]
			df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
		except:
			try:
				idx=[c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('date').any(),axis=1)==True].index if c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('description').any(),axis=1)==True].index][0]
				df.columns=df.iloc[idx]; df=df.iloc[idx+1:,:]; df.reset_index(drop=True, inplace=True)
			except:
				print("Column values not found")
				pass

	try:
		df.reset_index(drop=True, inplace=True)
		idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('closing balance').any(),axis=1)==True].index][0]    
		df.drop(df.index[idx2:], inplace=True)
	except:pass

	try:
		df.reset_index(drop=True, inplace=True)
		idx2 = [c for c in df[df.apply(lambda row:row.astype(str).str.lower().str.contains('total').any(),axis=1)==True].index][0]    
		df.drop(df.index[idx2:], inplace=True)
	except:pass

	try:
		df.reset_index(drop=True, inplace=True)
		idx2 = [ c for c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('date').any(), axis=1) ==True].index if c in df[df.apply(lambda row: row.astype(str).str.lower().str.contains('balance').any(), axis=1) ==True].index ]   
		# print("IDX:",idx2)
		df.drop(df.index[idx2], inplace=True)
	except:pass

	try:
		df=df[~df.index.isin(df[df.apply(lambda row: row.astype(str).str.lower().str.contains('account number|holding|opening balance|statement of a/c|statement summary|generated on|generated by|brought forward').any(), axis=1) ==True].index)]
	except:pass

	try:
		dat=[c for c in df.columns if "POSTING DATE" in str(c).upper() ][0]
	except:
		try:
			dat=[c for c in df.columns if "POST DATE" in str(c).upper() ][0]
		except:
			try:
				dat=[c for c in df.columns if "TRAN DATE" in str(c).upper() ][0]
			except:
				try:
					dat=[c for c in df.columns if "TXN DATE" in str(c).upper() ][0]
				except:
					try:
						dat=[c for c in df.columns if "Txn\nDate" in str(c).upper() ][0]
					except:
						try:
							dat=[c for c in df.columns if "TRANSACTION DATE" in str(c).upper() ][0]
						except:
							try:
								dat=[c for c in df.columns if "DATE" in str(c).upper() ][0]
							except:
								try:
									dat=[c for c in df.columns if "DAT" in str(c).upper() ][0]
								except:
									print("Date column not found")
									pass
						
	try:
		chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
	except:
		try:
			chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
		except:
			try:
				chq=[c for c in df.columns if "CHE" in str(c).upper() ][0]
			except:
				try:
					chq=[c for c in df.columns if "CH" in str(c).upper() ][0]
				except:
					try:
						df["CHEQUE"] = np.nan
						chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
					except:
						print("Cheque column not found.")
						pass
				
	try:
		narr=[c for c in df.columns if "DESCRIPTION" in str(c).upper() ][0]
	except:
		try:
			narr=[c for c in df.columns if "REMARK" in str(c).upper() ][0]
		except:
			try:
				narr=[c for c in df.columns if "PARTICULAR" in str(c).upper() ][0]
			except:
				try:
					narr=[c for c in df.columns if "NARRATION" in str(c).upper() ][0]
				except:
					try:
						narr=[c for c in df.columns if "TRANSACTION" in str(c).upper() ][0]
					except:
						try:
							narr=[c for c in df.columns if "DETAIL" in str(c).upper() ][0]
						except:
							print("Narration column not found.")
							pass
						
	try:
		bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
		df["bal"]=df[bal]
	except:
		try:
			bal=[c for c in df.columns if "BOOKBAL" in str(c).upper() ][0]
			df["bal"]=df[bal]
		except:
			try:
				bal=[c for c in df.columns if "BAL" in str(c).upper() ][0]
				df["bal"]=df[bal] 
			except:
				try:
					df["BALANCE"] = np.nan
					bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
				except:
					print("Balance column not found.")
					pass
			
	try:
		wdl=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			wdl=[c for c in df.columns if "DEBIT" in str(c).upper() ][0]
		except:
			try:
				wdl=[c for c in df.columns if "DR" in str(c).upper() ][0]
			except:
				print("Withdraw column not found.")
				pass
			
	try:
		dep=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
	except:
		try:
			dep=[c for c in df.columns if "CREDIT" in str(c).upper() ][0]
		except:
			try:
				dep=[c for c in df.columns if "CR" in str(c).upper() ][0]
				if str(dep).upper() == "DESCRIPTION":
					dep = "CR"
				else:pass
			except:
				print("Deposit column not found.")
				pass
			
	try:
		crdr=[c for c in df.columns if "DR/CR" in str(c).upper() ][0]
	except:
		pass

	try:
		df[dep]=df[dep].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	except:pass
	try:
		df[wdl]=df[wdl].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	except:pass
	try:
		df[bal]=df[bal].apply( lambda x: x.split(' ')[0] if type(x) == str else x )
	except:pass
	try:
		df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("-",""))
	except:
		pass
	try:
		df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("-",""))
	except:
		pass
	try:
		df = df.replace(r'^\s*$', np.nan, regex=True)
	except:pass
	try:
		df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","").replace("CR","").replace("DR","")).astype(float) *-1
	except Exception as e:
		print(e)
		print("wdl error")
		pass
	try:
		df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","").replace("CR","").replace("DR","")).astype(float)
	except Exception as e:
		print(e)
		print("dep error")
		pass
	try:
		df[bal]=df[bal].astype(str).apply(lambda x:str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","").replace("CR","").replace("DR","")).astype(float)
	except Exception as e:
		print(e)
		print("bal error")
		pass

	df['flag']=df.iloc[:,0].astype(str)+df[wdl].astype(str) +df[bal].astype(str)
	df['flag2']=np.arange(len(df))
	df.loc[ df[['flag']].duplicated(keep=False) , 'flag' ] =df['flag'] + df['flag2'].astype(str)
	df['flag']=df['flag'].apply( lambda row : np.nan if 'nannan' in row else row ).fillna(method='ffill')
	df[narr]=df.groupby('flag')[narr].transform( lambda x :''.join(x)) 
	df=df.drop_duplicates(['flag'],keep='first').reset_index(drop=True)

	try:
		for i in df.index:
			if "Dr" in df["bal"][i]:
				df[bal][i] = df[bal][i]*-1
			elif "DR" in df["bal"][i]:
				df[bal][i] = df[bal][i]*-1
			else:
				pass
	except:pass

	try:
		for i in df.index:
			if df[crdr][i] == "Dr":
				df[bal][i] = df[bal][i]*-1
			elif df[crdr][i] == "DR":
				df[bal][i] = df[bal][i]*-1
			else:
				pass
	except:pass

	df = df[[dat,chq,narr,wdl,dep,bal]]
	df.columns = ["Xns Date","Cheque No","Narration","Debits","Credits","Balance"]

	try:
		p = r'\b(\d+-\d+-\d+)\b'
		df["Xns Date"] = df["Xns Date"].str.extract(p, expand=False)
		df["Narration"] = df["Narration"].str.replace(p, "")
	except:pass

	try:
		df["Xns Date"]=df["Xns Date"].str.replace('\r','')
		df["Xns Date"]=pd.to_datetime(df["Xns Date"], dayfirst=True)
		df["Xns Date"]=df["Xns Date"].dt.date.astype(str)

		if df["Xns Date"][2]>df["Xns Date"][len(df)-1]:
			df = df.iloc[::-1]
			df.reset_index(inplace = True, drop =True)
	except:pass

	df.to_excel("check_csv.xlsx", index=0)

	# print("parsed")
	return df