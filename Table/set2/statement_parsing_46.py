import pandas as pd
import numpy as np
import tabula
import camelot

def state_file46(job_id,analytics_id,bankname,filename,password):
	ex = camelot.read_pdf(filename, flavor='stream', pages="all", password=password)
	tables = ex

	if len(tables) != 0:
		dt=pd.DataFrame(); tmp=pd.DataFrame()
		for i in range(len(tables)):
			print(i)
			tmp=tables[i].df
			if (tmp.shape[1] < 5):
				print("\nDiiferent structure on page:",i); pass
			elif (tmp.shape[1] >= 5):
				tmp=pd.DataFrame(tmp).replace(r'^\s*$', np.nan, regex=True)
				try:
					idx=[c for c in tmp[tmp.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index if c in tmp[tmp.apply(lambda row:row.astype(str).str.lower().str.contains('date').any(),axis=1)==True].index][0]
					tmp.columns=tmp.iloc[idx]; tmp=tmp.iloc[idx+1:,:]; tmp.reset_index(drop=True, inplace=True)
				except:
					try:
						idx=[c for c in tmp[tmp.apply(lambda row:row.astype(str).str.lower().str.contains('balance').any(),axis=1)==True].index if c in tmp[tmp.apply(lambda row:row.astype(str).str.lower().str.contains('particulars').any(),axis=1)==True].index][0]
						tmp.columns=tmp.iloc[idx]; tmp=tmp.iloc[idx+1:,:]; tmp.reset_index(drop=True, inplace=True)
					except:pass
				dt = pd.concat([dt,tmp]).reset_index(drop=True)
			else:
				print("\nAnother Structure on page:",i); pass
	df = dt

	try:
		dat=[c for c in df.columns if "POSTING DATE" in str(c).upper() ][0]
	except:
		try:
			dat=[c for c in df.columns if "POST DATE" in str(c).upper() ][0]
		except:
			try:
				dat=[c for c in df.columns if "TRAN DATE" in str(c).upper() ][0]
			except:
				try:
					dat=[c for c in df.columns if "TXN DATE" in str(c).upper() ][0]
				except:
					try:
						dat=[c for c in df.columns if "TRANSACTION DATE" in str(c).upper() ][0]
					except:
						try:
							dat=[c for c in df.columns if "DATE" in str(c).upper() ][0]
						except:
							try:
								dat=[c for c in df.columns if "DAT" in str(c).upper() ][0]
							except:
								print("Date column not found")
								pass

	try:
		chq=[c for c in df.columns if "CHQ" in str(c).upper() ][0]
	except:
		try:
			chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
		except:
			try:
				df["CHEQUE"] = np.nan
				chq=[c for c in df.columns if "CHEQUE" in str(c).upper() ][0]
			except:
				print("Cheque column not found.")
				pass

	try:
		narr=[c for c in df.columns if "DESCRIPTION" in str(c).upper() ][0]
	except:
		try:
			narr=[c for c in df.columns if "REMARK" in str(c).upper() ][0]
		except:
			try:
				narr=[c for c in df.columns if "PARTICULAR" in str(c).upper() ][0]
			except:
				try:
					narr=[c for c in df.columns if "NARRATION" in str(c).upper() ][0]
				except:
					try:
						narr=[c for c in df.columns if "TRANSACTION" in str(c).upper() ][0]
					except:
						try:
							narr=[c for c in df.columns if "DETAIL" in str(c).upper() ][0]
						except:
							print("Narration column not found.")
							pass

	try:
		bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
		df["bal"]=df[bal]
	except:
		try:
			bal=[c for c in df.columns if "BOOKBAL" in str(c).upper() ][0]
			df["bal"]=df[bal]
		except:
			try:
				bal=[c for c in df.columns if "BAL" in str(c).upper() ][0]
				df["bal"]=df[bal] 
			except:
				try:
					df["BALANCE"] = np.nan
					bal=[c for c in df.columns if "BALANCE" in str(c).upper() ][0]
				except:
					print("Balance column not found.")
					pass

	try:
		amt=[c for c in df.columns if "AMOUNT" in str(c).upper() ][0]
	except:pass

	try: 
		if amt in df.columns:
			df["Debits"] = np.nan; df["Credits"] = np.nan;
			for i,j in enumerate(df[amt]):
				if "Dr" in str(j):
					df["Debits"][i] = df[amt][i]
				elif "Cr" in str(j):
					df["Credits"][i] = df[amt][i]
				else:pass
	except Exception as e:
		print(e)
		pass
				
	try:
		wdl=[c for c in df.columns if "WITHDRAW" in str(c).upper() ][0]
	except:
		try:
			wdl=[c for c in df.columns if "DEBIT" in str(c).upper() ][0]
		except:
			try:
				wdl=[c for c in df.columns if "DR" in str(c).upper() ][0]
			except:
				print("Withdraw column not found.")
				pass

	try:
		dep=[c for c in df.columns if "DEPOSIT" in str(c).upper() ][0]
	except:
		try:
			dep=[c for c in df.columns if "CREDIT" in str(c).upper() ][0]
		except:
			try:
				dep=[c for c in df.columns if "CR" in str(c).upper() ][0]
				if str(dep).upper() == "DESCRIPTION":
					dep = "CR"
				else:pass
			except:
				print("Deposit column not found.")
				pass

	try:
		df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("Rs.","").replace("Dr",""))
	except:
		pass
	try:
		df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("Rs.","").replace("Cr",""))
	except:
		pass
	try:
		df[bal]=df[bal].astype(str).apply(lambda x: str(x).replace("Rs.",""))
	except:
		pass
	try:
		df = df.replace(r'^\s*$', np.nan, regex=True)
	except:pass
	try:
		df[wdl]=df[wdl].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","").replace("CR","").replace("DR","")).astype(float) *-1
	except Exception as e:
		print(e)
		print("wdl error")
		pass
	try:
		df[dep]=df[dep].astype(str).apply(lambda x: str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","").replace("CR","").replace("DR","")).astype(float)
	except Exception as e:
		print(e)
		print("dep error")
		pass
	try:
		df[bal]=df[bal].astype(str).apply(lambda x:str(x).replace("\r","").replace(",","").replace("Cr","").replace("Dr","").replace("CR","").replace("DR",""))
	except Exception as e:
		print(e)
		print("bal error")
		pass

	df['Wdl1']=df[wdl].fillna(df[dep].astype(str).apply(lambda x: str(x).replace(",","").replace("(Cr)","").replace("(Dr)","")).astype(float) )
	df['flag']=df.iloc[:,0].astype(str)+df['Wdl1'].astype(str) +df[bal].astype(str)
	df['flag2']=np.arange(len(df))

	df.loc[ df[['flag']].duplicated(keep=False) , 'flag' ] =df['flag'] + df['flag2'].astype(str) 
	df['flag']=df['flag'].apply( lambda row : np.nan if 'nannan' in row else row ).fillna(method='ffill')   
	# print(df.head(40))

	df[narr] = df[narr].astype(str) 

	df[narr]=df.groupby('flag')[narr].transform( lambda x :''.join(x))                
	df=df.drop_duplicates(['flag'],keep='first').iloc[0:,:-3].reset_index(drop=True)

	df = df[[dat,chq,narr,wdl,dep,bal]]
	df.columns = ["Xns Date","Cheque No","Narration","Debits","Credits","Balance"]
	df["Narration"] = df["Narration"].str.replace('\r','')
	df=df.drop(df.index[0])

	try:
		df["Xns Date"]=df["Xns Date"].str.replace('\r','')
		df["Xns Date"]=pd.to_datetime(df["Xns Date"], dayfirst=True)
		df["Xns Date"]=df["Xns Date"].dt.date.astype(str)

		if df["Xns Date"][2]>df["Xns Date"][len(df)-1]:
			df = df.iloc[::-1]
			df.reset_index(inplace = True, drop =True)
	except:pass

	print("Parsed...")
	df.to_excel("check.xlsx")

	return df
