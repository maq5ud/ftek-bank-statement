from flask import Flask, request, jsonify
from index04 import main_file
from pprint import pprint
from Helper.date import date
from Logo.logo import logo

"""
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn="https://0c1634ff4bad40fd9a901f4e40850d3e@o310832.ingest.sentry.io/5193790",
    integrations=[FlaskIntegration()]
)"""

app = Flask(__name__)


@app.route('/statement-parsing', methods=['POST'])
def parse():
    if request.method == "POST":
        pprint(request.__dict__)
        json_dict = request.json
        print(json_dict)


        job_id = json_dict['job_id']
        analytics_id = json_dict['analytics_id']
        bankname = json_dict['bankname']  # Drop down
        filelist = json_dict['filename']
        password = json_dict['password']
        startdate = json_dict['startdate']
        enddate = json_dict['enddate']
        # print(job_id,analytics_id,bankname,filelist,password)

        if password != None:
            password = str(password)
        else:
            password = ""
            pass

        startdate, enddate = date(startdate, enddate)

        result_01 = main_file(job_id, analytics_id, bankname, filelist, password, startdate, enddate)

        return result_01


@app.route('/logo-detection', methods=['POST'])
def par():
    if request.method == "POST":
        json_dict = request.json
        filelist = json_dict['filename']
        password = json_dict['password']
        bankId = logo(filelist, password)

    return bankId


app.run(host='0.0.0.0', port=5006)
