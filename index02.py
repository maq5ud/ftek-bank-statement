import glob
import json
import os
from inspect import trace

import pandas as pd
import tabula
from Analytics.meta_data import meta
from Analytics.sunday_trans import fraud
from Table.connector import table_connector
from Headers.cust_info import header
from Headers.get_bankcode import bankcode
from Categorisation.makecategory import categorisation
from Database.access_bank import access_bank_details
from Database.get_filesize import get_file_size
from Database.parsing_exception import parsing
from Database.access_db import update_db
from Database.access_branch import access_branch
from Database.access_ftek_meta_data import update_ftek_meta_data
from Database.access_new_ftek_meta_data_detail import update_new_ftek_meta_detail
from Database.access_ftek_customer import update_ftek_customer
from Database.access_ftek_bank_details import update_ftek_bank_details
from Database.access_ftek_xns import update_ftek_xns
from Database.access_ftek_feature import update_ftek_feature
from Database.monthly_details import update_monthly_details
from Analytics.main_analytics import main_ana
from Analytics.basic_feature import ratios
from Analytics.main_feature import featurelake
from PyPDF2 import PdfFileReader
import sys



# from Logo.logo_detection import logo
sys.path.append('./../')


def main_file(job_id, analytics_id, bankname, filelist, password, startdate, enddate, startdate1, enddate1):
    pth = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'case_1')
    # print(pth)
    os.chdir(pth)
    # Meta Data
    filename = filelist[0]

    try:
        tabula.read_pdf(filename, password=password, pages=1)
    except:
        try:
            pass_error = Exception.__class__.__name__
            if pass_error == "CalledProcessError":
                print(pass_error)
                dic = {
                    "Status": "Unsuccessful",
                    "code": 201,
                    "description": "Wrong password"
                }
                return dic
        except:
            pass


    try:
        meta_output = meta(filename, password)
        meta_df = pd.DataFrame.from_dict(meta_output, orient='index')
        print(meta_df)
        print("Meta data created")
    except:
        data_df = {"0": ["Meta data not created"]}
        meta_df = pd.DataFrame(data_df, index=[0])
        print("Meta data not created")
        pass

    json_data1 = meta_df.to_json(r'E:\bank-statement-master\json_data1.json', orient="split")

    """
    # logo
    try:
        logo(filename,password)
        print("................................logo detector working...................................")
    except:
        print("................................logo detector not working...............................")
        pass
    """



    # header
    try:
        header_df, bankname, ifsc_code, mobilenumber = header(filename, password, bankname)
        # print(header_df)
        # header = extract_data(filename, bankname)
        print("Header data created")
    except Exception as e:
        print("Header Error:", e)
        # data_df = {"0" : ["Table data not created"]}
        # table_df = pd.DataFrame(data_df,index=[0])
        print("Header data not created")
        trace = []
        tb = e.__traceback__
        while tb is not None:
            trace.append({
                "filename": tb.tb_frame.f_code.co_filename,
                "name": tb.tb_frame.f_code.co_name,
                "lineno": tb.tb_lineno
            })
            tb = tb.tb_next
        dic = {
            "Status": "Unsuccessful",
            "code": 201,
            "description": "Header Error",
            "Exception": {
            "type": type(e).__name__,
            "message": str(e),
            "trace": trace
            }
        }

        parsing_dict = {"type": type(e).__name__, "message": str(e), "path": trace[1]["filename"], "lineno": trace[1]["lineno"]}

        try:
            parsing(parsing_dict)
        except Exception as e:
            print("Parsing Statement error:", e)
            pass


        return dic
    # print("Header Error:",e)
    # data_df = {"0" : ["Header data not created"]}
    # header_df = pd.DataFrame(data_df,index=[0])
    # print("Header data not created")
    
    
    # json_data2 =

    # access_branch(ifsc_code, mobilenumber)


    # Table
    tables_list = []
    # bankname = "SYNDICATE"
    try:
        for i, j in enumerate(filelist):
            try:
                dicts = {}
                table_df = table_connector(job_id, analytics_id, bankname, j, password)
                dicts["Dataframe"] = table_df
                print(table_df)
                try:
                    dicts["Dataframe_date"] = table_df["Xns Date"][1]
                except:
                    dicts["Dataframe_date"] = table_df["Xns Date"][0]
                tables_list.append(dicts)
                print("**********************************************************************************************")
                print(i, j, "Table data created")
                print("**********************************************************************************************")
            except Exception as e:
                print("Error", e)
                print(
                    "******************************************************************************************************************")
                print(i, j, "Table data not created")
                print(
                    "******************************************************************************************************************")

        tables_list.sort(key=lambda x: x['Dataframe_date'])
        new_tables_list = []
        for test_dict in tables_list:
            new_tables_list.append(test_dict["Dataframe"])
        table_df = pd.concat(new_tables_list)
        table_df.reset_index(drop=True, inplace=True)
        table_df.drop_duplicates(keep="first", inplace=True)
        table_df.reset_index(drop=True, inplace=True)
        table_df['Xns Date'] = pd.to_datetime(table_df["Xns Date"])
        # table_df = table_df.sort_values(by="Xns Date")
        try:
            table_df.drop_duplicates(subset=["Xns Date", "Cheque No", "Debits", "Credits", "Balance"], keep='first',
                                     inplace=True)
        except:
            table_df.drop_duplicates(subset=["Xns Date", "Debits", "Credits", "Balance"], keep='first',
                                     inplace=True)
        table_df.reset_index(drop=True, inplace=True)
        stored_table_df = table_df

        try:
            table_df["Xns Date"] = table_df["Xns Date"].str.replace('\r', '')
            table_df["Xns Date"] = pd.to_datetime(table_df["Xns Date"], dayfirst=True)
            table_df["Xns Date"] = table_df["Xns Date"].dt.date.astype(str)

            if table_df["Xns Date"][2] > table_df["Xns Date"][len(table_df) - 1]:
                table_df = table_df.iloc[::-1]
                table_df.reset_index(inplace=True, drop=True)
        except:
            pass

        table_df['Xns Date'] = pd.to_datetime(table_df['Xns Date']).dt.strftime('%Y-%m-%d')

        json_data3 = table_df.to_json(orient='records', date_format='iso')
        json_data3 = json.dumps(json_data3, indent=4, separators=("','", " ."))

        try:
            mask = (table_df["Xns Date"] >= startdate) & (table_df["Xns Date"] <= enddate)
            table_df = table_df.loc[mask]
            table_df.reset_index(drop=True, inplace=True)
            print("Table data created")
        except:
            print("Date range wrong.")
            dic = {
                "Status": "Unsuccessful",
                "code": 201,
                "description": "Uploaded date range wrong"
            }
            return dic

        print(table_df.head(10))

    except Exception as e:
        print("Table Error:", e)
        # data_df = {"0" : ["Table data not created"]}
        # table_df = pd.DataFrame(data_df,index=[0])
        print("Table data not created")
        trace = []
        tb = e.__traceback__
        while tb is not None:
            trace.append({
                "filename": tb.tb_frame.f_code.co_filename,
                "name": tb.tb_frame.f_code.co_name,
                "lineno": tb.tb_lineno
            })
            tb = tb.tb_next
        dic = {
            "Status": "Unsuccessful",
            "code": 201,
            "description": "Table Parsing Error",
            "Exception": {
            "type": type(e).__name__,
            "message": str(e),
            "trace": trace
            }
        }

        parsing_dict = {"type": type(e).__name__, "message": str(e), "path": trace[1]["filename"],
                        "lineno": trace[1]["lineno"]}

        try:
            parsing(parsing_dict)
        except Exception as e:
            print("Parsing Statement error:", e)
            pass

        return dic


    no_rows = len(table_df.axes[0])
    no_cols = len(table_df.axes[1])
    filesize = get_file_size(filename)
    pdf = PdfFileReader(open(filename, 'rb'))
    no_page = pdf.getNumPages()

    statement_dict = {'filename': filename, 'password': password, 'filesize_in_kb': filesize, 'no_of_rows': no_rows,
                      'no_of_pages': no_page, 'no_of_columns': no_cols}

    try:
        bcode = bankcode(bankname)
        access_bank_details(bankname, bcode, ifsc_code, mobilenumber, statement_dict)
        # statement_details(filename, filesize, no_rows, no_cols, no_page)
    except Exception as e:
        print("Access Bank Detail error:", e)
        pass








    # sundy_tran
    try:
        sunday_fraud = fraud(table_df)
        sunday_fraud_df = pd.DataFrame.from_dict(sunday_fraud, orient='index')
        print("Sunday transaction created.")
    except:
        data_df = {"0": ["Not found fraud sunday transaction"]}
        sunday_fraud_df = pd.DataFrame(data_df, index=[0])
        print("Not found fraud sunday transaction")
        pass

    # categorisation
    try:
        table_df_new = categorisation(table_df)
        print("Categorisation created.")
    except Exception as e:
        # print("Error Categorisation:",e)
        # table_df_new = table_df
        # print("Categorisation not created")
        print("Categorisation Error:", e)
        # data_df = {"0" : ["Table data not created"]}
        # table_df = pd.DataFrame(data_df,index=[0])
        print("Categorisation not created")
        dic = {
            "Status": "Unsuccessful",
            "code": 201,
            "description": "Categorisation Error"
        }
        return dic
    # pass

    # json_data4 = table_df_new.to_json(r'E:\bank-statement-master\json_data4.json', orient="records")

    result_json = [json_data3]




    # print(table_df_new.head(10))

    # analytics
    try:
        breakup_of_expenses, breakup_of_income, top_expenses, reccuring_expenses, loan_emi, bounced_penal, monthly_details, eod_balance, high_val_trans, irr_xns = main_ana(
            table_df_new)
        print("Analytics created.")
    except Exception as e:
        print("Error Analytics:", e)
        dic = {
            "Status": "Unsuccessful",
            "code": 201,
            "description": "Analytics Error"
        }
        return dic
    # pass

    print(table_df_new.head(10))
    table_df_new.to_excel("before_bf.xlsx")

    # features
    try:
        basic_feature = ratios(table_df_new)
    except Exception as e:
        print("Error features1:", e)
        dic = {
            "Status": "Unsuccessful",
            "code": 201,
            "description": "Basic Features Error"
        }
        return dic
    # pass

    try:
        main_feature = featurelake(table_df_new)
    except Exception as e:
        print("Error features2:", e)
        dic = {
            "Status": "Unsuccessful",
            "code": 201,
            "description": "Main Features Error"
        }
        return dic
    # pass

    # save excel to database
    try:
        directory = str(analytics_id)
        parent_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'case_1')
        path = os.path.join(parent_dir, directory)
        os.mkdir(path)
    except:
        pass
    try:
        sunday_fraud_df["Xns Date"] = sunday_fraud_df["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        breakup_of_expenses["Xns Date"] = breakup_of_expenses["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        breakup_of_income["Xns Date"] = breakup_of_income["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        top_expenses["Xns Date"] = top_expenses["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        reccuring_expenses["Xns Date"] = reccuring_expenses["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        loan_emi["Xns Date"] = loan_emi["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        bounced_penal["Xns Date"] = bounced_penal["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        monthly_details["Xns Date"] = monthly_details["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        eod_balance["Xns Date"] = eod_balance["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        high_val_trans["Xns Date"] = high_val_trans["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        irr_xns["Xns Date"] = irr_xns["Xns Date"].dt.date.astype(str)
    except:
        pass

    try:
        table_df_new["Xns Date"] = table_df_new["Xns Date"].dt.date.astype(str)
    except:
        pass
    try:
        table_df_new.drop("Day", axis=1, inplace=True)
    except:
        pass
    try:
        table_df_new.drop("Date", axis=1, inplace=True)
    except:
        pass
    try:
        table_df_new["Narration"] = table_df_new["Narration"].str.replace('\r', '')
    except:
        pass

    # pth = '/var/www/html/ftekcsv2/storage/app/public/bks/'
    # os.chdir(pth)

    # new_filename = 'check.xlsx'
    # with pd.ExcelWriter(new_filename) as writer:
    #   meta_df.to_excel(writer, sheet_name='Meta Data')
    #   header_df.to_excel(writer, sheet_name='Customer Information', index = False)
    #   sunday_fraud_df.to_excel(writer, sheet_name='Sunday Fraud Transactions', index = False)
    #   breakup_of_expenses.to_excel(writer, sheet_name='Breakup of Expenses')
    #   breakup_of_income.to_excel(writer, sheet_name='BreakUp of Incomes')
    #   top_expenses.to_excel(writer, sheet_name='Top Expenses')
    #   reccuring_expenses.to_excel(writer, sheet_name='Recurring Expenses')
    #   loan_emi.to_excel(writer, sheet_name='Loans and EMI Transactions')
    #   bounced_penal.to_excel(writer, sheet_name='Bounced-Penal Xns')
    #   monthly_details.to_excel(writer, sheet_name='Monthly Details')
    #   eod_balance.to_excel(writer, sheet_name='EOD Balances')
    #   high_val_trans.to_excel(writer, sheet_name='High Value Transactions')
    #   irr_xns.to_excel(writer, sheet_name='FCU Irregular Xns')
    #   table_df_new.to_excel(writer, sheet_name='Xns', index = False)

    pth = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'case_1/') + str(analytics_id) + "/"
    os.chdir(pth)

    new_filename = str(job_id) + str("_") + str(bankname) + '.xlsx'
    with pd.ExcelWriter(new_filename) as writer:
        meta_df.to_excel(writer, sheet_name='Meta Data')
        header_df.to_excel(writer, sheet_name='Customer Information', index=False)
        sunday_fraud_df.to_excel(writer, sheet_name='Fraud Transactions', index=False)
        breakup_of_expenses.to_excel(writer, sheet_name='Breakup of Expenses')
        breakup_of_income.to_excel(writer, sheet_name='BreakUp of Incomes')
        top_expenses.to_excel(writer, sheet_name='Top Expenses')
        reccuring_expenses.to_excel(writer, sheet_name='Recurring Expenses')
        loan_emi.to_excel(writer, sheet_name='Loans and EMI Transactions')
        bounced_penal.to_excel(writer, sheet_name='Bounced-Penal Xns')
        monthly_details.to_excel(writer, sheet_name='Monthly Details')
        eod_balance.to_excel(writer, sheet_name='EOD Balances')
        high_val_trans.to_excel(writer, sheet_name='High Value Transactions')
        irr_xns.to_excel(writer, sheet_name='FCU Irregular Xns')
        table_df_new.to_excel(writer, sheet_name='Xns', index=False)
        basic_feature.to_excel(writer, sheet_name='Features', index=False)
        main_feature.to_excel(writer, sheet_name='Category_Features', index=False)
    # meta_df.to_excel(writer, sheet_name='Meta Data')
    # header_df.to_excel(writer, sheet_name='Customer Information', index = False)
    # table_df_new.to_excel(writer, sheet_name='Xns', index = False)
    # sunday_fraud_df.to_excel(writer, sheet_name='Sunday Fraud Transactions', index = False)





    # update_db(new_filename, job_id)







    # save data to database
    """try:
        pth = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'case_1')
        update_ftek_meta_data(meta_df, pth, filelist, job_id)
    except Exception as e:
        print("ftek_meta_data error:", e)
        pass
    try:
        pth = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'case_1')
        update_new_ftek_meta_detail(meta_df, pth, filelist, job_id)
    except Exception as e:
        print("ftek_meta_data error:", e)
        pass
    try:
        update_ftek_customer(header_df, job_id)
    except Exception as e:
        print("ftek_customer error:", e)
        pass
    try:
        update_ftek_bank_details(header_df, job_id)
    except Exception as e:
        print("ftek_bank_details error:", e)
        pass
    try:
        update_ftek_xns(table_df_new, job_id)
    except Exception as e:
        print("ftek_xns erro:", e)
        pass
    try:
        update_ftek_feature(basic_feature, job_id)
    except Exception as e:
        print("basic_feature error:", e)
        pass
    try:
        update_monthly_details(monthly_details, table_df_new, job_id)
    except Exception as e:
        print("update_monthly_details error:", e)
        pass"""



    try:
        bcode = bankcode(bankname)
        access_bank_details(bankname, bcode, ifsc_code, mobilenumber)
    except Exception as e:
        print("Access Bank Detail error:", e)
        pass

    """try:
        access_branch(bankId, ifsc_code, mobilenumber)
    except Exception as e:
        print("Access Branch error:", e)
        pass"""




    """result = []
    for f in glob.glob("json_data*.json"):
        with open(f, "rb") as infile:
            result.append(json.load(infile))

    with open("merged_file.json", "wb") as outfile:
        json.dump(result, outfile)"""


    dic = {
        "Status": "Successful",
        "code": 200,
        "filename": new_filename,
        "data": json_data3
    }
    return dic
