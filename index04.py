import pandas as pd
import tabula
import json
from Analytics.meta_data import meta
from Analytics.sunday_trans import fraud
from Database.main import database
from Headers.cust_info import header
from Table.main import table_main
from Categorisation.makecategory import categorisation
from Database.access_db import update_db
from Analytics.main_analytics import main_ana
from Analytics.basic_feature import ratios
from Analytics.main_feature import featurelake
from Exception.exception import throwException
from Excel.main import save_to_excel
from Dictionary.main import dict
import sys
from PyPDF2 import PdfFileReader
from Helper.get_filesize import get_file_size
from Helper.helper import *
from Database.update_db import update_statement_record

sys.path.append('./../')


# table_df_new.to_csv('all_statements.csv', mode='a', header=False)


def main_file(job_id, user_id, bankname, filelist, password, startdate, enddate):
    path = getPath(user_id)

    print("PATH", path)
    os.chdir(path)
    # Meta Data
    print("FILELIST", filelist)
    filename = filelist[0]
    print("FILENMAE", filename)
    try:
        if len(password) == 0:
            tabula.read_pdf(filename, pages=1)
        else:
            tabula.read_pdf(filename, password=password, pages=1)

    except Exception as e:
        if e.__class__.__name__ == "CalledProcessError":
            return throwException(e, "Wrong password")

    try:
        meta_output = meta(filename, password)
        meta_df = pd.DataFrame.from_dict(meta_output, orient='index')
        print("Meta data created")
        if meta_output["Producer"] == ('Microsoft: Print To PDF' or 'CamScanner'):
            quit()
        else:

            try:
                header_df, headerJson = header(filename, password, bankname)
                print("Header data created")
            except Exception as e:
                return throwException(e, "Header")

            try:
                table_df, json_data3 = table_main(filelist, password, job_id, bankname, startdate, enddate, user_id)
                print("Table Data Created")
            except Exception as e:
                return throwException(e, "Table")

            try:
                sunday_fraud = fraud(table_df)
                sunday_fraud_df = pd.DataFrame.from_dict(sunday_fraud, orient='index')
                print("Sunday transaction created.")

            except:
                data_df = {"0": ["Not found fraud sunday transaction"]}
                sunday_fraud_df = pd.DataFrame(data_df, index=[0])
                pass

            try:
                table_df_new = categorisation(table_df)
                print("Categorisation created.")
            except Exception as e:
                return throwException(e, "Categorisation")

            try:
                breakup_of_expenses, breakup_of_income, top_expenses, reccuring_expenses, loan_emi, bounced_penal, monthly_details, eod_balance, high_val_trans, irr_xns = main_ana(
                    table_df_new)
                print("Analytics created.")
            except Exception as e:
                return throwException(e, "Analytics")

            table_df_new.to_excel("before_bf.xlsx")

            try:
                basic_feature = ratios(table_df_new)
            except Exception as e:

                return throwException(e, "Basic Feature")
            # pass

            try:
                main_feature = featurelake(table_df_new)
            except Exception as e:
                return throwException(e, "Main Feature")

            # save excel to database
            new_filename = save_to_excel(user_id, path, sunday_fraud_df, breakup_of_expenses, breakup_of_income,
                                         top_expenses,
                                         reccuring_expenses, loan_emi, bounced_penal, monthly_details, eod_balance,
                                         high_val_trans,
                                         irr_xns, table_df_new, job_id, meta_df, header_df, basic_feature, main_feature)

            update_db(new_filename, job_id)

            # save data to database
            update_statement_record({
                'filename': filename,
                'password': password,
                'ifsc': json.loads(headerJson)["ifsc_code"],
                'bank_name': json.loads(headerJson)["bank_name"],
                'filesize_in_kb': get_file_size(filename, user_id),
                'no_of_rows': len(table_df.axes[0]),
                'no_of_pages': PdfFileReader(open(
                    getFilePath(filename, user_id),
                    'rb')).getNumPages(),
                'no_of_columns': len(table_df.axes[1]),
                'startDate': startdate,
                'endDate': enddate,
                'user_id': user_id,
                'has_bank_logo': 0
            })

            try:
                database(meta_df, path, filelist, job_id, header_df, table_df_new, basic_feature, monthly_details)
            except Exception as e:

                return throwException(e, "Database")

            dic = dict(new_filename, meta_df, headerJson, table_df_new, sunday_fraud_df, breakup_of_expenses,
                       breakup_of_income,
                       top_expenses, reccuring_expenses, loan_emi, bounced_penal, eod_balance, high_val_trans, irr_xns,
                       basic_feature,
                       main_feature)

            return dic

    except:
        data_df = {"0": ["Wrong Format"]}
        meta_df = pd.DataFrame(data_df, index=[0])

        print("Wrong Format")
        # if meta_df["prod"] == 'Microsoft: Print To PDF' or "CamScanner":
        #     print("Wrong format")
        #     exit()
        pass
